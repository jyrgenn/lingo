#!/usr/bin/env perl
# generate the Lisp preload code string literal

use strict;
use warnings;

print('"');
for my $file (@ARGV) {
        if (open(my $fh, '<', $file)) {
                print(";;; lisp preload: $file\\n");
                while (defined($_ = <$fh>)) {
                        chomp();
                        s/\"/\\"/g;
                        print;
                        print('\n');
                }
                close($fh);
        } else {
                die("$0: failed to open $file: $!");
        }
}
print("\"\n");
