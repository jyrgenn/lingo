(defspecial quasiquote (form)
  "In FORM, replace unquoted items them with their values as appropriate.
Return the resulting form. Unquoted items are those preceded by an »unquote«
sign (»,«) or an »unquote-splicing« (»,@«). In the latter case, if the value
is a list, splice it into the surrounding list."
  (let* ((qq-hlp                        ;quasiquote-helper, with a shorter name
          (lambda (form env)
            (if (consp form)            ;else just form
              (let ((head (car form))
                    (tail (cdr form)))
                (if (consp head)
                    (let ((headhead (car head)))
                      (cond ((eq headhead 'unquote)
                             (if (= (length head) 2)
                                 (cons (eval (cadr head) env)
                                       (qq-hlp tail env))
                               (error "wrong # args to unquote: %s" head)))
                            ((eq headhead 'unquote-splicing)
                             (if (= (length head) 2)
                                 (append (eval (cadr head) env)
                                         (qq-hlp tail env))
                               (error "wrong # args to unquote-splicing: %s"
                                      head)))
                            (t (cons (qq-hlp head env)
                                     (qq-hlp tail env)))))
                  (cons head (qq-hlp tail env))))
              form))))
    (qq-hlp form (the-environment))))
