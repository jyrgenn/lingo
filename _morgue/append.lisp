;; replaced by builtin
(defun append (&rest lists)
  "Append all argument lists and return the result as a new list."
  (let (result)
    (dolist (l lists (nreverse result))
      (dolist (el l)
        (push el result)))))
