/* Print the values for the digitWeights array in digit_char_p(). */
#include <stdio.h>

int main(void) {
        for (int i = 0; i < 128; i++) {
                int value;
                if (i < '0') {
                        value = -1;
                } else if (i <= '9') {
                        value = i - '0';
                } else if (i < 'A') {
                        value = -1;
                } else if (i <= 'Z') {
                        value = i - 'A' + 10;
                } else if (i < 'a') {
                        value = -1;
                } else if (i <= 'z') {
                        value = i - 'a' + 10;
                } else {
                        value = -1;
                }
                printf("\t%d,\n", value);
        }
        return 0;
}
