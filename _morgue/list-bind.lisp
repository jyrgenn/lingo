
;;; (list-bind ((var1 ...) expr)
;;;   bodyforms)
;;;
;;; binds the variables var1 ... to the members of the list resulting from
;;; the evaluation of expr. If there are fewer members than variables, the
;;; remaning variables will be bound to nil. If there are more members
;;; than variables, then the remaining members will be bound to the
;;; last cdr of the variables list, if it is non-null, or dropped. The value
;;; of the list-bind form is the value of its last bodyform.
;;;
;;; Examples:
;;;   (list-bind ((var1 var2 var3) '(a b c d e f))
;;;     (list var1 var2 var3))
;;; => (a b c)
;;;
;;;   (list-bind ((var1 var2 var3) '(a b))
;;;     (list var1 var2 var3))
;;; => (a b nil)
;;;
;;;   (list-bind ((var1 var2 var3 . var4) '(a b c d e f))
;;;     (list var1 var2 var3 var4))
;;; => (a b c (d e f))


;;(list-bind ((a b) '(3 4 5)) (list a b))
;;(list-bind ((a b c) '(3 4)) (list a b c))
;;(list-bind ((a b . c) '(3 4 5 6)) (list a b c))

(defmacro list-bind (binding &rest bodyforms)
  (let ((valsym (gensym)))
    `(let* ((,valsym ,(cadr binding))
            ,@(let ((result '())
                    (vars (car binding)))
                (while (consp vars)
                  (push (list (pop vars) `(pop ,valsym)) result))
                (when vars
                  (push (list vars `,valsym) result))
                (nreverse result)))
       ,@bodyforms)))

(defmacro setsym (sym val)
  `(setf (unquote sym) (unquote val)))

(defun aaa (&rest args)
  (list-bind ((a b c) args)
             (list a b c)))

(defun aab (&rest args)
  (list-bind ((a b . c) args)
             (list a b c)))
