// Object-less utilities
package olu

import (
	"bytes"
	"io"
	"os/user"
	"regexp"

	"../msg"
)

var ReHostname *regexp.Regexp
var ReHostPort *regexp.Regexp
var ReIPv6 *regexp.Regexp
var ReIPv6Port *regexp.Regexp
var ReIPv4 *regexp.Regexp
var ReIPv4Port *regexp.Regexp

var reTildePathname *regexp.Regexp
var dotDotPathname *regexp.Regexp
var slashRE *regexp.Regexp
var slashSlashPathname *regexp.Regexp

func NormalizePathname(pathname string) string {
	pathname = slashSlashPathname.ReplaceAllString(pathname, "/")
	matches := dotDotPathname.FindStringSubmatch(pathname)
	if matches != nil {
		result := []string{}
		// path components
		pcomps := slashRE.Split(pathname, -1)
		isAbsolute := len(pcomps) > 0 && pcomps[0] == ""
		if isAbsolute {
			// it is easier to deal with an absolute path by keeping
			// that fact separate instead of treating the zero
			// component totally special
			pcomps = pcomps[1:]
		}
		for i, dir := range pcomps {
			if dir == "." {
				// ignore . components
				continue
			} else if dir == "" && i < len(pcomps)-1 {
				// ignore an empty component except if it's the
				// last one
				continue
			} else if dir == ".." {
				if len(result) > 0 {
					// on .., drop the last component we saw
					result = result[:len(result)-1]
				}
			} else {
				// append all others
				result = append(result, dir)
			}
		}
		resultBuf := bytes.NewBufferString("")
		if isAbsolute {
			resultBuf.WriteRune('/')
		}
		lastComp := len(result) - 1
		for i, dir := range result {
			resultBuf.WriteString(dir)
			if i < lastComp {
				resultBuf.WriteRune('/')
			}
		}
		pathname = resultBuf.String()
	}
	return pathname
}

// expand ~ and ~user in path names, also canonicalise
func ExpandFileName(pathname string) string {
	matches := reTildePathname.FindStringSubmatch(pathname)
	if matches != nil {
		var usr *user.User
		var err interface{}
		if matches[1] == "" {
			usr, err = user.Current()
		} else {
			usr, err = user.Lookup(matches[1])
		}
		if err == nil {
			pathname = reTildePathname.ReplaceAllString(pathname,
				usr.HomeDir+matches[2])
		}
	}
	return NormalizePathname(pathname)
}

func SplitHostPort(hostmaybeport, maybeport string) (string, string) {
	hp := ReHostname.FindStringSubmatch(hostmaybeport)
	if hp != nil {
		return hp[1], maybeport
	}
	hp = ReHostPort.FindStringSubmatch(hostmaybeport)
	if hp != nil {
		if maybeport != "" {
			msg.Bailf("hostname:port spec plus port: %s + %s",
				hostmaybeport, maybeport)
		}
		return hp[1], hp[2]
	}
	hp = ReIPv6.FindStringSubmatch(hostmaybeport)
	if hp != nil {
		return hp[1], maybeport
	}
	hp = ReIPv6Port.FindStringSubmatch(hostmaybeport)
	if hp != nil {
		if maybeport != "" {
			msg.Bailf("IPv6addr:port spec plus port: %s + %s",
				hostmaybeport, maybeport)
		}
		return hp[1], hp[2]
	}
	hp = ReIPv4.FindStringSubmatch(hostmaybeport)
	if hp != nil {
		return hp[1], maybeport
	}
	hp = ReIPv4Port.FindStringSubmatch(hostmaybeport)
	if hp != nil {
		if maybeport != "" {
			msg.Bailf("IPv4addr:port spec plus port: %s + %s",
				hostmaybeport, maybeport)
		}
		return hp[1], maybeport
	}
	portpart := ""
	if maybeport != "" {
		portpart = "+ " + maybeport
	}
	msg.Bailf("no valid host[:port] spec found in `%s%s'",
		hostmaybeport, portpart)
	return "", ""
}

// Return a substring of string `s', starting at `start' and ending before
// 'end'; these counts refer to runes, not bytes. If `end' is smaller than zero,
// copy to the end of `s'.
//
func RuneSubstr(s string, start, end int) string {
	sbuf := bytes.NewBufferString(s)
	nbuf := bytes.NewBufferString("")
	si := 0
	if start < 0 {
		msg.Bailf("negative substring start index %d for \"%s\"",
			start, s)
	}
	for ; si < start; si++ {
		_, _, err := sbuf.ReadRune()
		if err != nil {
			if err == io.EOF {
				msg.Bailf("invalid substring start index %d"+
					" for string of length %d (\"%s\")",
					start, len(s), s)
			}
			msg.Bailf("reading from string \"%s\": %v", s, err)
		}
	}
	for ; si < end || end < 0; si++ {
		r, _, err := sbuf.ReadRune()
		if err != nil {
			if err == io.EOF {
				if end < 0 {
					break
				} else {
					msg.Bailf("invalid substring end"+
						" index %d for string of"+
						" length %d (\"%s\")",
						end, len(s), s)
				}
			}
			msg.Bailf("reading from string \"%s\": %v", s, err)
		}
		nbuf.WriteRune(r)
	}
	return nbuf.String()
}

func StringPosition(s string, ch rune, fromEnd bool, start, end int) int {
	result := -1
	pos := 0
	for _, r := range s {
		if end >= 0 && pos >= end {
			break
		}
		if r == ch && pos >= start {
			result = pos
			if !fromEnd {
				break
			}
		}
		pos++
	}
	return result
}

func Init_OLU() {
	//fmt.Println("olu/object-less-utils.init")
	ReHostname = regexp.MustCompile("^([[:alnum:].-]+)$")
	ReHostPort = regexp.MustCompile("^([[:alnum:].-]+):([[:alnum:]]+)$")
	ReIPv6 = regexp.MustCompile("^\\[?([[:xdigit:]:]+)\\]?$")
	ReIPv6Port =
		regexp.MustCompile("^\\[([[:xdigit:]:]+)\\]:([[:alnum:]]+)$")
	ReIPv4 = regexp.MustCompile("^([\\d.]+)$")
	ReIPv4Port = regexp.MustCompile("^([\\d.]+):([[:alnum:]]+)$")
	reTildePathname = regexp.MustCompile("^~([^/]*)(/.*)?$")
	dotDotPathname = regexp.MustCompile("/\\.\\.?(/.*)?")
	slashSlashPathname = regexp.MustCompile("/\\.?/+")
	slashRE = regexp.MustCompile("/")
}
