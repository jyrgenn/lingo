package olu

import "os"

func IsDirectory(fname string) bool {
	finfo, err := os.Stat(fname)
	return err == nil && finfo.IsDir()
}
