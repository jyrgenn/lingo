// Go test module for object-less-utils
package olu

import (
	"fmt"
	"testing"
)

func rss(t *testing.T, s string, start, end int, expect string) {
	var calc string
	func() {
		defer func() {
			err := recover()
			if err != nil {
				fmt.Printf("err = %v\n", err)
				calc = "err"
			}
		}()
		calc = RuneSubstr(s, start, end)
	}()
	if calc != expect {
		t.Error(fmt.Sprintf("start %d end %d expect \"%s\", is \"%s\"",
			start, end, expect, calc))
	}
}

func TestRuneSubstr(t *testing.T) {
	theString := "Jürgen ⌘ möchte 日 teßten 㛛"
	rss(t, theString, 0, -1, theString)
	rss(t, theString, 0, 26, theString)
	rss(t, theString, 0, 25, "Jürgen ⌘ möchte 日 teßten ")
	rss(t, theString, 0, 200, "err")
	rss(t, theString, 0, 1, "J")
	rss(t, theString, 0, 2, "Jü")
	rss(t, theString, 0, 3, "Jür")
	rss(t, theString, 3, 10, "gen ⌘ m")
	rss(t, theString, 33, 310, "err")
	rss(t, theString, 10, 3, "")
}

func spos(t *testing.T, s string, ch rune, fromEnd bool,
	start, end int, expect int) {

	calc := StringPosition(s, ch, fromEnd, start, end)
	if calc != expect {
		t.Error(fmt.Sprintf("s \"%s\" ch %c fromEnd %v start %d end %d expect %d, is %d",
			s, ch, fromEnd, start, end, expect, calc))
	}
}

func TestStringPosition(t *testing.T) {
	theString := "Jürgen ⌘möcht日ß㛛" // must not have repeating runes
	pos := 0
	spos(t, "abcdedcba", 'c', false, 0, -1, 2)
	spos(t, "abcdedcba", 'c', false, 3, -1, 6)
	spos(t, "abcdedcba", 'c', true, 0, -1, 6)
	spos(t, "abcdedcba", 'c', true, 0, 5, 2)
	spos(t, "abcdedcba", 'd', true, 2, 6, 5)
	spos(t, "abcdedcba", 'd', false, 2, 6, 3)
	for _, ch := range theString {
		//fmt.Printf("s \"%s\" pos %d ch %c\n", theString, pos, ch)
		spos(t, theString, ch, false, 0, len(theString), pos)
		pos++
	}
}
