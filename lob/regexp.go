// Regexp object type
package lob

import (
	"bytes"
	"fmt"
	"regexp"

	"../msg"
)

var regexpCount = 0

func NRegexps() int {
	return regexpCount
}

type Regexp struct {
	re *regexp.Regexp
}

func (re *Regexp) Type() Obtype {
	return REGEXP
}

func (_ *Regexp) IsSpecial() bool {
	return false
}

func (re *Regexp) Call(nargs int, arglist Object) Object {
	var limit int = 1
	allMatches := false
	var target string
	if nargs == 0 {
		msg.Bailf("calling regexp (%s) needs a string argument",
			re.ValueString(nil))
		return nil
	} else if nargs >= 1 {
		arg1 := Pop(&arglist)
		target = StringArg(arg1, "string")
		if nargs == 2 {
			limitArg := Pop(&arglist)
			if limitArg == T {
				allMatches = true
				limit = -1
			} else if limitArg == Nil {
				// as if not specified
			} else {
				allMatches = true
				limit = int(NumberArg(limitArg,
					"regexp match limit"))
			}
		} else if nargs > 2 {
			msg.Bailf("too many arguments for calling regexp (%s)",
				re.ValueString(nil))
			return nil
		}
	}
	if allMatches {
		return StringSliceSlice2list(
			re.re.FindAllStringSubmatch(target, limit))
	} else {
		return StringSlice2list(re.re.FindStringSubmatch(target))
	}
}

func (re *Regexp) Deftype() string {
	return "regexp"
}

func (re *Regexp) Synopsis() string {
	return "(REGEXP STRING &optional LIMIT) => matches"
}

func (re *Regexp) Docstring() string {
	return `If STRING matches REGEXP, return list of match and sub-matches, else nil.
With optional third argument LIMIT, a list of match lists for (potentially)
multiple matches is returned. If LIMIT is t, all matches are considered;
otherwise, a number specifies the number of matches to be considered.

Regular expression syntax is that of the Go regexp package (RE2), which
is largely similar to that of the Perl and Python languages. A "(?flags)"
specification in the regexp can modify the behaviour of the match in the
current group. Possible flags are:

i  case-insensitive (default false)
m  multi-line mode: ^ and $ match begin/end line in addition to begin/end
   text (default false)
s  let . match \n (default false)
U  ungreedy: swap meaning of x* and x*?, x+ and x+?, etc (default false)
`
}

func (re *Regexp) Equal(ob Object) bool {
	if !IsRegexp(ob) {
		return false
	}
	return re.re.String() == ob.(*Regexp).re.String()
}

func (re *Regexp) Eq(ob Object) bool {
	return re.Equal(ob)
}

func escapeSlashes(in string) string {
	var buf bytes.Buffer
	for _, r := range in {
		switch r {
		case '/':
			buf.WriteRune('\\')
		}
		buf.WriteRune(r)
	}
	return buf.String()
}

func (re *Regexp) ValueString(_ Obset) string {
	return fmt.Sprintf("#/%v/", escapeSlashes(re.re.String()))
}

func (re *Regexp) ReaderString(_ Obset) string {
	return re.ValueString(nil)
}
func (re *Regexp) String() string {
	return re.ValueString(nil)
}

func (re *Regexp) Regexp() *regexp.Regexp {
	return re.re
}

func (re *Regexp) IsNil() bool {
	return false
}

func (ob *Regexp) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Regexp) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Regexp) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Regexp) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func NewRegexp(s string) *Regexp {
	//regexpCount++
	re, err := regexp.Compile(s)
	if err != nil {
		msg.Bail("regexp:", err)
	}
	ob := Regexp{re: re}
	//msg.Trace("new regexp", ob)
	return &ob
}

func Init_regexp() {
	//fmt.Println("lob/regexp.init")
}
