// Object helper functions
package lob

// func List2slice(arglist Object, nargs int) []Object {
// 	args := make([]Object, nargs, nargs)

// 	for i := 0; IsPair(arglist); i++ {
// 		args[i] = Pop(&arglist)
// 	}
// 	return args
// }

func length(list Object) int {
	length := 0
	for IsPair(list) {
		list = list.(*Pair).Cdr()
		length++
	}
	return length
}

func ListOf(arg ...Object) Object {
	return Slice2list(arg)
}

func StringSliceSlice2list(args [][]string) Object {
	var result Object = Nil
	if args != nil {
		for i := len(args) - 1; i >= 0; i-- {
			result = Cons(StringSlice2list(args[i]), result)
		}
	}
	return result
}

func StringSlice2list(args []string) Object {
	var result Object = Nil
	if args != nil {
		for i := len(args) - 1; i >= 0; i-- {
			result = Cons(NewString(args[i]), result)
		}
	}
	return result
}

func Slice2list(args []Object) Object {
	var result Object = Nil
	for i := len(args) - 1; i >= 0; i-- {
		result = Cons(args[i], result)
	}
	return result
}

func Uint64Slice2List(args []uint64) Object {
	var result Object = Nil
	for i := len(args) - 1; i >= 0; i-- {
		result = Cons(NewNumber(float64(args[i])), result)
	}
	return result
}

// EOF
