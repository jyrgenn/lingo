// Traceable object interface
package lob

//import "fmt"

type Traceable interface {
	Object
	SetTrace(flag bool)
	GetTrace() bool
}

func Init_traceable() {
	//fmt.Println("lob/traceable.init")
}
