// Table object type
package lob

// The module implements the Table object type.

import (
	"bytes"
	"fmt"
	"sort"

	"../msg"
)

var tableCount = 0

// Return the total number of tables created.
func NTables() int {
	return tableCount
}

// The key type is an interface, as we want it to encompass numbers, strings,
// characters and (other) objects.
type Key interface{}

// This is the actual table object type.
type Table struct {
	m map[Key]Object
}

// Member type of a KVSlice, which we use for comparisons.
type KVPair struct {
	key       Key
	uniqueTag string // for sorting
	value     Object
}

// A transformation of a table used for comparisons. It implements the interface
// used by sort.Sort().
type KVSlice []KVPair

// The length of the slice; part of KVSlice's interface for sorting.
func (kvs KVSlice) Len() int {
	return len(kvs)
}

// The swap function of the slice; part of KVSlice's interface for sorting.
func (kvs KVSlice) Swap(i, j int) {
	kvs[i], kvs[j] = kvs[j], kvs[i]
}

// The comparison function of the slice; part of KVSlice's interface for
// sorting.
func (kvs KVSlice) Less(i, j int) bool {
	return kvs[i].kvpUniqueTag() < kvs[j].kvpUniqueTag()
}

// The type of the Table object.
func (t *Table) Type() Obtype {
	return TABLE
}

// Return a KVPair's unique tag, used for sorting. It contains the type so we
// can distinguish e.g. a symbol |a| from the character 'a'.
func (kvp KVPair) kvpUniqueTag() string {
	if kvp.uniqueTag == "" {
		switch key := kvp.key.(type) {
		case float64:
			kvp.uniqueTag = fmt.Sprintf("n%v", key)
		case string:
			kvp.uniqueTag = fmt.Sprintf("s%v", key)
		case rune:
			kvp.uniqueTag = fmt.Sprintf("r%v", key)
		default:
			kvp.uniqueTag = fmt.Sprintf("o%p", key)
		}
	}
	return kvp.uniqueTag
}

// Return a slice of KVPairs in deterministic (i.e. sorted) order.
func (t *Table) KVSlice() []KVPair {
	kv := make([]KVPair, len(t.m))
	index := 0
	for key, value := range t.m {
		kv[index].key = key
		kv[index].value = value
		index++
	}
	sort.Sort(KVSlice(kv))
	return kv
}

// For the Callable interface: is this a special function? No, nay, never.
func (t *Table) IsSpecial() bool {
	return false
}

// For the Callable interface: call the table as a function.
func (t *Table) Call(nargs int, args Object) Object {
	arglist := args.(Sequence)
	switch nargs {
	case 1:
		value, _ := t.Get(arglist.Car())
		if value == nil {
			return Nil
		}
		return value
	case 2:
		value := arglist.Cdr().Car()
		t.Put(arglist.Car(), value)
		return value
	case 0:
		msg.Bail("calling table " + t.ValueString(nil) +
			" needs at least a key argument")
	default:
		msg.Bail("too many arguments for calling table " +
			t.ValueString(nil) + "; args are " +
			arglist.ValueString(nil))
	}
	return nil
}

// For the Callable interface: return type of callable.
func (t *Table) Deftype() string {
	return "table"
}

// For the Callable interface: return the call synopsis.
func (t *Table) Synopsis() string {
	return "(TABLE KEY &optional VALUE) => value"
}

// For the Callable interface: return the documentation string.
func (t *Table) Docstring() string {
	return `Return the value stored in the TABLE for KEY.
With optional VALUE, set the value.`
}

// Check if the table is #'equal to the other object.
func (t *Table) Equal(ob Object) bool {
	var t2 *Table
	switch tab := ob.(type) {
	case *Table:
		t2 = tab
	default:
		return false
	}
	if t == t2 { // eq
		return true
	}
	if len(t.m) != len(t2.m) {
		return false
	}
	kv1 := t.KVSlice()
	kv2 := t2.KVSlice()
	for i, kvp := range kv1 {
		if kvp.kvpUniqueTag() != kv2[i].kvpUniqueTag() {
			return false
		}
		if !kvp.value.Equal(kv2[i].value) {
			return false
		}
	}
	return true
}

// Check if the table is #'eq to the other object.
func (t *Table) Eq(ob Object) bool {
	return ob == t
}

// To let equal numbers, strings, and runes be the *same* key (and not, perhaps,
// different number objects of the same value), extract their value from the
// object and make it the key in a table. key2ob() below does the reverse
// operation.
func ob2key(key Object) Key {
	switch k := key.(type) {
	case *Number:
		return k.Value()
	case *String:
		return k.ValueString(nil)
	case *Char:
		return k.Value()
	}
	return key
}

// Reverse operation to ob2key(); see there (above) for the rationale of this.
func key2ob(k Key) Object {
	switch key := k.(type) {
	case float64:
		return NewNumber(key)
	case string:
		return NewString(key)
	case rune:
		return NewChar(key)
	case Object:
		return key
	}
	return nil
}

// The string representing the value of the table.
func (t *Table) ValueString(pmap Obset) string {
	return t.ReaderString(pmap)
}

// The reader string is the same as the value string. Reading this will bomb on
// undef values, though.
func (t *Table) ReaderString(pmap Obset) string {
	if pmap == nil {
		pmap = make(Obset)
	} else {
		if pmap[t] {
			return "[...]"
		}
	}
	pmap[t] = true
	defer func() { delete(pmap, t) }()

	result := bytes.NewBufferString("#:(")
	for _, kv := range t.KVSlice() {
		result.WriteString("(")
		result.WriteString(key2ob(kv.key).ReaderString(pmap))
		result.WriteString(" . ")
		if kv.value == nil {
			result.WriteString("<<###UNDEF###>>")
		} else {
			result.WriteString(kv.value.ReaderString(pmap))
		}
		result.WriteString(")")
	}
	result.WriteString(")")
	return result.String()
}

// The String string (for printing %v).
func (t *Table) String() string {
	return t.ValueString(nil)
}

// Cxr() is defined for all objects, but makes no sense on a table.
func (ob *Table) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

// Car() is defined for all objects, but makes no sense on a table.
func (ob *Table) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

// Cdr() is defined for all objects, but makes no sense on a table.
func (ob *Table) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

// Rplacd() is defined for all objects, but makes no sense on a table.
func (ob *Table) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

// A table is never Nil.
func (t *Table) IsNil() bool {
	return false
}

// Put a key/value pair into the table. Value must be defined.
func (t *Table) Put(key Object, value Object) {
	if value == nil {
		msg.Bailf("table put: value is undefined for key %s", key)
	}
	t.m[ob2key(key)] = value
}

// Get a value for a key from the table. Return the value, if present, and if
// present.
func (t *Table) Get(key Object) (Object, bool) {
	value, ok := t.m[ob2key(key)]
	return value, ok
}

// Delete the pair for the specified key from the table.
func (t *Table) Delete(key Object) {
	delete(t.m, ob2key(key))
}

// Return the list of key/value pairs in the table as Lisp pairs (cons cells)
func (t *Table) KVPairs() Object {
	var pairs List = Nil
	for key, value := range t.m {
		pairs = Cons(Cons(key2ob(key), value), pairs).(List)
	}
	return pairs
}

// Return the list of keys in the table.
func (t *Table) Keys() Object {
	var keys List = Nil
	for key := range t.m {
		keys = Cons(key2ob(key), keys).(List)
	}
	return keys
}

// Return the list of values in the table.
func (t *Table) Values() Object {
	var values List = Nil
	for _, value := range t.m {
		values = Cons(value, values).(List)
	}
	return values
}

// Return the key/value pair count of the table.
func (t *Table) Count() int {
	return len(t.m)
}

// Create a new empty table object.
func NewTable() *Table {
	return NewMapTable(make(map[Key]Object))
}

// Create a new table object from the specified map.
func NewMapTable(theTable map[Key]Object) *Table {
	//tableCount++
	return &Table{m: theTable}
}

// Return a new table object containing the kay/value pairs from the specified
// list.
func NewPairsTable(kvpairs Object) *Table {
	theTable := NewTable()
	for IsPair(kvpairs) {
		first := Pop(&kvpairs)
		switch elem := first.(type) {
		case *Pair:
			theTable.Put(elem.Car(), elem.Cdr())
		default:
			theTable.Put(elem, Nil)
		}
	}
	return theTable
}

// Initialize the table module.
func Init_table() {
	//fmt.Println("lob/table.init")
}

// EOF
