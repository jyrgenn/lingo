// Object interface and related functions
package lob // the Lisp Objects

import (
	"../msg"
)

const UndefName = "<##UNDEF##>" // for some weird reason, undefined
// (i.e. nil) values creep up not only
// in the environment maps (which would
// already be strange enough), but also
// as the symbol value in
// symbol.String(). This should be
// totally impossible.

const ( // Object Types
	INVALiD        Obtype = 0 + iota // invalid object type as guard value
	SYMBOL                           // a Lisp symbol
	PAIR                             // cons cell
	FUNCTION                         // any function or special form
	NUMBER                           // any number
	STRING                           // a string
	CHAR                             // a character
	TABLE                            // Go's map
	VECTOR                           // simple one-dimensional array
	PORT                             // I/O port
	NETADDR                          // network address
	REGEXP                           // regular expression
	ENVIRON                          // environment
	STRUCT                           // struct
	LIST_COLLECTOR                   // ListPusher
)

var typeNames = [...]string{
	"INVALiD",
	"symbol",
	"pair",
	"function",
	"number",
	"string",
	"char",
	"table",
	"vector",
	"port",
	"netaddr",
	"regexp",
	"environment",
	"struct",
	"list-collector",
}

type Obtype uint8

type Obmap map[Object]Object // for parsed keywords etc.

type Obset map[Object]bool // set of Objects

type Object interface {
	Type() Obtype // one of the constants above

	// the Obset passed here contains the objects already being printed in
	// this recursive descent, to recognize and handle printing of cyclic
	// data structures
	ValueString(Obset) string
	ReaderString(Obset) string

	IsNil() bool
	Car() Object
	Cdr() Object
	Cxr() (Object, Object)
	Rplacd(Object)
	Equal(Object) bool
	Eq(Object) bool
}

func (t Obtype) Name() string {
	return typeNames[int(t)]
}

func Coerce(ob Object, obtype Obtype, what string) Object {
	//msg.Trace("Coerce", ob, "to", obtype.Name(), what)
	if ob.Type() == obtype {
		return ob
	}
	msg.Bail(what, "is not a", typeNames[obtype]+":", ob)
	return Nil
}

func IsNumber(ob Object) bool {
	switch ob.(type) {
	case *Number:
		return true
	}
	return false
}

func IsChar(ob Object) bool {
	switch ob.(type) {
	case *Char:
		return true
	}
	return false
}

func IsSequence(ob Object) bool {
	if ob == Nil {
		return true
	}
	switch ob.(type) {
	case *Pair, *String, *Vector:
		return true
	}
	return false
}

func IsComparable(ob Object) bool {
	switch ob.(type) {
	case *Symbol, *Number, *String, *Char:
		return true
	}
	return false
}

func IsKeyword(ob Object) bool {
	switch sym := ob.(type) {
	case *Symbol:
		return sym.name[0] == ':'
	}
	return false
}

func IsPair(ob Object) bool {
	switch ob.(type) {
	case *Pair:
		return true
	}
	return false
}

func IsList(ob Object) bool {
	if ob == Nil {
		return true
	}
	switch ob.(type) {
	case *Pair:
		return true
	}
	return false
}

func IsSymbol(ob Object) bool {
	switch ob.(type) {
	case *Symbol:
		return true
	}
	return false
}

func IsEnvironment(ob Object) bool {
	switch ob.(type) {
	case *Environment:
		return true
	}
	return false
}

func IsAtom(ob Object) bool {
	switch ob.(type) {
	case *Symbol, *Number, *String, *Char:
		return true
	}
	return false
}

func IsVector(ob Object) bool {
	switch ob.(type) {
	case *Vector:
		return true
	}
	return false
}

func IsString(ob Object) bool {
	switch ob.(type) {
	case *String:
		return true
	}
	return false
}

func IsFunction(ob Object) bool {
	switch ob.Type() {
	case FUNCTION:
		return true
	}
	return false
}

func IsRegexp(ob Object) bool {
	switch ob.(type) {
	case *Regexp:
		return true
	}
	return false
}

func IsPort(ob Object) bool {
	switch ob.(type) {
	case *Port:
		return true
	}
	return false
}

func IsNetaddr(ob Object) bool {
	switch ob.(type) {
	case *Netaddr:
		return true
	}
	return false
}

func IsStruct(ob Object) bool {
	switch ob.(type) {
	case *Struct:
		return true
	}
	return false
}

func IsTable(ob Object) bool {
	switch ob.(type) {
	case *Table:
		return true
	}
	return false
}

func Bool2Ob(value bool) Object {
	if value {
		return T
	}
	return Nil
}

func Ob2Bool(value Object) bool {
	return value != Nil
}

func Boolish(value Object) Object {
	if value == Nil {
		return Nil
	}
	return T
}

func NumberArg(arg Object, whatarg string) float64 {
	if IsNumber(arg) {
		return arg.(*Number).Value()
	}
	msg.Bail(whatarg, "argument is not a number:", arg)
	return 0
}

func StringArg(arg Object, whatarg string) string {
	if IsString(arg) {
		return arg.(*String).ValueString(nil)
	}
	msg.Bail(whatarg, "argument is not a string:", arg)
	return ""
}

func Init_object() {
	//fmt.Println("lob/object.init")
}

// EOF
