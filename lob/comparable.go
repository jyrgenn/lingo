// Comparable object interface
package lob

//import "fmt"

type Comparable interface {
	Object
	Cmp(other Comparable) int // -1, 0, 1 for this <, =, > other
	NotEqual(others Object) bool
}

func Init_comparable() {
	//fmt.Println("lob/comparable.init")
}
