// Pair/cons object type
package lob

import (
	"bytes"

	"../msg"
)

var pairCounter = 0

func NPairs() int {
	return pairCounter
}

type Pair struct {
	car, cdr Object
}

func (p *Pair) Type() Obtype {
	return PAIR
}

func (p *Pair) Equal(ob Object) bool {
	var p2 *Pair
	switch pp := ob.(type) {
	case *Pair:
		p2 = pp
	default:
		return false
	}
	return p == p2 ||
		(p.Car().Equal(ob.Car()) && p.Cdr().Equal(ob.Cdr()))
}

func (p *Pair) Eq(ob Object) bool {
	return p == ob
}

func (p *Pair) Delete(item Object) Object {
	return p.Remove(item)
}

func (p *Pair) Remove(item Object) Object {
	switch theCdr := p.Cdr().(type) {
	default:
		msg.Bail("delete/remove on non-proper list:", p)
	case Sequence:
		cdrResult := theCdr.Delete(item)
		if item.Eq(p.Car()) {
			return cdrResult
		} else {
			return Cons(p.Car(), cdrResult)
		}
	}
	return Nil // not reached
}

func (p *Pair) ValueString(pmap Obset) string {
	return p.ReaderString(pmap)
}

// does not recurse for a list
func (p *Pair) ReaderString(pmap Obset) string {
	if pmap == nil {
		pmap = make(Obset)
	} else {
		if pmap[p] {
			return "[...]"
		}
	}
	delmap := make(Obset) // those to delete later
	defer func() {
		for item, _ := range delmap {
			delete(pmap, item)
		}
	}()

	result := bytes.NewBufferString("(")
	first := true
	var cur Object = p
	for IsPair(cur) && !pmap[cur] {
		pmap[cur] = true
		delmap[cur] = true
		if first {
			first = false
		} else {
			result.WriteString(" ")
		}
		var el Object
		el, cur = cur.Cxr()
		result.WriteString(el.ReaderString(pmap))
	}
	if !cur.IsNil() {
		result.WriteString(" . ")
		result.WriteString(cur.ReaderString(pmap))
	}
	result.WriteString(")")
	return result.String()
}

func (p *Pair) String() string {
	return p.ValueString(nil)
}

func (p *Pair) IsNil() bool {
	return false
}

func (p *Pair) Rplaca(newcar Object) {
	p.car = newcar
}

func (p *Pair) Rplacd(newcdr Object) {
	p.cdr = newcdr
}

func (p *Pair) Car() Object {
	return p.car
}

func (p *Pair) Cdr() Object {
	return p.cdr
}

func (p *Pair) Length() int {
	return length(p) // we need this function separately anyway
}

func (p *Pair) SubSequence(start, end int) Object {
	list := Object(p)
	to_end := end < 0
	for start > 0 {
		list = list.Cdr()
		if !IsPair(list) {
			return Nil
		}
		start--
		end--
	}
	lp := NewListPusher()
	for (to_end || end > 0) && IsPair(list) {
		lp.Push(Pop(&list))
		end--
	}
	return lp.List()
}

func (p *Pair) Position(item Object, fromEnd bool, start, end int) int {
	result := -1
	pos := 0
	var curp Object = p
	for IsPair(curp) && (end < 0 || pos < end) {
		elem := Pop(&curp)
		if pos >= start && elem.Eq(item) {
			if fromEnd {
				result = pos
			} else {
				return pos
			}
		}
		pos++
	}
	return result
}

func (p *Pair) Element(index int) Object {
	list := Object(p)
	i := index
	for IsPair(list) {
		elem := Pop(&list)
		if i == 0 {
			return elem
		}
		i--
	}
	return nil
}

func (p *Pair) SetElement(index int, value Object) {
	list := Object(p)
	i := index
	for IsPair(list) {
		if i == 0 {
			list.(*Pair).car = value
			return
		}
		list = list.Cdr()
		i--
	}
	msg.Bailf("index %d out of bounds for list %v", index, p)
}

func (p *Pair) ToList() Object {
	return p
}

func (p *Pair) ElementPair(index int) *Pair {
	list := Object(p)
	i := index
	for IsPair(list) {
		if i == 0 {
			return list.(*Pair)
		}
		list = list.Cdr()
		i--
	}
	msg.Bailf("invalid index %d for list %v", index, p)
	return nil
}

func (p *Pair) Copy() Object {
	lp := NewListPusher()
	list := Object(p)
	for IsPair(list) {
		lp.Push(Pop(&list))
	}
	lp.AddEnd(list) // may, but need not be Nil
	return lp.List()
}

func (p *Pair) Reverse() Object {
	result := Object(Nil)
	list := Object(p)
	for IsPair(list) {
		car := Pop(&list)
		result = Cons(car, result)
	}
	return result
}

func (p *Pair) NReverse() Object {
	next := p.Cdr()
	p.Rplacd(Nil)
	for IsPair(next) {
		nextPair := Coerce(next, PAIR, "nreverse: cdr").(*Pair)
		next = nextPair.Cdr()
		nextPair.Rplacd(p)
		p = nextPair
	}
	return p
}

func (p *Pair) Cxr() (Object, Object) {
	return p.car, p.cdr
}

// pop the first element from a pair and return it; set the passed pair pointer
// to the cdr of the pair
func Pop(p *Object) Object {
	ob := *p
	switch pair := ob.(type) {
	case *Pair:
		car := pair.car
		*p = pair.cdr
		return car
	default:
		return Nil
	}
}

func Cons(car Object, cdr Object) Object {
	pairCounter++
	return &Pair{car: car, cdr: cdr}
}

func Init_pair() {
	//fmt.Println("lob/pair.init")
}
