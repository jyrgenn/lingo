// Number object type
package lob

import (
	"fmt"

	"../msg"
)

var numberCount = 0

func NNumbers() int {
	return numberCount
}

type Number struct {
	value float64
}

// be so kind and don't change these, nor any other Number object (it could be
// one of these!)
var Zero = &Number{value: 0}
var One = &Number{value: 1}
var MinusOne = &Number{value: -1}

func (n *Number) Type() Obtype {
	return NUMBER
}

func (n *Number) Equal(ob Object) bool {
	return n.Eq(ob)
}

func (n *Number) Eq(ob Object) bool {
	switch o := ob.(type) {
	case *Number:
		return o.value == n.value
	}
	return false
}

func (num *Number) Cmp(other Comparable) int {
	if !IsNumber(other) {
		msg.Bailf("compare: number %v to other: %v (%v)",
			num, other, other.Type().Name())
	}
	oval := other.(*Number).value
	if num.value < oval {
		return -1
	}
	if num.value > oval {
		return 1
	}
	return 0
}

func (num *Number) NotEqual(others Object) bool {
	have := make(map[float64]bool)
	have[num.value] = true
	for IsPair(others) {
		arg := Pop(&others)
		switch arg.(type) {
		default:
			msg.Bailf("/=: number %s compared to %v (%s)",
				num.ReaderString(nil), arg.ReaderString(nil),
				arg.Type().Name())
		case *Number:
			value := arg.(*Number).value
			if have[value] {
				return false
			}
			have[value] = true
		}
	}
	return true
}

func (n *Number) ValueString(_ Obset) string {
	if float64(int64(n.value)) == n.value {
		return fmt.Sprintf("%v", int64(n.value))
	}
	return fmt.Sprintf("%v", n.value)
}

func (n *Number) ReaderString(_ Obset) string {
	return n.ValueString(nil)
}

func (n *Number) String() string {
	return n.ValueString(nil)
}

func (n *Number) IsNil() bool {
	return false
}

func (n *Number) Value() float64 {
	return n.value
}

func (n *Number) MaybeIntVal() interface{} {
	if float64(int64(n.value)) == n.value {
		return int64(n.value)
	}
	return n.value
}

func (ob *Number) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Number) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Number) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Number) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func NewNumber(n float64) *Number {
	//numberCount++
	ob := Number{value: n}
	//msg.Trace("new number", ob)
	return &ob
}

func NewIntNumber(n int) *Number {
	return NewNumber(float64(n))
}

func NewInt64Number(n int64) *Number {
	return NewNumber(float64(n))
}

func Init_number() {
	//fmt.Println("lob/number.init")
}
