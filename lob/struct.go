// Struct object type
package lob

import (
	"bytes"
	"fmt"

	"../msg"
)

var structCount = 0

var structTypeMap = map[*Symbol]*StructType{}

func NStructs() int {
	return structCount
}

type StructType struct {
	tag         *Symbol
	doc         *String
	slotIndexes map[*Symbol]int
	slots       []*Symbol
}

type Struct struct {
	stype *StructType
	slots []Object
}

func (s *Struct) Type() Obtype {
	return STRUCT
}

func (s *Struct) Equal(ob Object) bool {
	var s2 *Struct
	switch str := ob.(type) {
	case *Struct:
		s2 = str
	default:
		return false
	}
	if s == s2 {
		return true
	}
	if s.stype != s2.stype {
		return false
	}
	for i, val := range s.slots {
		if !val.Equal(s2.slots[i]) {
			return false
		}
	}
	return true
}

func (s *Struct) Eq(ob Object) bool {
	return s == ob
}

func (s *Struct) Get(index int) Object {
	if index < 0 || index >= len(s.slots) {
		msg.Bailf("index %d out of bounds for struct %s", index, s)
	}
	return s.slots[index]
}

func (s *Struct) Set(index int, value Object) {
	if index < 0 || index >= len(s.slots) {
		msg.Bailf("index %d out of bounds for struct %s", index, s)
	}
	s.slots[index] = value
}

func (s *Struct) ValueString(pmap Obset) string {
	return s.ReaderString(pmap)
}

func (s *Struct) ReaderString(pmap Obset) string {
	if pmap == nil {
		pmap = make(Obset)
		defer func() { delete(pmap, s) }()
	} else {
		if pmap[s] {
			return "[...]"
		}
	}
	pmap[s] = true

	result := bytes.NewBufferString("#S(")
	result.WriteString(s.stype.tag.ReaderString(pmap))
	for i, value := range s.slots {
		result.WriteRune(' ')
		result.WriteString(s.stype.slots[i].ReaderString(pmap))
		result.WriteRune(' ')
		result.WriteString(value.ReaderString(pmap))
	}
	result.WriteRune(')')
	return result.String()
}

func (s *Struct) String() string {
	return s.ValueString(nil)
}

func (s *Struct) Copy() *Struct {
	newslots := make([]Object, len(s.slots))
	copy(newslots, s.slots)
	s2 := newStructFromSlots(s.stype, newslots)
	return s2
}

func (s *Struct) IsNil() bool {
	return false
}

func (ob *Struct) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Struct) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Struct) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Struct) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func GetSlotIndex(tag *Symbol, slot *Symbol) int {
	stype := structTypeMap[tag]
	return stype.slotIndexes[slot]
}

func DefineStructType(tag *Symbol, doc *String, slots []*Symbol) {
	if _, exists := structTypeMap[tag]; exists {
		msg.Bail("struct type already defined:", tag)
	}
	stype := StructType{
		tag:         tag,
		doc:         doc,
		slotIndexes: map[*Symbol]int{},
		slots:       slots,
	}
	for i, sym := range slots {
		stype.slotIndexes[sym] = i
	}
	structTypeMap[tag] = &stype
}

func (s *Struct) Tag() *Symbol {
	return s.stype.tag
}

func GetStructType(tag *Symbol) Object {
	stype, exists := structTypeMap[tag]
	if !exists {
		msg.Bail("unknown struct type:", tag)
	}
	var desc Object = Nil
	for i := len(stype.slots) - 1; i >= 0; i-- {
		desc = Cons(stype.slots[i], desc)
	}
	desc = Cons(stype.doc, desc)
	desc = Cons(stype.tag, desc)
	return desc
}

// Create a new Struct object from a map of slot->value pairs.
//
func NewStruct(tag *Symbol, values VarMap) *Struct {
	stype, exists := structTypeMap[tag]
	if !exists {
		msg.Bail("unknown struct type:", tag)
	}
	slots := []Object{}
	for _, sym := range stype.slots {
		if value, exists := values[sym]; exists {
			slots = append(slots, value)
			delete(values, sym)
		} else {
			slots = append(slots, Nil)
		}
	}
	if vlen := len(values); vlen > 0 {
		pl := "s"
		if vlen == 1 {
			pl = ""
		}
		buf := bytes.NewBufferString("")
		for key, value := range values {
			buf.WriteString(fmt.Sprintf("(%v %s)",
				key, value.ReaderString(nil)))
		}
		msg.Bailf("initialiser for struct %v has extra slot%s: %v",
			GetStructType(stype.tag), pl, buf)
	}
	return newStructFromSlots(stype, slots)
}

func newStructFromSlots(stype *StructType, slots []Object) *Struct {
	s := &Struct{stype: stype, slots: slots}
	//structCount++
	return s
}

// build a struct from the list in a #s(tag elem1 value1 ...) struct literal
func NewStructFromList(body Object) *Struct {
	tag := Pop(&body)
	if !IsSymbol(tag) {
		msg.Bail("struct tag in struct literal not a symbol:", tag)
	}
	tagSymbol := tag.(*Symbol)

	slots := VarMap{}
	var slotName *Symbol
	wantSlotname := true // alternates with slot name / value
	for IsPair(body) {
		if wantSlotname {
			slot := Pop(&body)
			if !IsSymbol(slot) {
				msg.Bail("slot name in struct literal "+
					"not a symbol:", slot)
			}
			slotName = slot.(*Symbol)
			if _, exists := slots[slotName]; exists {
				msg.Bail("duplicate slot name in"+
					" struct literal:", slotName)
			}
		} else {
			value := Pop(&body)
			slots[slotName] = value
		}
		wantSlotname = !wantSlotname
	}
	if !wantSlotname {
		msg.Bail("slot name without value in struct literal:", slotName)
	}
	return NewStruct(tagSymbol, slots)
}

func Init_struct() {
	//fmt.Println("lob/struct.init")
}
