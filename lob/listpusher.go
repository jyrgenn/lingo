// General helper functions on object level
package lob

import "../msg"

// Have a data structure for efficiently pushing elements to the *end* of a
// list. As exercised so tediously often by hand everywhere in the past, keep a
// pointer to the last pair of the list to have the push operation in constant
// time. The tediousity is mainly in distiguishing the case of the empty list
// (in which case we obviously *don't* have a pointer to the last pair in the
// list) from a list already containing at least a few elements.

type ListPusher struct {
	theList  Object // the list being built
	lastPair *Pair  // pointer to the last pair (or nil)
}

// initialize and return a ListPusher with an empty list
func NewListPusher() *ListPusher {
	return &ListPusher{theList: Nil, lastPair: nil}
}

// push an object to the end of the ListPusher's list
func (lp *ListPusher) Push(ob Object) {
	newPair := Cons(ob, Nil).(*Pair)
	if lp.lastPair == nil {
		lp.theList = newPair
	} else {
		lp.lastPair.cdr = newPair
	}
	lp.lastPair = newPair
}

// add an object as the last cdr of the ListPusher's list
func (lp *ListPusher) AddEnd(ob Object) {
	if lp.theList.IsNil() {
		lp.theList = ob
	} else {
		lp.lastPair.cdr = ob
	}
}

// return the ListPusher's list
func (lp *ListPusher) List() Object {
	return lp.theList
}

//
func (lp *ListPusher) IsEmpty() bool {
	return lp.lastPair == nil
}

func (lp *ListPusher) Type() Obtype { // one of the constants above
	return LIST_COLLECTOR
}

// the Obset passed here contains the objects already being printed in
// this recursive descent, to recognize and handle printing of cyclic
// data structures
func (lp *ListPusher) ValueString(obs Obset) string {
	return "#<list-collector " +
		Cons(lp.theList, Cons(lp.lastPair, Nil)).ValueString(obs) +
		">"
}
func (lp *ListPusher) ReaderString(obs Obset) string {
	return lp.ValueString(obs)
}

func (lp *ListPusher) IsNil() bool {
	return false
}
func (lp *ListPusher) Car() Object {
	msg.Bail("Car on non-pair:", lp)
	return nil
}
func (lp *ListPusher) Cdr() Object {
	msg.Bail("Cdr on non-pair:", lp)
	return nil
}
func (lp *ListPusher) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", lp)
	return nil, nil
}
func (lp *ListPusher) Rplacd(ob Object) {
	msg.Bail("Cdr on non-pair:", lp)
}
func (lp *ListPusher) Equal(ob Object) bool {
	switch other := ob.(type) {
	case *ListPusher:
		return lp.theList.Equal(other.theList) &&
			lp.lastPair.Equal(other.lastPair)
	}
	return false
}
func (lp *ListPusher) Eq(ob Object) bool {
	return lp == ob
}

// EOF
