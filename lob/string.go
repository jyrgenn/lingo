// String object type
package lob

import (
	//"fmt"
	"bytes"
	"strconv"
	"unicode/utf8"

	"../msg"
	"../olu"
)

var stringCount = 0

func NStrings() int {
	return stringCount
}

type String struct {
	s   string
	len int
}

func (s *String) Remove(item Object) Object {
	return s.Delete(item)
}

func (s *String) Delete(item Object) Object {
	var to_delete rune

	switch chob := item.(type) {
	default:
		return s
	case *Char:
		to_delete = chob.c
	}
	new_s := []rune{}
	deleted := false
	for _, c := range s.s {
		if c != to_delete {
			new_s = append(new_s, c)
		} else {
			deleted = true
		}
	}
	if deleted {
		return NewString(string(new_s))
	}
	// don't allocate a new string if there is no change
	return s
}

func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func (s *String) NReverse() Object {
	return s.Reverse()
}

func (s *String) Reverse() Object {
	return NewString(reverse(s.s))
}

func (s *String) ToList() Object {
	var result Object = Nil
	runes := []rune(s.s)
	for i := len(runes) - 1; i >= 0; i-- {
		result = Cons(NewChar(runes[i]), result)
	}
	return result
}

func (s *String) Equal(ob Object) bool {
	return IsString(ob) && ob.(*String).s == s.s
}

func (s *String) Eq(ob Object) bool {
	return s.Equal(ob)
}

func (s *String) Cmp(other Comparable) int {
	if !IsString(other) {
		msg.Bailf("compare: string %s to other: %v (%v)",
			s.s, other, other.Type().Name())
	}
	ostr := other.(*String).s
	if s.s < ostr {
		return -1
	}
	if s.s > ostr {
		return 1
	}
	return 0
}

func (s *String) NotEqual(others Object) bool {
	have := make(map[string]bool)
	have[s.s] = true
	for IsPair(others) {
		arg := Pop(&others)
		switch arg.(type) {
		default:
			msg.Bailf("/=: string %s compared to %v (%s)",
				s.ReaderString(nil), arg.ReaderString(nil),
				arg.Type().Name())
		case *String:
			value := arg.(*String).s
			if have[value] {
				return false
			}
			have[value] = true
		}
	}
	return true
}

func (s *String) Type() Obtype {
	return STRING
}

func (s *String) ValueString(_ Obset) string {
	return s.s
}

func (s *String) ReaderString(_ Obset) string {
	return strconv.Quote(s.s)
}

func (s *String) String() string {
	return s.ReaderString(nil)
}

func (s *String) Length() int {
	if s.len < 0 {
		s.len = utf8.RuneCountInString(s.s)
	}
	return s.len
}

func (s *String) Element(index int) Object {
	if index >= len(s.s) {
		return nil
	}
	return Object(NewChar(rune(s.s[index])))
}

func (s *String) SetElement(index int, value Object) {
	if index >= len(s.s) {
		msg.Bailf("invalid index %d for string %v", index, s)
	}
	if IsChar(value) {
		var buf bytes.Buffer
		i := 0 // index of rune in string, not position
		// in array
		for _, r := range s.s {
			if i == index {
				buf.WriteRune(value.(*Char).c)
			} else {
				buf.WriteRune(r)
			}
			i++
		}
		s.s = buf.String()
	} else {
		msg.Bailf("setelt: cannot set string element to object type %s",
			value.Type().Name())
	}
}

func (s *String) SubSequence(start, end int) Object {
	return NewString(olu.RuneSubstr(s.s, start, end))
}

func (s *String) Copy() Object {
	// equal strings are indistinguishable anyway, so we can return the same
	// string
	return s
}

func (s *String) Position(item Object, fromEnd bool, start, end int) int {
	if !IsChar(item) {
		msg.Bail("position: item is not char:", item)
	}
	return olu.StringPosition(s.s, item.(*Char).c, fromEnd, start, end)
}

func (s *String) IsNil() bool {
	return false
}

func (ob *String) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *String) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *String) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *String) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func NewString(s string) *String {
	//stringCount++

	return &String{s: s, len: -1}
}

func Init_string() {
	//fmt.Println("lob/string.init")
}
