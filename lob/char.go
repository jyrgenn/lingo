// Character object type
package lob

import (
	"fmt"
	"unicode"
	"unicode/utf8"

	"../msg"
)

var charCount = 0

func NChars() int {
	return stringCount
}

type Char struct {
	c rune
}

func (ch *Char) Type() Obtype {
	return CHAR
}

func (ch *Char) Equal(ob Object) bool {
	return ch.Eq(ob)
}

func (ch *Char) Eq(ob Object) bool {
	switch o := ob.(type) {
	case *Char:
		return o.c == ch.c
	}
	return false
}

func (ch *Char) Cmp(other Comparable) int {
	if !IsChar(other) {
		msg.Bailf("compare: char %s to other: %v (%v)",
			ch.ReaderString(nil), other, other.Type().Name())
	}
	och := other.(*Char).c
	if ch.c < och {
		return -1
	}
	if ch.c > och {
		return 1
	}
	return 0
}

func (ch *Char) NotEqual(others Object) bool {
	have := make(map[rune]bool)
	have[ch.c] = true
	for IsPair(others) {
		arg := Pop(&others)
		switch arg.(type) {
		default:
			msg.Bailf("char %s compared to %v (%s)",
				ch.ReaderString(nil), arg.ReaderString(nil),
				arg.Type().Name())
		case *Char:
			value := arg.(*Char).c
			if have[value] {
				return false
			}
			have[value] = true
		}
	}
	return true
}

func (ch *Char) Value() rune {
	return ch.c
}

var charMap = map[rune]string{
	' ':    "Space",
	'\n':   "Newline",
	'\t':   "Tab",
	'\f':   "Page",
	'\177': "Rubout",
	'\r':   "Return",
	'\010': "Backspace",
	'\a':   "Bell",
}

func (ch *Char) ReaderString(_ Obset) string {
	if name, ispresent := charMap[ch.c]; ispresent {
		return "#\\" + name
	}
	if unicode.IsGraphic(ch.c) {
		return "#\\" + string(ch.c)
	}
	if ch.c < 0x100 {
		return fmt.Sprintf("#\\x%02X", ch.c)
	} else if ch.c < 0x10000 {
		return fmt.Sprintf("#\\u%04X", ch.c)
	} else {
		return fmt.Sprintf("#\\U%08X", ch.c)
	}
}

func (ch *Char) String() string {
	return ch.ReaderString(nil)
}

func (ch *Char) ValueString(_ Obset) string {
	return string(ch.c)
}

func (ch *Char) IsNil() bool {
	return false
}

func (ob *Char) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Char) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Char) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Char) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func NewChar(c rune) *Char {
	//charCount++
	if utf8.ValidRune(c) {
		return &Char{c: c}
	} else {
		msg.Bailf("code 0x%X is not a valid Unicode code point",
			uint32(c))
		return nil
	}
}

func Init_char() {
	//fmt.Println("lob/char.init")
}

// EOF
