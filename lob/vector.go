// Vector object type
package lob

import (
	"bytes"

	"../msg"
)

var vectorCount = 0

func NVectors() int {
	return vectorCount
}

type Vector struct {
	vec []Object
}

func (v *Vector) Type() Obtype {
	return VECTOR
}

func (v *Vector) IsSpecial() bool {
	return false
}

func (v *Vector) Call(nargs int, args Object) Object {
	arglist := args.(Sequence)
	if nargs == 0 {
		msg.Bail("calling vector " + v.ValueString(nil) +
			" needs at least an index argument")
		return nil
	}

	index := NumberArg(arglist.Car(), "vector index")
	switch nargs {
	case 1:
		return v.Get(int(index))
	case 2:
		value := arglist.Cdr().Car()
		v.Set(int(index), value)
		return value
	default:
		msg.Bail("too many arguments for vector " +
			v.ValueString(nil) + "; args are " +
			arglist.ValueString(nil))
	}
	return nil
}

func (v *Vector) Deftype() string {
	return "vector"
}

func (v *Vector) Synopsis() string {
	return "(VECTOR INDEX &optional VALUE) => value"
}

func (v *Vector) Docstring() string {
	return `Return the value of the VECTOR slot at INDEX.
With optional VALUE, set the value.`
}

func (v *Vector) Delete(item Object) Object {
	return v.Remove(item)
}

func (v *Vector) Remove(item Object) Object {
	vlen := v.Length()
	new_v := []Object{}
	for i := 0; i < vlen; i++ {
		if !item.Eq(v.vec[i]) {
			new_v = append(new_v, v.vec[i])
		}
	}
	return NewVectorInit(new_v, false)
}

func (v *Vector) NReverse() Object {
	length := len(v.vec)
	half := length / 2
	for i := 0; i < half; i++ {
		v.vec[i], v.vec[length-1-i] =
			v.vec[length-1-i], v.vec[i]
	}
	return v
}

func (v *Vector) Reverse() Object {
	length := len(v.vec)
	newvec := make([]Object, length)
	for i, val := range v.vec {
		newvec[length-1-i] = val
	}
	return NewVectorInit(newvec, false)
}

func (v *Vector) Equal(ob Object) bool {
	var v2 *Vector
	switch vec := ob.(type) {
	case *Vector:
		v2 = vec
	default:
		return false
	}
	if v == v2 {
		return true
	}
	if len(v.vec) != len(v2.vec) {
		return false
	}
	for i, val := range v.vec {
		if !val.Equal(v2.vec[i]) {
			return false
		}
	}
	return true
}

func (v *Vector) Eq(ob Object) bool {
	return v == ob
}

func (v *Vector) Get(index int) Object {
	if index < 0 || index >= len(v.vec) {
		msg.Bailf("index %d out of bounds for vector %v", index, v)
	}
	return v.vec[index]
}

func (v *Vector) Set(index int, value Object) {
	if index < 0 || index >= len(v.vec) {
		msg.Bailf("index %d out of bounds for vector %v", index, v)
	}
	v.vec[index] = value
}

func (v *Vector) ValueString(pmap Obset) string {
	return v.ReaderString(pmap)
}

func (v *Vector) ReaderString(pmap Obset) string {
	if pmap == nil {
		pmap = make(Obset)
	} else {
		if pmap[v] {
			return "[...]"
		}
	}
	pmap[v] = true
	defer func() { delete(pmap, v) }()

	result := bytes.NewBufferString("#(")
	for i, value := range v.vec {
		if i > 0 {
			result.WriteString(" ")
		}
		result.WriteString(value.ReaderString(pmap))
	}
	result.WriteString(")")
	return result.String()
}

func (v *Vector) String() string {
	return v.ValueString(nil)
}

func (v *Vector) Length() int {
	return len(v.vec)
}

func (v *Vector) Element(index int) Object {
	if index >= len(v.vec) {
		return nil
	}
	return v.vec[index]
}

func (v *Vector) SetElement(index int, value Object) {
	if index >= len(v.vec) {
		msg.Bailf("invalid index %d for vector %v", index, v)
	}
	v.vec[index] = value
}

func (v *Vector) SubSequence(start, end int) Object {
	if end < 0 {
		end = len(v.vec)
	}
	return NewVectorInit(v.vec[start:end], true)
}

func (v *Vector) Copy() Object {
	return NewVectorInit(v.vec, true)
}

func (v *Vector) Position(item Object, fromEnd bool, start, end int) int {
	result := -1
	for pos, elem := range v.vec {
		if end >= 0 && pos >= end {
			break
		}
		if elem.Eq(item) && pos >= start {
			result = pos
			if !fromEnd {
				break
			}
		}
	}
	return result
}

func (v *Vector) ToList() Object {
	var result Object = Nil
	for i := len(v.vec) - 1; i >= 0; i-- {
		result = Cons(v.vec[i], result)
	}
	return result
}

func (v *Vector) IsNil() bool {
	return false
}

func (ob *Vector) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Vector) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Vector) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Vector) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func NewVector(size int, fill Object) *Vector {
	//vectorCount++
	v := &Vector{vec: make([]Object, size)}
	for i := 0; i < size; i++ {
		v.vec[i] = fill
	}
	return v
}

// Create a new Vector object from an existing slice of Objects. It must_copy is
// true, this slice must be copied for the new Vector. This is the case for a
// subsequence and Copy, because in this case the slice refers to the original
// Vector, which may be modified afterwards.
func NewVectorInit(elems []Object, must_copy bool) *Vector {
	//vectorCount++
	var v *Vector
	if must_copy {
		v = &Vector{vec: make([]Object, len(elems))}
		copy(v.vec, elems)
	} else {
		v = &Vector{vec: elems}
	}
	return v
}

func NewVectorList(elems Object, len int) *Vector {
	if len < 0 {
		len = length(elems)
	}
	v := NewVector(len, Nil)
	for i := 0; IsPair(elems); i++ {
		elem := Pop(&elems)
		v.vec[i] = elem
	}
	return v
}

func Init_vector() {
	//fmt.Println("lob/vector.init")
}
