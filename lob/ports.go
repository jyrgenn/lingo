// Port object type, including network ports
package lob

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"

	"github.com/chzyer/readline"

	"../dep"
	"../msg"
)

var (
	optInput, optOutput, optIo, optDefault, optIfExists, optError,
	optNewVersion, optRename, optRenameAndDelete, optOverwrite, optAppend,
	optSupersede, optNewest, optCreate, optIfDoesNotExist, optProbe *Symbol
)

var portCount = 0

func NPorts() int {
	return portCount
}

const (
	FILE Ptype = 0 + iota
	STR
	BUF
	PIPE
	SOCK
	OTHER
)

type Ptype int8

var typeName = [...]string{
	"FILE",
	"STR",
	"BUF",
	"PIPE",
	"SOCK",
	"OTHER",
}

type Port struct {
	isInput       bool
	isOutput      bool
	isOpen        bool
	isInteractive bool
	ptype         Ptype
	name          string
	rw            *bufio.ReadWriter
	buf           *bytes.Buffer
	file          interface{}
	readline      *readline.Instance
	prompt        Object
}

var openPorts map[*Port]bool = map[*Port]bool{}

func closeOpenPorts() {
	for port := range openPorts {
		port.Close()
	}
}

func (p *Port) IsInput() bool {
	return p.isInput
}
func (p *Port) IsOutput() bool {
	return p.isOutput
}
func (p *Port) IsOpen() bool {
	return p.isOpen
}

func (p *Port) IsFilePort() bool {
	return p.ptype == FILE
}

func (p *Port) dirString() (result string) {
	if !p.isOpen {
		result += "["
	}
	if p.isInput {
		result += "i"
	}
	if p.isOutput {
		result += "o"
	}
	if !p.isOpen {
		result += "]"
	}
	return result
}

func (p *Port) Equal(ob Object) bool {
	return p.Eq(ob)
}

func (p *Port) Eq(ob Object) bool {
	return p == ob
}

func (p *Port) Type() Obtype {
	return PORT
}

func (p *Port) ReaderString(_ Obset) string {
	var typename string
	if p.ptype < 0 || p.ptype > OTHER {
		typename = fmt.Sprintf("type=%d", p.ptype)
	} else {
		typename = typeName[p.ptype]
	}
	return fmt.Sprintf("#<port:%s:%v:%v>",
		p.dirString(), typename, p.name)
}

func (p *Port) PType() Object {
	return Intern(typeName[p.ptype])
}

func (p *Port) String() string {
	return p.ReaderString(nil)
}

func (p *Port) ValueString(_ Obset) string {
	return p.ReaderString(nil)
}

func (p *Port) IsNil() bool {
	return false
}

func (p *Port) Name() string {
	return p.name
}

func (ob *Port) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Port) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Port) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Port) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func (p *Port) GetPrompt() Object {
	return p.prompt
}

func (p *Port) GetPromptString() string {
	if p.prompt != nil {
		return p.prompt.ValueString(nil)
	} else {
		return ""
	}
}

func (p *Port) SetPrompt(prompt Object) {
	p.prompt = prompt
}

func (p *Port) SetPromptString(prompt string) {
	p.prompt = NewString(prompt)
}

func (p *Port) assertOpenIn() {
	if !p.isOpen {
		msg.Bailf("port is closed: %v\n", p)
	}
	if !p.isInput {
		msg.Bailf("not an input port: %v\n", p)
	}
}

// Make sure there is a buffered line present, or return a (non-nil) error. If
// the port is interactive, read the line using readline.
func (p *Port) assureLineBuf() (err error) {
	p.assertOpenIn()
	if p.buf == nil || p.buf.Len() <= 0 {
		var err error
		var line []byte
		var input string
		if p.isInteractive {
			promptString := ""
			if p.prompt != nil {
				prompt := p.prompt
				if IsFunction(p.prompt) {
					prompt = EvalCurEnv(Cons(p.prompt, Nil))
				}
				promptString = prompt.ValueString(nil)
				p.readline.SetPrompt(promptString)
			}
			input, err = p.readline.Readline()
			if err == nil {
				if input != "" {
					p.readline.SaveHistory(input)
				}
				// The newline character is necessary for the
				// Reader to see a new line has begun. (Duh! but
				// I didn't realize that so soon.)
				line = append([]byte(input), byte('\n'))
			}
		} else {
			line, err = p.rw.ReadBytes('\n')
		}
		if err != nil && err != io.EOF {
			if p.buf == nil {
				p.buf = bytes.NewBuffer([]byte{})
			}
			return err
		}
		p.buf = bytes.NewBuffer(line)
	}
	return nil
}

func (p *Port) ReadRune() (r rune, size int, err error) {
	if err = p.assureLineBuf(); err != nil {
		return 0, 0, err
	}
	return p.buf.ReadRune()
}

func (p *Port) ReadLine() (line []byte, err error) {
	if err = p.assureLineBuf(); err != nil {
		return nil, err
	}
	return p.buf.ReadBytes('\n')
}

func (p *Port) ReadLineObj() (Object, error) {
	s, err := p.ReadLine()
	if err != nil {
		return nil, err
	}
	return NewString(string(s)), err
}

func (p *Port) Write(sargs ...string) {
	if !p.isOutput {
		msg.Bailf("not an output port: %v\n", p)
	}
	if !p.isOutput {
		msg.Bailf("not an output port: %v\n", p)
	}
	// msg.Debug("port.Write using rw", sargs)
	for _, s := range sargs {
		_, err := p.rw.Write([]byte(s))
		if err != nil {
			break
		}
	}
	p.rw.Flush()
}

func (p *Port) Close() bool {
	if !p.isOpen {
		return false
	}
	if p.isOutput {
		p.rw.Flush()
	}
	if p.isInteractive {
		p.readline.Close()
	}
	if p.file != nil {
		switch cl := p.file.(type) {
		case io.Closer:
			cl.Close()
		}
	}
	p.isOpen = false
	delete(openPorts, p)
	return true
}

func NewStringPort(s, label string) *Port {
	//portCount++
	return &Port{
		isInput:       true,
		isOutput:      false,
		isOpen:        true,
		isInteractive: false,
		ptype:         STR,
		name:          label,
		rw: bufio.NewReadWriter(bufio.NewReader(
			bytes.NewBufferString(s)), nil),
		buf:      nil,
		file:     nil,
		readline: nil,
		prompt:   Nil,
	}
}

func NewBufferPort() *Port {
	//portCount++

	var theBuffer bytes.Buffer
	return &Port{
		isInput:       false,
		isOutput:      true,
		isOpen:        true,
		isInteractive: false,
		ptype:         BUF,
		name:          "*buf*",
		rw: bufio.NewReadWriter(nil, bufio.NewWriter(
			&theBuffer)),
		buf:      &theBuffer,
		file:     nil,
		readline: nil,
		prompt:   Nil,
	}
}

func (p *Port) GetString() string {
	if p.ptype != BUF {
		msg.Bail("port is not buffer type:", p)
	}
	return p.buf.String()
}

func NewOutPort(out io.Writer, name string) (p *Port) {
	//portCount++
	return &Port{
		isInput:       false,
		isOutput:      true,
		isOpen:        true,
		isInteractive: false,
		ptype:         OTHER,
		name:          name,
		rw:            bufio.NewReadWriter(nil, bufio.NewWriter(out)),
		buf:           nil,
		file:          out,
		readline:      nil,
		prompt:        Nil,
	}
}

func NewInPort(in io.Reader, name string) *Port {
	//portCount++
	p := &Port{
		isInput:       true,
		isOutput:      false,
		isOpen:        true,
		isInteractive: false,
		ptype:         OTHER,
		name:          name,
		rw:            bufio.NewReadWriter(bufio.NewReader(in), nil),
		buf:           nil,
		file:          in,
		readline:      nil,
		prompt:        Nil}
	openPorts[p] = true
	return p
}

func NewInteractivePort(in *Port, prompt Object, historyFileName string) *Port {
	p := NewInPort(in.rw, "*interactive*")
	p.isInteractive = true
	p.prompt = prompt
	p.readline, _ = readline.New(prompt.ValueString(nil))

	if historyFileName != "" {
		p.readline.SetHistoryPath(historyFileName)
	}
	return p
}

func NewFilePort(fname string, keymap Obmap) *Port {
	//fmt.Printf("new file port %s, %v\n", fname, keymap)
	//portCount++
	var mode int
	var present, retnil bool
	var direction, ifexists, ifdoesnotexist Object
	port := new(Port)

	direction, present = keymap[OptDirection]
	if present {
		switch direction {
		case OptInput:
			port.isInput = true
			mode = os.O_RDONLY
		case OptOutput:
			port.isOutput = true
			mode = os.O_WRONLY | os.O_CREATE | os.O_TRUNC
		case OptIo:
			port.isInput = true
			port.isOutput = true
			mode = os.O_RDWR
		case Nil:
			retnil = true
		case OptProbe:
			msg.Bail("file: :probe not supported")
		default:
			msg.Bailf("file: invalid %v: %v",
				OptDirection, direction)
		}
	} else {
		port.isInput = true
		mode = os.O_RDONLY
	}

	ifexists, present = keymap[OptIfExists]
	if present {
		switch ifexists {
		case OptError:
			if port.isOutput {
				mode |= os.O_CREATE | os.O_EXCL
			}
		case OptNewVersion, OptOverwrite, OptSupersede:
			if port.isOutput {
				mode |= os.O_CREATE | os.O_TRUNC
			}
		case OptAppend:
			if port.isOutput {
				mode &^= os.O_CREATE | os.O_TRUNC
				mode |= os.O_APPEND
			}
		case Nil:
			retnil = true
		case nil:
			if port.isOutput {
				mode |= os.O_CREATE | os.O_TRUNC
			}
		default:
			msg.Bailf("file: invalid %v: %v",
				OptIfExists, ifexists)
		}
	}
	ifdoesnotexist, present = keymap[OptIfDoesNotExist]
	if present {
		switch ifdoesnotexist {
		case OptError:
			mode &^= os.O_CREATE
		case OptCreate:
			mode |= os.O_CREATE
		case Nil:
			retnil = true
		case nil:
			if port.isInput {
				mode &^= os.O_CREATE
			}
		default:
			msg.Bailf("file: invalid %v: %v",
				OptIfDoesNotExist, ifdoesnotexist)
		}
	}
	file, err := os.OpenFile(fname, mode, 0666)
	if err != nil {
		if retnil {
			return nil
		}
		msg.Bail("open file:", err.Error())
	}
	port.ptype = FILE
	port.name = fname
	port.isOpen = true

	var reader *bufio.Reader = nil
	var writer *bufio.Writer = nil
	if port.isInput {
		reader = bufio.NewReader(file)
	}
	if port.isOutput {
		writer = bufio.NewWriter(file)
	}
	port.rw = bufio.NewReadWriter(reader, writer)

	port.file = file
	return port
}

// return the current file offset of the port, or -1 if it is not a file, or not
// at all
func (p *Port) Position() int64 {
	if p.file == nil {
		fmt.Printf("%v not a file port, apparently\n", p)
		return -1
	}
	switch file := p.file.(type) {
	case *os.File:
		ret, err := file.Seek(0, os.SEEK_CUR)
		if err != nil {
			msg.Bailf("error in Seek on %v: %s", p, err.Error())
		}
		fmt.Printf("%v returning %d\n", p, ret)
		return ret
	}
	fmt.Printf("%v.file not nil, but not os.File? %#v\n", p, p.file)
	return -1
}

func (p *Port) Reader() io.Reader {
	return p.rw
}

func (p *Port) Writer() io.Writer {
	return p.rw
}

func Stdin() *Port {
	return Coerce(SysStdin.Value(), PORT, "sys:stdin").(*Port)
}

func StdinReader() io.Reader {
	return Stdin().rw
}

func Stdout() *Port {
	return Coerce(SysStdout.Value(), PORT, "sys:stdout").(*Port)
}

func StdoutWriter() io.Writer {
	return Stdout().rw
}

func Stderr() *Port {
	return Coerce(SysStderr.Value(), PORT, "sys:stderr").(*Port)
}

func StderrWriter() io.Writer {
	return Stderr().rw
}

func isTerminal(f *os.File) bool {
	return readline.IsTerminal(int(f.Fd()))
}

func Init_ports() {
	//fmt.Println("lob/ports.init")

	// Set the line input editor on stdin if both stdin and stdout are
	// terminals.
	if isTerminal(os.Stdin) && isTerminal(os.Stdout) {
		in := NewInteractivePort(NewInPort(os.Stdin,
			"*interactive stdin*"), NewString("> "),
			dep.LingoHistoryFileName)
		SysStdin.DefVar(in)
	} else {
		SysStdin.DefVar(NewInPort(os.Stdin, "*stdin*"))
	}
	SysStdout.DefVar(NewOutPort(os.Stdout, "*stdout*"))
	SysStderr.DefVar(NewOutPort(os.Stderr, "*stderr*"))
	dep.StdinReader = StdinReader
	dep.StdoutWriter = StdoutWriter
	dep.StderrWriter = StderrWriter
	dep.AtExit(closeOpenPorts)
}
