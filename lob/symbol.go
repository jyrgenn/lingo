// Symbol object type
package lob

import (
	"bytes"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"unicode"

	"../dep"
	"../msg"
)

type Symbol struct {
	name         string
	function     Object
	props        VarMap
	readerString string // in case of needsBars
	immutable    bool   // value must not be changed
	trace        bool
	needsBars    bool // needs |quoting bars| for printing
}

// all symbols
var symbolMap = map[string]*Symbol{}

// number to try for next gensym-generated symbol
var gensymCounter uint64 = 0
var gensymPrefix = "G#"

func Gensym(stem string) *Symbol {
	if stem == "" {
		stem = gensymPrefix
	}
	sym := NewSymbol(fmt.Sprintf("%s%d", stem, gensymCounter), false)
	gensymCounter++
	return sym
}

var gentempCounter uint64 = 0
var gentempPrefix = "T"

func Gentemp(prefix string) *Symbol {
	if prefix == "" {
		prefix = gentempPrefix
	}
	for {
		name := fmt.Sprintf("%s%d", prefix, gentempCounter)
		if _, exists := symbolMap[name]; !exists {
			return Intern(name)
		}
		gentempCounter++
	}
	return nil
}

func NSymbols() int {
	return len(symbolMap)
}

// Object methods

func (sym *Symbol) Type() Obtype {
	return SYMBOL
}

func (sym *Symbol) Equal(ob Object) bool {
	return sym == ob
}

func (sym *Symbol) Eq(ob Object) bool {
	return sym == ob
}

func (sym *Symbol) Remove(item Object) Object {
	if sym != Nil {
		msg.Bail("remove on non-sequence:", sym)
	}
	return Nil
}

func (sym *Symbol) Delete(item Object) Object {
	if sym != Nil {
		msg.Bail("delete on non-sequence:", sym)
	}
	return Nil
}

func (sym *Symbol) Copy() Object {
	if sym != Nil {
		msg.Bail("copy-seq on non-sequence:", sym)
	}
	return Nil
}

func (sym *Symbol) Cmp(other Comparable) int {
	if !IsSymbol(other) {
		msg.Bailf("compare: symbol %v to other: %v (%v)",
			sym, other, other.Type().Name())
	}
	oname := other.(*Symbol).name
	if sym.name < oname {
		return -1
	}
	if sym.name > oname {
		return 1
	}
	return 0
}

func (sym *Symbol) NotEqual(others Object) bool {
	have := make(map[string]bool)
	have[sym.name] = true
	for IsPair(others) {
		arg := Pop(&others)
		switch arg.(type) {
		default:
			msg.Bailf("/=: symbol %s compared to %v (%s)",
				sym.ReaderString(nil), arg.ReaderString(nil),
				arg.Type().Name())
		case *Symbol:
			value := arg.(*Symbol).name
			if have[value] {
				return false
			}
			have[value] = true
		}
	}
	return true
}

func (sym *Symbol) ValueString(_ Obset) string {
	return sym.name
}

func (sym *Symbol) ReaderString(_ Obset) string {
	if sym.needsBars {
		if len(sym.readerString) == 0 {
			var buf bytes.Buffer
			buf.WriteRune('|')
			for _, r := range sym.name {
				if r == '|' || r == '\\' {
					buf.WriteRune('\\')
				}
				buf.WriteRune(r)
			}
			buf.WriteRune('|')
			sym.readerString = buf.String()
		}
		return sym.readerString
	} else {
		return sym.name
	}
}
func (sym *Symbol) String() string {
	if sym == nil {
		return UndefName
	}
	return sym.ReaderString(nil)
}

func (sym *Symbol) Dump() string {
	return fmt.Sprintf("Symbol{ \"%s\", %v, %v, %v, %v, %v, \"%s\"}",
		sym.name, sym.function, sym.props, sym.immutable, sym.trace,
		sym.needsBars, sym.readerString)
}

// Symbol methods

func (sym *Symbol) Function() Object {
	if sym.trace {
		fmt.Printf("sym %s get function: %v\n",
			sym.ReaderString(nil), sym.function)
	}
	return sym.function
}

func (sym *Symbol) Value() Object {
	value := get(sym)
	if sym.trace {
		fmt.Printf("sym %s get value: %v\n",
			sym.ReaderString(nil), value)
	}
	return value
}

func (sym *Symbol) NeedValue() Object {
	value := get(sym)
	if sym.trace {
		fmt.Printf("sym %s need value: %v\n",
			sym.ReaderString(nil), value)
	}
	if value == nil {
		if sym.trace {
			dep.PrintStack()
		}
		msg.Bailf("unbound variable %s", sym.ReaderString(nil))
	}
	return value
}

func (sym *Symbol) NeedFunction() Object {
	if sym.trace {
		fmt.Printf("sym %s need function: %v\n",
			sym.ReaderString(nil), sym.function)
	}
	if sym.function == nil {
		msg.Bailf("symbol %s has no function value",
			sym.ReaderString(nil))
	}
	return sym.function
}

func (sym *Symbol) haveProps() VarMap {
	if sym.props == nil {
		sym.props = make(VarMap)
	}
	return sym.props
}

func (sym *Symbol) Properties() Object {
	var result Object = Nil
	if sym.props != nil {
		for key, value := range sym.props {
			result = Cons(Cons(key, value), result)
		}
	}
	return result
}

func (sym *Symbol) GetProp(prop *Symbol) Object {
	val, found := sym.haveProps()[prop]
	if !found {
		val = Nil
	}
	if sym.trace {
		fmt.Printf("sym %s get prop %v: %v\n",
			sym.ReaderString(nil), prop, val)
	}
	return val
}

func (sym *Symbol) PutProp(prop *Symbol, value Object) Object {
	sym.haveProps()
	if value == nil {
		msg.Bailf("PutProp: value for symbol %v:%v undefined!",
			sym, prop)
	}
	if value.IsNil() {
		delete(sym.props, prop)
	} else {
		sym.props[prop] = value
	}
	if sym.trace {
		fmt.Printf("sym %s put prop %v: %v\n",
			sym.ReaderString(nil), prop, value)
	}
	return value
}

func (sym *Symbol) DefVar(ob Object) {
	if sym.immutable {
		msg.Bail("attempt to defvar immutable variable",
			sym.ReaderString(nil))
	}
	defvar(sym, ob)
}

func (sym *Symbol) Bind(ob Object) {
	if sym.immutable {
		msg.Bail("attempt to bind immutable variable", sym)
	}
	if sym.trace {
		fmt.Printf("sym %s bind value: %v\n", sym.ReaderString(nil), ob)
	}
	envbind(sym, ob)
}

// return previous value
func (sym *Symbol) SetValue(ob Object) Object {
	if sym.immutable {
		msg.Bailf("tried to set value of constant symbol %s",
			sym.ReaderString(nil))
	}
	if sym.trace {
		fmt.Printf("sym %s set value: %v\n", sym.ReaderString(nil), ob)
	}
	return set(sym, ob)
}

func (sym *Symbol) SetFunction(fun Object) {
	if dep.IsBuiltin(sym.function) {
		msg.Warn("redefining builtin function", sym.ReaderString(nil))
	}
	sym.SetFunctionSilently(fun)
}

// have a function to set the function cell silently in all cases; used when
// restoring function bindings on leaving an flet context
func (sym *Symbol) SetFunctionSilently(fun Object) {
	if sym.trace {
		fmt.Printf("sym %s set function: %v\n",
			sym.ReaderString(nil), fun)
	}
	sym.function = fun
}

// Following are a number of methods that are suitable for lists, but not for
// symbols in general. But because Nil is a symbol as well as a list, we have to
// define them for all symbols. :-(

func (sym *Symbol) Length() int {
	if sym != Nil {
		msg.Bailf("length of a non-list: %s", sym.ReaderString(nil))
	}
	return 0
}

func (sym *Symbol) Element(index int) Object {
	if sym != Nil {
		msg.Bailf("elt of a non-list: %s", sym.ReaderString(nil))
	}
	return nil
}

func (sym *Symbol) SetElement(index int, value Object) {
	if sym == Nil {
		msg.Bailf("index %d out of bounds for list %v", index, sym)
	} else {
		msg.Bailf("setelt of a non-sequence: %s", sym.ReaderString(nil))
	}
}

func (sym *Symbol) SubSequence(start, end int) Object {
	if sym != Nil {
		msg.Bailf("subseq of a non-list: %s", sym.ReaderString(nil))
	}
	return Nil
}

func (sym *Symbol) Position(item Object, fromEnd bool, start, end int) int {
	if sym != Nil {
		msg.Bailf("position on a non-list: %s", sym.ReaderString(nil))
	}
	return -1
}

func (sym *Symbol) Reverse() Object {
	if sym != Nil {
		msg.Bailf("reverse of a non-sequence: %s",
			sym.ReaderString(nil))
	}
	return Nil
}

func (sym *Symbol) NReverse() Object {
	if sym != Nil {
		msg.Bailf("nreverse of a non-sequence: %s",
			sym.ReaderString(nil))
	}
	return Nil
}

func (sym *Symbol) Car() Object {
	if sym != Nil {
		msg.Bailf("car of a non-list: %s", sym.ReaderString(nil))
	}
	return Nil
}

func (sym *Symbol) Cdr() Object {
	if sym != Nil {
		msg.Bailf("cdr of a non-list: %s", sym.ReaderString(nil))
	}
	return Nil
}

func (sym *Symbol) Cxr() (Object, Object) {
	if sym != Nil {
		msg.Bailf("car/cdr of a non-list: %s", sym.ReaderString(nil))
	}
	return Nil, Nil
}

func (sym *Symbol) SetSysProp() *Symbol {
	sym.PutProp(SysSystem, T)
	return sym
}

func (sym *Symbol) SysProp() Object {
	return sym.GetProp(SysSystem)
}

func (sym *Symbol) IsNil() bool {
	return sym == Nil
}

func (sym *Symbol) IsImmutable() bool {
	return sym.immutable
}

func (sym *Symbol) MakeImmutable() *Symbol {
	sym.immutable = true
	return sym
}

func (ob *Symbol) Rplacd(_ Object) {
	msg.Bail("rplacd on non-pair:", ob)
}

func (sym *Symbol) ToList() Object {
	if sym == Nil {
		return Nil
	}
	msg.Bail("sequence-list on non-pair:", sym)
	return Nil
}

func (sym *Symbol) SetTrace(flag bool) {
	if flag {
		fmt.Printf("sym %s set trace: %v\n",
			sym.ReaderString(nil), flag)
	}
	sym.trace = flag
}

func (sym *Symbol) GetTrace() bool {
	if sym.trace {
		fmt.Printf("sym %s get trace: %v\n",
			sym.ReaderString(nil), sym.trace)
	}
	return sym.trace
}

// return true iff the symbol is a keyword, i.e. its name starts with a colon
func (sym *Symbol) IsKeyword() bool {
	return len(sym.name) > 0 && sym.name[0] == ':'
}

// return the parameter name for a keyword symbol, i.e. the symbol that has
// the same name, just without the colon
func (sym *Symbol) Keyword2Param() *Symbol {
	if !sym.IsKeyword() {
		panic(fmt.Errorf("called Keyword2Param() on non-keyword %s",
			sym.ReaderString(nil)))
	}
	return internIntern(sym.name[1:], false)
}

// return the keyword symbol for a keyword parameter name, i.e. the symbol with
// the same name, but a colon prepended
func (sym *Symbol) Param2Keyword() *Symbol {
	if sym.IsKeyword() {
		panic(fmt.Errorf("called Param2Keyword() on keyword %s",
			sym.ReaderString(nil)))
	}
	return internIntern(":"+sym.name, true)
}

type reverseS struct {
	sort.Interface
}

func (r reverseS) Less(i, j int) bool {
	return r.Interface.Less(j, i)
}

type symSlice []*Symbol

func (s symSlice) Len() int           { return len(s) }
func (s symSlice) Less(i, j int) bool { return s[i].name > s[j].name }
func (s symSlice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func Symbols() []*Symbol {
	result := symSlice{}
	for _, sym := range symbolMap {
		result = append(result, sym)
	}
	sort.Sort(result)
	return result
}

// determine if the symbol needs quoting bars when printed for reading back in
func needsBars(name string) bool {
	if name == "" {
		return true
	}
	if name == "." {
		return true
	}
	for index, r := range name {
		if index == 0 {
			// comma and octothorpe are fine inside of a symbol
			// name, but not at the beginning
			if strings.ContainsRune("#,", r) {
				return true
			}
		}
		if unicode.IsSpace(r) {
			return true
		}
		if !unicode.IsGraphic(r) {
			return true
		}
		// these are separator characters (must keep reader in sync to
		// match!)
		if strings.ContainsRune("|()\"';\\`", r) {
			return true
		}
	}
	_, err := strconv.ParseFloat(name, 64)
	if err == nil { // looks like a number
		return true
	}
	return false
}

// This function is (newly) separate from Intern (or internIntern, respectively)
// so we can now create uninterned symbols
//
func NewSymbol(name string, selfValue bool) *Symbol {
	sym := &Symbol{
		name:         name,
		function:     nil,
		props:        nil,
		readerString: "",
		immutable:    false,
		trace:        false,
		needsBars:    needsBars(name),
	}
	if selfValue {
		sym.DefVar(sym)
		sym.immutable = true
	}
	return sym
}

// "internal" Intern; if selfValue is true, the symbol gets itself assigned as
// the value *and* is immutable. This is used for keywords and for a number of
// other symbols, e.g. nil, t, lambda, quote, and quasiquote; see stdsyms.go.
func internIntern(name string, selfValue bool) *Symbol {
	sym, found := symbolMap[name]
	if found {
		if sym.trace {
			fmt.Printf("sym %s interned again\n", name)
		}
		return sym
	}
	sym = NewSymbol(name, selfValue)
	symbolMap[name] = sym
	//msg.Trace("Interned", sym)
	return sym
}

func Intern(name string) *Symbol {
	sym := internIntern(name,
		len(name) > 0 && name[0] == ':') // keyword --> selfValue
	return sym
}

// Intern a system symbol; set the system property and immediately assign a
// value if the value passed is not nil. (This is the Go nil, not the Lisp nil.)
func InternSys(name string, value Object) *Symbol {
	sym := Intern(name)
	if value != nil {
		sym.DefVar(value)
	}
	sym.PutProp(SysSystem, T)
	return sym
}

func Init_symbol() {
	//fmt.Println("lob/symbol.init")
}

// EOF
