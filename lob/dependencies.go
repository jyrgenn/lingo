// Dependencies on modules using object, to resolve circular dependencies
package lob

//import "fmt"

var Eval func(Object, *Environment) Object
var EvalCurEnv func(Object) Object
var EvalLevel func() int
var EvalCount func() int64

var MacroExpandForm func(body Object) Object

var Read func(source *Port) Object
var Load func(fname string, keywords Obmap) (result, found bool)

func Init_lob() {
	//fmt.Println("lob/lob.init")
}
