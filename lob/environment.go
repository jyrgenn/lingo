// Environment object type
package lob

import (
	"fmt"

	"../msg"
)

var Env *Environment // the current environment
var rootEnv *Environment

var envCount int = 0

type VarMap map[*Symbol]Object

type Environment struct {
	parent *Environment
	count  int
	level  int
	val    VarMap
}

func (env *Environment) Type() Obtype {
	return ENVIRON
}

func (env *Environment) Equal(ob Object) bool {
	return env.Eq(ob)
}

func (env *Environment) Eq(ob Object) bool {
	return ob == env
}

func (env *Environment) stringLead() string {
	return fmt.Sprintf("#<env%d[%d]", env.count, env.level)
}

func (env *Environment) ValueString(pmap Obset) string {
	return env.stringLead() + ">"
}

func (env *Environment) ReaderString(pmap Obset) string {
	if pmap == nil {
		pmap = make(Obset)
		defer func() { delete(pmap, env) }()
	} else {
		if pmap[env] {
			return "[...]"
		}
	}
	pmap[env] = true

	vars := env.allvarmap(false)
	result := env.stringLead()
	for variable, value := range vars {
		var valstring string
		if value == nil {
			valstring = UndefName
		} else {
			valstring = value.ReaderString(pmap)
		}
		result += variable.ReaderString(pmap) + "=" + valstring + ";"
	}
	result = result[:len(result)-1]
	return result + ">"
}

// Return a map containing all symbols and values; unless noparents is true,
// this includes those from the parent environments.
func (env *Environment) allvarmap(noparents bool) VarMap {
	vars := make(VarMap)
	for ; env != nil; env = env.parent {
		for sym, value := range env.val {
			vars[sym] = value
		}
		if noparents {
			break
		}
	}
	return vars
}

func (env *Environment) KeyMap(noparents bool) map[Key]Object {
	vars := make(map[Key]Object)
	for ; env != nil; env = env.parent {
		for sym, value := range env.val {
			vars[ob2key(sym)] = value
		}
		if noparents {
			break
		}
	}
	return vars
}

// Return the symbol/value map of the environment. This is not a copy! Modifying
// this map will modify the environment.
func (env *Environment) Varmap() VarMap {
	return env.val
}

func (env *Environment) Level() int {
	return env.level
}

func (env *Environment) Vars(noparents bool) Object {
	vars := env.allvarmap(noparents)
	var result Object = Nil
	for sym := range vars {
		result = Cons(sym, result)
	}
	return result
}

func (env *Environment) String() string {
	return env.ValueString(nil)
}

func (env *Environment) IsNil() bool {
	return false
}

func (ob *Environment) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Environment) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Environment) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Environment) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func defvar(sym *Symbol, ob Object) {
	// TODO: take this out
	if sym == nil {
		panic("symbol is nil in lob.defvar")
	}
	// TODO: take this out
	if rootEnv == nil {
		panic("rootEnv nil in lob.defvar")
	}
	// fmt.Printf("defvar %v to %v in %v\n", sym, ob, rootEnv)
	if rootEnv.val[sym] == nil {
		rootEnv.val[sym] = ob
	}
}

func envbind(sym *Symbol, ob Object) {
	// fmt.Printf("bind %v to %v in %v\n", sym, ob, Env)
	if ob == nil {
		delete(Env.val, sym)
	} else {
		Env.val[sym] = ob
	}
}

func set(sym *Symbol, ob Object) Object {
	// TODO: take this out
	if sym == nil {
		panic("symbol is nil in Set")
	}
	env := Env
	for env != nil {
		if env.val[sym] != nil {
			// fmt.Printf("set %v to %v in %v\n", sym, ob, env)
			prev := env.val[sym]
			env.val[sym] = ob
			return prev
		}
		env = env.parent
	}
	if ob != nil { // make makunbound work on unbound
		// variables
		msg.Warnf("setting unbound variable %v to %v", sym, ob)
		rootEnv.val[sym] = ob
	}
	return nil
}

func get(sym *Symbol) Object {
	// fmt.Printf("Get symbol %v from", sym)
	env := Env
	for env != nil {
		// fmt.Printf(" %v", env)
		value := env.val[sym]
		if value != nil {
			// fmt.Printf(" got %v\n", value);
			return value
		}
		env = env.parent
	}
	// fmt.Printf(" failed\n");
	return nil
}

func TheEnvironment() *Environment {
	return Env
}

func NewEnvironment(parent *Environment, values *Table) *Environment {
	level := 0
	if parent != nil {
		level = parent.level + 1
	}
	env := &Environment{
		parent: parent,
		count:  envCount,
		level:  level,
		val:    make(VarMap),
	}
	if values != nil {
		for key, value := range values.m {
			switch sym := key.(type) {
			case *Symbol:
				env.val[sym] = value
			default:
				msg.Bail("environment initializer table has "+
					"non-symbol key:", sym)
			}
		}
	}
	envCount++
	return env
}

func EnterEnvironment(parent_or_not ...*Environment) *Environment {
	parent := Env
	if len(parent_or_not) == 1 {
		parent = parent_or_not[0]
	}
	savedEnv := Env
	Env = NewEnvironment(parent, nil)
	return savedEnv
}

func (env *Environment) BackTo() {
	Env = env
}

func (env *Environment) SwitchTo() *Environment {
	previousEnv := Env
	Env = env
	return previousEnv
}

func Init_environment() {
	//fmt.Println("lob/environment.init")
	if rootEnv == nil {
		rootEnv = NewEnvironment(nil, nil)
	} else {
		panic(fmt.Sprintf("rootEnv already there: %s\n",
			rootEnv.ReaderString(nil)))
	}
	Env = rootEnv
}
