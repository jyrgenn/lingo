// Sequence object interface
package lob

//import "fmt"

// lists, strings, vectors
type Sequence interface {
	Object
	Length() int
	Element(index int) Object
	SetElement(index int, value Object)
	SubSequence(start, end int) Object
	Position(item Object, fromEnd bool, start, end int) int
	Reverse() Object
	NReverse() Object
	ToList() Object
	Delete(item Object) Object
	Remove(item Object) Object
	Copy() Object
}

func Init_sequence() {
	//fmt.Println("lob/sequence.init")
}
