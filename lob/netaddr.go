// Network address object type
package lob

import (
	"fmt"
	"net"
	"strings"

	"../msg"
)

type Netaddr struct {
	unixdomain bool
	name       string
	ipaddrs    []net.IP
	port       int
}

func (addr *Netaddr) Type() Obtype {
	return NETADDR
}

func (addr *Netaddr) Equal(ob Object) bool {
	var addr2 *Netaddr
	switch a := ob.(type) {
	case *Netaddr:
		addr2 = a
	default:
		return false
	}
	if len(addr.ipaddrs) != len(addr2.ipaddrs) {
		return false
	}
	for i, val := range addr.ipaddrs {
		if val.Equal(addr2.ipaddrs[i]) {
			return false
		}
	}
	return addr.unixdomain == addr2.unixdomain &&
		addr.name == addr2.name &&
		addr.port == addr2.port
}

func (addr *Netaddr) Eq(ob Object) bool {
	return ob == addr
}

func (addr *Netaddr) ValueString(_ Obset) string {
	return addr.name
}

func (addr *Netaddr) ReaderString(_ Obset) string {
	addrlist := fmt.Sprintf("%v", addr.ipaddrs)
	addrlist = strings.Replace(addrlist, " ", ",", -1)
	return fmt.Sprintf("#<A@%v%v:%v>", addr.name, addrlist, addr.port)
}

func (addr *Netaddr) String() string {
	return addr.ReaderString(nil)
}

func (addr *Netaddr) IsNil() bool {
	return false
}

func (ob *Netaddr) Cxr() (Object, Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Netaddr) Car() Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Netaddr) Cdr() Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Netaddr) Rplacd(_ Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func (addr *Netaddr) Port() int {
	return addr.port
}

func (addr *Netaddr) IPAddrs() []net.IP {
	return addr.ipaddrs
}

func NewNetaddr(host string, port string) *Netaddr {
	addr := &Netaddr{unixdomain: false, name: host, ipaddrs: nil, port: 0}
	if strings.Contains(host, "/") {
		addr.unixdomain = true
		return addr
	}
	addrs, err := net.LookupIP(host)
	if err != nil {
		msg.Bail("netaddr:", err)
	}
	addr.ipaddrs = addrs
	if port == "" {
		port = "0"
	}
	addr.port, err = net.LookupPort("", port)
	if err != nil {
		msg.Bail("netaddr:", err)
	}
	return addr
}

func Init_netaddr() {
	//fmt.Println("lob/netaddr.init")
}
