// Standard symbols
// These are symbols that we want to have in variables so we don't need to
// address them through their name in the Go code.
package lob

//import "fmt"

var (
	AndKey,
	AndOptional,
	AndRest,
	Call,
	CmdArgs,
	CurrentLoadFile,
	FuncDefinedIn,
	Function,
	VarDefinedIn,
	DocString,
	EvalSym,
	Lambda,
	LastEvalStats,
	LoadPaths,
	Nil,
	OptAllMatches,
	OptAppend,
	OptClient,
	OptCreate,
	OptDefault,
	OptDirection,
	OptDotMatchesNewline,
	OptError,
	OptIPv4,
	OptIPv6,
	OptIfDoesNotExist,
	OptIfExists,
	OptIgnoreCase,
	OptInput,
	OptInteractive,
	OptIo,
	OptMatchLimit,
	OptMultiLine,
	OptNewVersion,
	OptNewest,
	OptOutput,
	OptOverwrite,
	OptProbe,
	OptRename,
	OptRenameAndDelete,
	OptServer,
	OptSupersede,
	OptTcp,
	OptUdp,
	OptUngreedy,
	OptUnixDomain,
	OptVerbose,
	PTypeFILE,
	PTypeSTR,
	PTypeBUF,
	PTypePIPE,
	PTypeSOCK,
	PTypeOTHER,
	ProgramName,
	QQuote,
	Quote,
	RegexpSym,
	RootEnvironment,
	SSymbol,
	SysCall,
	SysCalltrace,
	SysErrordrop,
	SysEval,
	SysEvaltrace,
	SysLastError,
	SysStacktrace,
	SysStderr,
	SysStdin,
	SysStdout,
	SysSystem,
	SysVerbosity,
	SysVersionString,
	SysWarnerrors,
	T,
	Unquote,
	UnquoteSplicing,
	Λ *Symbol
)

var initialized = false

func Init_stdsyms() {
	//fmt.Println("lob/stdsyms.init");

	// these are needed for SetSysProp()
	T = internIntern("t", true)
	SysSystem = Intern("sys:system")
	SysSystem.SetSysProp()
	T.SetSysProp()
	// needed for most of the InternWithValue()s
	Nil = internIntern("nil", true).SetSysProp()

	AndKey = InternSys("&key", nil)
	AndOptional = InternSys("&optional", nil)
	AndRest = InternSys("&rest", nil)
	Call = InternSys("call", nil)
	CmdArgs = InternSys("sys:args", nil)
	CurrentLoadFile = InternSys("sys:current-load-file", Nil)
	FuncDefinedIn = InternSys("func-defined-in", nil)
	Function = InternSys("function", nil)
	VarDefinedIn = InternSys("var-defined-in", nil)
	DocString = InternSys("docstring", nil)
	EvalSym = InternSys("eval", nil)
	Lambda = internIntern("lambda", true).SetSysProp()
	LastEvalStats = InternSys("sys:last-eval-stats", Nil)
	LoadPaths = InternSys("sys:load-paths", Nil)
	OptAllMatches = InternSys(":all-matches", nil)
	OptAppend = InternSys(":append", nil)
	OptClient = InternSys(":client", nil)
	OptCreate = InternSys(":create", nil)
	OptDefault = InternSys(":default", nil)
	OptDirection = InternSys(":direction", nil)
	OptDotMatchesNewline = InternSys(":dot-matches-newline", nil)
	OptError = InternSys(":error", nil)
	OptIPv4 = InternSys(":ipv4", nil)
	OptIPv6 = InternSys(":ipv6", nil)
	OptIfDoesNotExist = InternSys(":if-does-not-exist", nil)
	OptIfExists = InternSys(":if-exists", nil)
	OptIgnoreCase = InternSys(":ignore-case", nil)
	OptInput = InternSys(":input", nil)
	OptInteractive = InternSys(":interactive", nil)
	OptIo = InternSys(":io", nil)
	OptMatchLimit = InternSys(":match-limit", nil)
	OptMultiLine = InternSys(":multi-line", nil)
	OptNewVersion = InternSys(":new-version", nil)
	OptNewest = InternSys(":newest", nil)
	OptOutput = InternSys(":output", nil)
	OptOverwrite = InternSys(":overwrite", nil)
	OptProbe = InternSys(":probe", nil)
	OptRename = InternSys(":rename", nil)
	OptRenameAndDelete = InternSys(":rename-and-delete", nil)
	OptServer = InternSys(":server", nil)
	OptSupersede = InternSys(":supersede", nil)
	OptTcp = InternSys(":tcp", nil)
	OptUdp = InternSys(":udp", nil)
	OptUngreedy = InternSys(":ungreedy", nil)
	OptUnixDomain = InternSys(":unix-domain", nil)
	OptVerbose = InternSys(":verbose", nil)
	PTypeFILE = InternSys("FILE", nil)
	PTypeSTR = InternSys("STR", nil)
	PTypeBUF = InternSys("BUF", nil)
	PTypePIPE = InternSys("PIPE", nil)
	PTypeSOCK = InternSys("SOCK", nil)
	PTypeOTHER = InternSys("OTHER", nil)
	ProgramName = InternSys("sys:program-name", Nil)
	QQuote = internIntern("quasiquote", false).SetSysProp()
	Quote = internIntern("quote", false).SetSysProp()
	RootEnvironment = InternSys("sys:root-environment", rootEnv)
	RegexpSym = InternSys("regexp", nil)
	SSymbol = InternSys("symbol", nil)
	SysCall = InternSys("sys:call", nil)
	SysCalltrace = InternSys("sys:calltrace", Nil)
	SysErrordrop = InternSys("sys:errordrop", Nil)
	SysEval = InternSys("sys:eval", nil)
	SysEvaltrace = InternSys("sys:evaltrace", Nil)
	SysLastError = InternSys("sys:last-error", Nil)
	SysStacktrace = InternSys("sys:stacktrace", Nil)
	SysStderr = InternSys("sys:stderr", nil)
	SysStdin = InternSys("sys:stdin", nil)
	SysStdout = InternSys("sys:stdout", nil)
	SysVerbosity = InternSys("sys:verbosity", Nil)
	SysVersionString = InternSys("sys:lingo-version", nil)
	SysWarnerrors = InternSys("sys:warnings-as-errors", Nil)
	Unquote = InternSys("unquote", nil)
	UnquoteSplicing = InternSys("unquote-splicing", nil)
	Λ = internIntern("λ", true).SetSysProp()
}
