// Go test module; not more that just begun
package lob

import "testing"

var symName = "hullala"

func TestSymbolIdentity(t *testing.T) {
	ob1 := Intern(symName)
	ob2 := Intern(symName)
	if ob1 != ob2 {
		t.Error("Symbols are not same:", ob1, ob2)
	}

	Init_environment()
	Init_stdsyms()
	nil2 := Intern("nil")
	if nil2 != Nil {
		t.Error("nil2 is not nil:", nil2)
	}
	t2 := Intern("t")
	if t2 != T {
		t.Error("t2 is not t:", t2)
	}
}
