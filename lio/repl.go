// Lisp I/O: the REPL
package lio

import (
	"fmt"

	"../dep"
	"../eval"
	"../hlp"
	"../lob"
	"../msg"
)

// the types of return from a read+eval
type retType int8

const (
	VALUE retType = 0 + iota // have a value
	ERROR                    // encountered an error
	EOF                      // saw EOF on input
)

var replLevel = 0

// call a recursive REPL, meant to be used by the error handler, so it needs to
// be called with minimum dependencies
func RecurseRepl() bool {
	_, ret := Repl(lob.Stdin(), true)
	return ret
}

// took this out of the Repl function to disentangle the latter a bit
func recoverFromEval(result *retType) {
	err := recover()
	switch tvalue := (err).(type) {
	case *lob.Pair:
		err = fmt.Errorf("throw without catch: %s",
			tvalue.Car().ValueString(nil))
	}
	if err != nil {
		msg.PrintError(err)
		if msg.ExitOnError {
			dep.Exit(5)
		}
		*result = ERROR
	}
}

// read, evaluate, and possibly print an expression; handle EOF and error
// conditions
func readEvalPrint(rdr *Reader, interactive bool) (
	value lob.Object, result retType) {

	if !dep.NoRecover {
		defer recoverFromEval(&result)
	}
	expr := rdr.ReadExpr()
	if expr == nil {
		//msg.Trace("repl reads EOF")
		return lob.Nil, EOF
	}
	//msg.Debug("repl read:", expr)

	perf := hlp.StartPerfdata()
	value = eval.EvalCurEnv(expr)
	perf.Stop()
	result = VALUE
	lob.LastEvalStats.SetValue(perf.List())

	if interactive {
		//msg.Debug("repl value:", value)
		fmt.Println()
		fmt.Println(value)
		msg.Info(perf.String())
	}
	return value, result
}

// The Lisp Read-Eval-Print loop. Either runs interactively or stops on error.
// Returns true iff there was an error.
func Repl(in *lob.Port, interactive bool) (lob.Object, bool) {
	replLevel++
	defer func() { replLevel-- }()
	// savedEnv := lob.EnterEnvironment()
	// defer savedEnv.BackTo()

	if replLevel == 1 {
		in.SetPromptString("> ")
	} else {
		savedPrompt := in.GetPromptString()
		defer in.SetPromptString(savedPrompt)
		in.SetPromptString(fmt.Sprintf("ERR REPL %d:> ", replLevel-1))
	}
	rdr := NewReader(in)
	var lastValue lob.Object = lob.Nil
	hadError := false
THELOOP:
	for !hadError || interactive {
		value, result := readEvalPrint(rdr, interactive)
		switch result {
		case EOF:
			if interactive {
				fmt.Println()
			}
			break THELOOP
		case ERROR:
			hadError = true
		case VALUE:
			lastValue = value
		}
	}
	return lastValue, hadError
}

func Init_repl() {
	//fmt.Println("lio/repl.init")
	dep.RecurseRepl = RecurseRepl
}
