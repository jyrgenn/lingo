// Lisp I/O: loading files
package lio

import (
	"path"
	"strings"

	"../dep"
	"../hlp"
	"../lob"
	"../msg"
	"../olu"
)

const lispFileSuffix = ".lisp" // (potential) suffix for load files

func evalStream(in *lob.Port, label string, keywords lob.Obmap) (
	lob.Object, bool) {

	savedEnv := lob.EnterEnvironment()
	savedLoadFile := lob.CurrentLoadFile.SetValue(lob.NewString(label))
	defer func() {
		savedEnv.BackTo()
		lob.CurrentLoadFile.SetValue(savedLoadFile)
	}()

	perf := hlp.StartPerfdata()
	value, err := Repl(in, false)
	if !err {
		verbose, found := keywords[lob.OptVerbose]
		if !found || !verbose.IsNil() {
			msg.Infof("load %s: %v, %s",
				label, lob.Bool2Ob(!err), perf.Stop().String())
		}
	}
	return value, !err
}

// convenience function for the startup of the interpreter: load a file if it
// exists; return true iff it has been read
func LoadIfExists(fname string) bool {
	_, found := Load(fname, lob.Obmap{lob.OptIfDoesNotExist: lob.Nil})
	return found
}

// Load code from the file at the specified pathname. The keywords parameter is
// a map of load options.
func LoadFile(pathname string, keywords lob.Obmap) (ok, found bool) {

	// msg.Debug("try to load", pathname)
	savedReadInteractively := dep.ReadInteractively
	dep.ReadInteractively = false
	defer func() { dep.ReadInteractively = savedReadInteractively }()
	if olu.IsDirectory(pathname) {
		return false, false
	}
	in := lob.NewFilePort(pathname, keywords)
	if in == nil {
		return false, false
	}
	defer in.Close()
	_, ok = evalStream(in, pathname, keywords)
	return ok, true
}

func tryLoad(pathname string, keywords lob.Obmap) (ok, found bool) {
	keywords[lob.OptIfDoesNotExist] = lob.Nil // don't get too excited here
	ok, found = LoadFile(pathname, keywords)
	if found {
		return ok, true
	}
	if !strings.HasSuffix(pathname, lispFileSuffix) {
		ok, found := LoadFile(pathname+lispFileSuffix, keywords)
		return ok, found
	}
	return false, false
}

// Load code from a file. If the file name is an absolute path name, it will be
// used directly; otherwise, it is tried to load it relative to the current
// directory and to the directories in the load-path.
func Load(fname string, keywords lob.Obmap) (ok, found bool) {
	loadPaths := lob.LoadPaths.Value()
	if loadPaths == nil {
		loadPaths = lob.Nil
	}

	if path.IsAbs(fname) || loadPaths.IsNil() {
		ok, found = tryLoad(fname, keywords)
	} else {
		loadPaths = lob.Cons(lob.NewString("."), loadPaths)
		for lob.IsPair(loadPaths) {
			pathdir := lob.Pop(&loadPaths)
			pathname := path.Join(pathdir.ValueString(nil), fname)
			ok, found = tryLoad(pathname, keywords)
			if found {
				break
			}
		}
	}
	return ok, found
}

// convenience function for the interpreter startup: load code from a string
func LoadFromString(lispCode, label string) (lob.Object, bool) {
	return evalStream(lob.NewStringPort(lispCode, label), label,
		lob.Obmap{})
}

// initialize the lio/load module
func Init_load() {
	//fmt.Println("lio/load.init")
	lob.Load = Load
}
