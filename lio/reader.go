// Lisp I/O: the Lisp reader
package lio

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"../dep"
	"../lob"
	"../msg"
	"../sys"
)

/////////////////////////
// some data definitions; what is the reader, a token?

// runeStack is a stack of runes to be written and read in last-in first-out
// order, for unreading runes
type runeStack struct {
	runes []rune
}

func (rs *runeStack) has() bool {
	return len(rs.runes) > 0
}

func (rs *runeStack) push(r rune) {
	rs.runes = append(rs.runes, r)
}

func (rs *runeStack) pop() (rune, int, error) {
	if !rs.has() {
		return 0, 0, errors.New("tried to pop from empty runeStack")
	}
	index := len(rs.runes) - 1
	r := rs.runes[index]
	rs.runes = rs.runes[0:index]
	return r, 1, nil
}

func (rs *runeStack) len() int {
	return len(rs.runes)
}

// srcLocation is the location in a source, which may be a file or something
// like *stdin*, *preload-code*, etc.; we stick with Emacs's notion that the
// first line number is 1, but the first column number is 0. :-\
type srcLocation struct {
	name   string // name for display purposes
	line   int    // current line number, starting at 1
	column int    // current column number, starting at 0
}

type Reader struct {
	loc           srcLocation   // where we are in the source
	src           *lob.Port     // source of runes
	prevCol       int           // previous column, use after unread
	prevLine      int           // previous line, use after unread
	unreadChars   *runeStack    // stack of unread chars
	tokbuf        *bytes.Buffer // buffer for atom token value
	pushbackToken Token         // pushed back token, or T_NO_TOK
	buffer        *bytes.Buffer // input line buffer, ReadRune() from here
	loadingFile   bool          // reading from file, prepare for #!
}

// An enum type for the type of a token.
type TokenType int

// the actual TokenType constants
const (
	// not a token at all
	T_NO_TOK TokenType = 223 + iota

	// "lightweight" tokens, no content:

	// a single period, as in dotted pairs or lists
	T_PERIOD

	// opening parenthesis
	T_OPAREN

	// closing parenthesis
	T_CPAREN

	// atom tokens, with extra content; string, regexp, char, symbol, number
	T_ISATOM

	// reader macro: single quote, backquote, comma, at-sign, or an
	// octothorpe-something combination
	T_RMACRO

	// begin a vector ("#(")
	T_VECBGN

	// begin a table ("#:")
	T_TABBGN

	// begin a struct
	T_STRBGN

	// end of file
	T_EOFTOK
)

type Token struct {
	typ TokenType
	loc srcLocation
	val lob.Object // value of the object, in case of a "heavy" token
}

// this should actually be a constant, but Go dun wanna
var noToken = Token{
	typ: T_NO_TOK,
	loc: srcLocation{name: "", line: 0, column: 0},
	val: nil,
}

func (tok Token) String() string {
	tname, exists := map[TokenType]string{
		T_NO_TOK: "T_NO_TOK",
		T_PERIOD: "T_PERIOD",
		T_CPAREN: "T_CPAREN",
		T_OPAREN: "T_OPAREN",
		T_ISATOM: "T_ISATOM",
		T_RMACRO: "T_RMACRO",
		T_VECBGN: "T_VECBGN",
		T_TABBGN: "T_TABBGN",
		T_STRBGN: "T_STRBGN",
		T_EOFTOK: "T_EOFTOK",
	}[tok.typ]

	if !exists {
		tname = "iNVALiD"
	}
	obname := "<no object>"
	if tok.val != nil {
		obname = tok.val.ReaderString(nil)
	}
	return fmt.Sprintf("%s{%s, %s}", tname, tok.loc.String(), obname)
}

/////////////////////////
// Public functions

// create a new Reader reading from the specified source port
func NewReader(source *lob.Port) *Reader {
	return &Reader{
		loc: srcLocation{
			name:   source.Name(),
			line:   1,
			column: 0,
		},
		src:           source,
		prevCol:       0,
		prevLine:      1,
		unreadChars:   new(runeStack),
		tokbuf:        new(bytes.Buffer),
		pushbackToken: noToken,
		buffer:        nil,
		loadingFile:   source.IsFilePort(),
	}
}

// read an expression from a port
func Read(source *lob.Port) (ob lob.Object) {
	// defer func() { fmt.Printf("Read returns %v\n", ob) }()
	return NewReader(source).ReadExpr()
}

// with an existing reader, return the next expression; return the object or
// throw an error
func (rdr *Reader) ReadExpr() (retob lob.Object) {
	//msg.Trace("enter ReadExpr")
	what := "read sexpr"
LOOP:
	for retob == nil {
		token := rdr.nextToken()
		switch token.typ {
		case T_ISATOM:
			retob = token.val
		case T_OPAREN:
			retob, _ = rdr.readLoop(T_CPAREN)
		case T_CPAREN:
			rdr.readError(what, "unexpected close paren")
		case T_TABBGN:
			loc := rdr.loc
			kvpairs := rdr.ReadExpr()
			if !lob.IsList(kvpairs) {
				rdr.loc = loc
				rdr.readError("reading table",
					"key/value pairs list not a list")
			}
			retob = lob.NewPairsTable(kvpairs)
		case T_STRBGN:
			loc := rdr.loc
			structBody := rdr.ReadExpr()
			if !lob.IsList(structBody) {
				rdr.loc = loc
				rdr.readError("reading struct literal",
					"struct elements list not a list")
			}
			retob = lob.NewStructFromList(structBody)
		case T_VECBGN:
			elems, nelem := rdr.readLoop(T_CPAREN)
			retob = lob.NewVectorList(elems, nelem)
		case T_RMACRO:
			retob = list2(token.val, rdr.ReadExpr())
		case T_PERIOD:
			rdr.readError(what, "unexpected period")
		case T_EOFTOK:
			break LOOP
		default:
			rdr.readError(what, "invalid token type %s in parser",
				token.String())
		}
	}
	msg.Trace("leave ReadExpr:", retob)
	return retob
}

// initialize the reader module
func Init_reader() {
	//fmt.Println("lio/reader.init")
	lob.Read = Read
}

///////////////////////////////
// more parts of the parser (without tokenizer)

func (rdr *Reader) pushback(tok Token) {
	rdr.pushbackToken = tok
}

// return a list of the two specified elements
func list2(ob1, ob2 lob.Object) lob.Object {
	return lob.Cons(ob1, lob.Cons(ob2, lob.Nil))
}

// return a sequence of read expressions (list, table, vector)
func (rdr *Reader) readLoop(expectedEnd TokenType) (lob.Object, int) {
	//msg.Trace("enter readLoop, expectedEnd", tokenName[expectedEnd])
	oldprompt := rdr.src.GetPrompt()
	if oldprompt != nil && oldprompt != lob.Nil {
		defer rdr.src.SetPrompt(oldprompt)
		rdr.src.SetPrompt(nil)
	}
	retob := lob.Object(lob.Nil) // the item to be returned
	last := lob.Object(lob.Nil)  // ptr to last element (or Nil)
	expr := lob.Object(lob.Nil)  // an expression
	pair := lob.Object(lob.Nil)  // a pair
	nelems := 0
LOOP:
	for {
		token := rdr.nextToken()
		switch token.typ {
		case expectedEnd:
			break LOOP
		case T_EOFTOK:
			rdr.readError("read elements", "unexpected EOF")
		case T_PERIOD:
			if expectedEnd != T_CPAREN || last == lob.Nil {
				rdr.readError("readLoop", "unexpected period")
			}
			expr = rdr.ReadExpr()
			last.Rplacd(expr)
			/* now we need the closing paren */
			token = rdr.nextToken()
			if token.typ != T_CPAREN {
				rdr.readError("read elements",
					"unexpected token %v", token.String())
			}
			break LOOP
		case T_ISATOM:
			expr = token.val
		default:
			rdr.pushback(token)
			expr = rdr.ReadExpr()
		}
		pair = lob.Cons(expr, lob.Nil)
		if retob == lob.Nil {
			retob = pair
		} else {
			last.Rplacd(pair)
		}
		last = pair
		nelems++
		//msg.Tracef("readLoop: have item %v, len %d", expr, nelems)
	}
	//msg.Trace("leave readLoop:", retob, nelems)
	return retob, nelems
}

//////////////////////
// the tokenizer

// The width of tabulators is assumed to be 8 spaces; this is only important for
// the text column in error messages.
const TABWIDTH = 8

func (loc srcLocation) String() string {
	return fmt.Sprintf("%s:%d:%d", loc.name, loc.line, loc.column)
}

// the characters that are delimiter tokens
var delimiterToken = map[rune]TokenType{
	'(': T_OPAREN,
	')': T_CPAREN,
}

// these are delimiter *characters*
var delimiters = "|()\"';\\`"

func isDelimiter(r rune) (isdelim bool) {
	return strings.IndexRune(delimiters, r) >= 0
}

// in the (lexer) initial state, return the next token
func (rdr *Reader) nextToken() (tok Token) {
	// defer func() { fmt.Printf("nextToken: %s\n", tok) }()
	// if there is a pushed back token, take this one
	if rdr.pushbackToken.typ != T_NO_TOK {
		tok = rdr.pushbackToken
		rdr.pushbackToken = noToken
		return tok
	}

	var r rune          // a rune read from the source
	var eof bool        // have reached EOF or not
	var loc srcLocation // current location in the source at start of token

	// read char, loop over whitespace and comments
	inComment := false
	for {
		loc = rdr.loc
		r, eof = rdr.readRune(false, "reading next token")
		if eof {
			return Token{typ: T_EOFTOK, loc: loc, val: nil}
		}
		switch r {
		case ';':
			inComment = true
		case '#':
			if rdr.loadingFile && loc.line == 1 && loc.column == 0 {
				inComment = true
			}
		case '\n':
			inComment = false
		}
		if inComment || unicode.IsSpace(r) {
			continue
		}
		// finally found a non-whitespace character outside of a
		// comment
		break
	}

	// delimiter token?
	ttype, exists := delimiterToken[r]
	if exists {
		return Token{typ: ttype, loc: loc, val: nil}
	}

	// a single dot?
	if r == '.' {
		next, _ := rdr.readRune(true, "after dot")
		rdr.unread(next)
		if unicode.IsSpace(next) || isDelimiter(next) {
			return Token{typ: T_PERIOD, loc: loc, val: nil}
		}
	}

	switch r {
	// reader macro?
	case '\'':
		return Token{typ: T_RMACRO, loc: loc, val: lob.Quote}
	case '`':
		return Token{typ: T_RMACRO, loc: loc, val: lob.QQuote}
	case ',':
		nextr, _ := rdr.readRune(true, "after comma")
		if nextr == '@' {
			return Token{typ: T_RMACRO, loc: loc,
				val: lob.UnquoteSplicing,
			}
		} else {
			rdr.unread(nextr)
			return Token{typ: T_RMACRO, loc: loc,
				val: lob.Unquote}
		}
	case '#':
		return rdr.octothorpeMacro(loc)

	// atom tokens
	case '"':
		return rdr.stringToken(loc)
	case '|':
		return rdr.quotedSymbolToken(loc)
	default:
		rdr.unread(r)
		return rdr.symbolOrNumberToken(loc)
	}
}

var backslashed = map[rune]rune{
	'a': '\a',
	'b': '\b',
	'f': '\f',
	'n': '\n',
	'r': '\r',
	't': '\t',
	'v': '\v',
}

// read from a group of three octal digits; the first is passed as argument
func (rdr *Reader) read3Octal(r rune, what string) rune {
	result := r - '0'
	for i := 0; i < 2; i++ {
		result <<= 3
		r, _ := rdr.readRune(true, "reading octal in "+what)
		switch r {
		case '0', '1', '2', '3', '4', '5', '6', '7':
			result += r - '0'
		default:
			rdr.readError("reading "+what,
				"char is not an octal digit: ``%c''", r)
		}
	}
	return result
}

// read a group of hexadecimal digits of length n
func (rdr *Reader) readUnicode(n int, what string) rune {
	result := rune(0)
	for i := 0; i < n; i++ {
		result <<= 4
		r, _ := rdr.readRune(true, "reading hexadecimal in "+what)
		switch r {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			result += r - '0'
		case 'a', 'b', 'c', 'd', 'e', 'f':
			result += r - 'a' + 10
		case 'A', 'B', 'C', 'D', 'E', 'F':
			result += r - 'A' + 10
		default:
			rdr.readError(what,
				"non-hex digit %d/%d in unicode escape: ``%c''",
				i, n, r)
		}
	}
	return result
}

// read string-like contents until a specified delimiter is encountered, not
// escaped by a backslash
func (rdr *Reader) readUntilDelim(delimiter rune, what string) string {
	var buf bytes.Buffer
	hadBackslash := false
LOOP:
	for {
		r, _ := rdr.readRune(true, "reading "+what)
		if hadBackslash {
			hadBackslash = false
			switch r {
			case 'a', 'b', 'f', 'n', 'r', 't', 'v':
				r = backslashed[r]
			case '0', '1', '2', '3', '4', '5', '6', '7':
				r = rdr.read3Octal(r, what) // pass first digit
			case 'x', 'X':
				r = rdr.readUnicode(2, what)
			case 'u':
				r = rdr.readUnicode(4, what)
			case 'U':
				r = rdr.readUnicode(8, what)
			default:
				// nop, character is taken literally
			}
		} else {
			switch r {
			case '\\':
				hadBackslash = true
				continue
			case delimiter:
				break LOOP
			}
		}
		buf.WriteRune(r)
	}
	return buf.String()
}

// read the rest of a string literal (initial double-quote has already been
// seen) and return it as a token
func (rdr *Reader) stringToken(loc srcLocation) Token {
	oldprompt := rdr.src.GetPrompt()
	if oldprompt != nil && oldprompt != lob.Nil {
		defer rdr.src.SetPrompt(oldprompt)
		rdr.src.SetPrompt(nil)
	}
	str := rdr.readUntilDelim('"', "string literal")
	return Token{typ: T_ISATOM, loc: loc, val: lob.NewString(str)}
}

// return the corresponding closing delimiter for the specified opening one;
// some come in complementary pairs, and for the others it is just the same one
func closingOf(opening rune) (closing rune) {
	// closing bracket-type delimiter characters
	closing, exists := map[rune]rune{
		'[': ']',
		'{': '}',
		'(': ')',
		'<': '>',
	}[opening]
	if exists {
		return closing
	}
	return opening
}

func (rdr *Reader) regexpTokenCustomDelimiter(loc srcLocation) Token {
	delimiter, _ := rdr.readRune(true, "reading regexp literal")
	if unicode.IsSpace(delimiter) || delimiter == ';' {
		rdr.readError("regexp literal",
			"delimiter may not be whitespace or comment sign")
	}
	return rdr.regexpToken(loc, closingOf(delimiter))
}

// This is very similar to readUntilDelim(), only a backslash is written into
// the string as it is if it is not followed by a special character literal or
// the delimiter. Subtle is this difference is, I do not yet see how I can unify
// these two functions elegantly enough.
func (rdr *Reader) regexpToken(loc srcLocation, delimiter rune) Token {
	var buf bytes.Buffer
	hadBackslash := false
	what := "regexp"
LOOP:
	for {
		r, _ := rdr.readRune(true, "reading "+what)
		if hadBackslash {
			hadBackslash = false
			switch r {
			case 'a', 'b', 'f', 'n', 'r', 't', 'v':
				r = backslashed[r]
			case '0', '1', '2', '3', '4', '5', '6', '7':
				r = rdr.read3Octal(r, what) // pass first digit
			case 'x', 'X':
				r = rdr.readUnicode(2, what)
			case 'u':
				r = rdr.readUnicode(4, what)
			case 'U':
				r = rdr.readUnicode(8, what)
			case delimiter:
				r = delimiter
			default:
				buf.WriteRune('\\')
			}
		} else {
			switch r {
			case '\\':
				hadBackslash = true
				continue
			case delimiter:
				break LOOP
			}
		}
		buf.WriteRune(r)
	}
	return Token{typ: T_ISATOM, loc: loc,
		val: lob.NewRegexp(buf.String())}
}

// read characters until whitespace or a symbol delimiter and return them as a
// string. No backslash escapes here.
func (rdr *Reader) readSymbolOrNumber() string {
	var buf bytes.Buffer
	for {
		r, eof := rdr.readRune(false, "symbol name")
		if eof {
			break
		}
		if unicode.IsSpace(r) || isDelimiter(r) {
			rdr.unread(r)
			break
		}
		buf.WriteRune(r)
	}
	return buf.String()
}

// names used in special character literals of the form #\Linefeed
var specialCharName = map[string]rune{
	"space":     ' ',
	"newline":   '\n',
	"tab":       '\t',
	"page":      '\f',
	"rubout":    '\177',
	"linefeed":  '\n',
	"return":    '\r',
	"backspace": '\010',
	"bell":      '\a',
}

// scan the digits of a #\x, #\u, or #\U character escape or number literal from
// string s
func (rdr *Reader) scanHex(s, what string) (n int64) {
	for _, r := range s {
		n <<= 4
		switch r {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			n += int64(r - '0')
		case 'a', 'b', 'c', 'd', 'e', 'f':
			n += int64(r - 'a' + 10)
		case 'A', 'B', 'C', 'D', 'E', 'F':
			n += int64(r - 'A' + 10)
		default:
			rdr.readError("reading hexadecimal",
				"non-hex digit in %s: ``%c''", what, r)
		}
	}
	return n
}

// scan the digits of a #\[0-7]+ character escape or number literal from string
// s
func (rdr *Reader) scanOctal(s, what string) (n int64) {
	for _, r := range s {
		n <<= 3
		switch r {
		case '0', '1', '2', '3', '4', '5', '6', '7':
			n += int64(r - '0')
		default:
			rdr.readError("reading octal",
				"non-octal digit in %s: ``%c''", what, r)
		}
	}
	return n
}

// scan the digits of a #\b character escape or number literal from string s
func (rdr *Reader) scanBinary(s, what string) (n int64) {
	for _, r := range s {
		n <<= 1
		switch r {
		case '0', '1':
			n += int64(r - '0')
		default:
			rdr.readError("reading binary",
				"non-binary digit in %s: ``%c''", what, r)
		}
	}
	return n
}

// read a character literal; the #\ has already been swalloed
func (rdr *Reader) charLiteralToken(loc srcLocation) Token {
	what := "reading character literal"
	charName := rdr.readSymbolOrNumber()
	length := utf8.RuneCountInString(charName)

	if length == 0 {
		// Character is nothing that could be read like a symbol name,
		// so it is a control character or whitespace or a delimiter.
		// And it must be the next character, so we need another attempt
		// and read a character whatever comes.
		char, _ := rdr.readRune(true, what)
		return Token{typ: T_ISATOM, loc: loc,
			val: lob.NewChar(char)}
	}
	// Done from here we have a character with a symbol-like "name".

	// defined character literal names first
	if char, exists := specialCharName[strings.ToLower(charName)]; exists {
		return Token{typ: T_ISATOM, loc: loc,
			val: lob.NewChar(char)}
	}

	// The first rune in the string is either the character itself (if the
	// name has length 1) or designated a numerical character literal.
	c1, _ := utf8.DecodeRuneInString(charName)

	var the_char rune
	if length == 1 {
		// single printable character
		the_char = c1
	} else if length == 3 && (c1 == 'x' || c1 == 'X') ||
		length == 5 && c1 == 'u' ||
		length == 9 && c1 == 'U' {
		// hexadecimal character literal with x2, u4, or U8 digits
		the_char = rune(rdr.scanHex(charName[1:], what))
	} else if c1 == 'b' || c1 == 'B' {
		// binary digits of arbitrary length
		the_char = rune(rdr.scanBinary(charName[1:], what))
	} else if c1 >= '0' && c1 <= '7' {
		// octal digits of arbitrary length
		the_char = rune(rdr.scanOctal(charName, what))
	} else {
		rdr.readError(what, "invalid character literal: ``#\\%s''",
			charName)
	}
	return Token{typ: T_ISATOM, loc: loc, val: lob.NewChar(the_char)}
}

// read the rest of a '|'-delimited symbol; we have already seen the first '|'.
func (rdr *Reader) quotedSymbolToken(loc srcLocation) Token {
	str := rdr.readUntilDelim('|', "quoted symbol")
	return Token{typ: T_ISATOM, loc: loc, val: lob.Intern(str)}
}

// read a token that could be a symbol or a number; return it as appropriate
func (rdr *Reader) symbolOrNumberToken(loc srcLocation) Token {
	s := rdr.readSymbolOrNumber()
	number, err := strconv.ParseFloat(s, 64)
	if err == nil {
		return Token{typ: T_ISATOM, loc: loc,
			val: lob.NewNumber(number)}
	}
	return Token{typ: T_ISATOM, loc: loc, val: lob.Intern(s)}
}

// a reader macro started with an octothorpe ('#'); we allow only a single
// additional character to determine the actual macro (should it perhaps be a
// whole token?)
func (rdr *Reader) octothorpeMacro(loc srcLocation) Token {
	r, _ := rdr.readRune(true, "reading octothorpe macro")
	switch r {
	case '/':
		return rdr.regexpToken(loc, '/')
	case 'r':
		return rdr.regexpTokenCustomDelimiter(loc)
	case '\'':
		return Token{typ: T_RMACRO, loc: loc, val: lob.Function}
	case '\\':
		return rdr.charLiteralToken(loc)
	case '(':
		return Token{typ: T_VECBGN, loc: loc, val: nil}
	case ':':
		return Token{typ: T_TABBGN, loc: loc, val: nil}
	case 's', 'S':
		return Token{typ: T_STRBGN, loc: loc, val: nil}
	case 'b':
		num := rdr.scanBinary(rdr.readSymbolOrNumber(), "number")
		return Token{typ: T_ISATOM, loc: loc,
			val: lob.NewInt64Number(num)}
	case 'o':
		num := rdr.scanOctal(rdr.readSymbolOrNumber(), "number")
		return Token{typ: T_ISATOM, loc: loc,
			val: lob.NewInt64Number(num)}
	case 'x':
		num := rdr.scanHex(rdr.readSymbolOrNumber(), "number")
		return Token{typ: T_ISATOM, loc: loc,
			val: lob.NewInt64Number(num)}
	default:
		rdr.readError("reader macro", "unexpected char `%c'", r)
	}
	return Token{typ: T_RMACRO, loc: loc, val: nil}
}

// Read and return a rune from the input stream. This may be a rune previously
// read and then unread again. Return an error -- or nil -- as appropriate,
// alternatively throw an error if needRune is true. Keep track of current line
// and column, even in the face of tabs (see TABWIDTH).
func (rdr *Reader) readRune(needRune bool, what string) (r rune, iseof bool) {
	// defer func() { fmt.Printf("readRune returns »%c«\n", r) }()
	if sys.HadSignal() {
		msg.Bail("interrupted")
	}
	var err error
	var whereFrom interface{}
	if rdr.unreadChars.has() {
		r, _, err = rdr.unreadChars.pop()
		whereFrom = "unread"
	} else {
		r, _, err = rdr.src.ReadRune()
		whereFrom = rdr.loc
	}
	if err != nil {
		if needRune || err != io.EOF {
			rdr.passError(err, what)
		}
		return r, true
	}
	msg.Tracef("read rune %s: 0x%02x `%c' from %v",
		what, r, r, whereFrom)
	rdr.prevCol = rdr.loc.column
	rdr.prevLine = rdr.loc.line
	if r == '\n' {
		rdr.loc.line++
		rdr.loc.column = 0
	} else {
		if r == '\t' {
			rdr.loc.column +=
				TABWIDTH - (rdr.loc.column % TABWIDTH)
		} else {
			rdr.loc.column++
		}
	}
	return r, false
}

// push a previously read rune (or another, don't care) back into the input
// stream
func (rdr *Reader) unread(r rune) {
	//msg.Trace("unread `" + string(r) + "'")
	msg.Tracef("unread rune 0x%02x `%c', have unread: %",
		r, r, rdr.unreadChars.len())
	rdr.unreadChars.push(r)
	if r == '\n' {
		rdr.loc.line--
	}
	rdr.loc.column = rdr.prevCol
	rdr.loc.line = rdr.prevLine
}

//////////////////////////
// Error handling helpers

func (rdr *Reader) readError(where string, format string, arg ...interface{}) {
	rdr.passError(fmt.Errorf(format, arg...), where)
}

func (rdr *Reader) passError(err error, where string) {
	var message string
	if err == io.EOF {
		message = "unexpected EOF"
	} else {
		message = err.Error()
	}
	loc := rdr.loc
	if dep.ReadInteractively {
		// in an interactive session, skip the rest of the input line
		for {
			r, eof :=
				rdr.readRune(false, "rest of line after error")
			if eof || r == '\n' {
				break
			}
		}
	}
	msg.Bailf("%s:%d:%d: %s %s %s", loc.name, loc.line,
		loc.column, where, msg.CallInfo(3), message)
}

/* EOF */
