// Struct builtins
package fun

// Definitions of all builtin Lisp functions

import (
	"../lob"
	"../msg"
)

/// name sys:def-struct-type
/// impl def_struct_type
/// mina 3
/// maxa -1
/// spec true
/// args tag docstring &rest args
/// retv struct
/// docs
///   Define a new struct type TAG with documentation DOCSTRING.
///   The ARGS are a sequence of slot name symbols.
///   This is a helper function for defstruct and not intended to be used
///   directly. Only defstruct will create the necessary make and accessor
///   functions.
/// endd
func def_struct_type(arglist lob.Object, nargs int) lob.Object {
	// var docArg lob.Object
	slots := []*lob.Symbol{}
	tagArg := lob.Pop(&arglist)
	tag := symbolArg(tagArg, "struct type tag")
	doc, arglist := arglist.Cxr()
	if !lob.IsString(doc) {
		msg.Bail("slot name argument is not a string:", doc)
	}
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		slots = append(slots, symbolArg(arg, "slot name"))
	}
	lob.DefineStructType(tag, doc.(*lob.String), slots)
	return tag
}

/// name get-struct-type
/// impl get_struct_type
/// mina 1
/// maxa 1
/// spec false
/// args struct-or-tag
/// retv type-desc
/// docs
///   Return the struct type description of a struct or struct tag.
///   The result is a list of the struct tag, the documentation string,
///   and the slots name symbols.
/// endd
func get_struct_type(arglist lob.Object, nargs int) lob.Object {
	var tag *lob.Symbol
	switch arg := arglist.Car().(type) {
	case *lob.Symbol:
		tag = arg
	case *lob.Struct:
		tag = arg.Tag()
	default:
		return lob.Nil
	}
	return lob.GetStructType(tag)
}

/// name get-struct-tag
/// impl get_struct_tag
/// mina 1
/// maxa 1
/// spec false
/// args struct
/// retv tag
/// docs
///   Return the type tag of STRUCT.
/// endd
func get_struct_tag(arglist lob.Object, nargs int) lob.Object {
	return structArg(arglist.Car(), "struct").Tag()
}

/// name sys:make-struct
/// impl make_struct
/// mina 1
/// maxa -1
/// spec false
/// args tag &rest args
/// retv struct
/// docs
///   Return a new struct of type TAG.
///   The ARGS are a sequence of slot keywords and associated value for each.
///   This is a helper function for the defstruct-generated make-... functions.
/// endd
func make_struct(arglist lob.Object, nargs int) lob.Object {
	tag := symbolArg(arglist.Car(), "struct type")
	arglist = arglist.Cdr()
	slots := lob.VarMap{}
	for lob.IsPair(arglist) {
		slotKeyw := lob.Pop(&arglist)
		value := lob.Pop(&arglist)
		keywName := slotKeyw.ValueString(nil)
		if !lob.IsSymbol(slotKeyw) || keywName[0] != ':' {
			msg.Bailf("make-%s: invalid non-keyword:",
				tag, keywName)
		}
		slot := lob.Intern(keywName[1:])
		if _, exists := slots[slot]; exists {
			msg.Bailf("make-%s: field %s named twice", tag, slot)
		}
		slots[slot] = value
	}
	return lob.NewStruct(tag, slots)
}

/// name sys:set-struct-slot
/// impl set_struct_slot
/// mina 3
/// maxa 3
/// spec false
/// args struct index value
/// retv value
/// docs
///   In STRUCT, set slot at INDEX to VALUE. Return VALUE.
///   This is a helper function for the defstruct-generated accessor functions.
/// endd
func set_struct_slot(arglist lob.Object, nargs int) lob.Object {
	sArg := lob.Pop(&arglist)
	indexArg := lob.Pop(&arglist)
	valueArg := arglist.Car()
	structArg(sArg, "struct").
		Set(int(numberArg(indexArg, "slot index")), valueArg)
	return valueArg
}

/// name sys:get-struct-slot
/// impl get_struct_slot
/// mina 2
/// maxa 2
/// spec false
/// args struct index
/// retv value
/// docs
///   From STRUCT, get value of slot at INDEX.
///   This is a helper function for the defstruct-generated accessor functions.
/// endd
func get_struct_slot(arglist lob.Object, nargs int) lob.Object {
	sArg := lob.Pop(&arglist)
	index := int(numberArg(arglist.Car(), "slot index"))
	return structArg(sArg, "struct").Get(index)
}
