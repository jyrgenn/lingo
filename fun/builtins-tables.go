// Table builtins
package fun

import (
	"../lob"
)

/// name table-count
/// impl tableCount
/// mina 1
/// maxa 1
/// spec false
/// args TABLE
/// retv number of pairs
/// docs
///   Return the number of key/value pairs in TABLE.
/// endd
func tableCount(arglist lob.Object, nargs int) lob.Object {
	return lob.NewIntNumber(
		tableArg(arglist.Car(), "table-count: table").Count())
}

/// name make-table
/// impl maketable
/// mina 0
/// maxa -1
/// spec false
/// args &rest pairs
/// retv table
/// docs
///   Return a new empty table, optionally filled with pairs.
///   Every argument that is not a pair will be used as (arg . nil).
/// endd
func maketable(arglist lob.Object, nargs int) lob.Object {
	return lob.NewPairsTable(arglist)
}

/// name table-get
/// impl tableget
/// mina 2
/// maxa 3
/// spec false
/// args TABLE KEY &optional DEFAULT
/// retv value or nil
/// docs
///   Return value associated in TABLE with KEY.
///   If KEY is not present, return DEFAULT (which defaults to nil).
/// endd
func tableget(arglist lob.Object, nargs int) lob.Object {
	table := lob.Pop(&arglist)
	key := lob.Pop(&arglist)
	defval := arglist.Car()
	value, _ := tableArg(table, "table-get").Get(key)
	if value == nil {
		return defval
	}
	return value
}

/// name table-put
/// impl tableput
/// mina 3
/// maxa 3
/// spec false
/// args TABLE KEY VALUE
/// retv value
/// docs
///   Make TABLE associate KEY with VALUE, return VALUE.
/// endd
func tableput(arglist lob.Object, nargs int) lob.Object {
	tableArg(arglist.Car(), "table-put").Put(arglist.Cdr().Car(), arglist.Cdr().Cdr().Car())
	return arglist.Cdr().Cdr().Car()
}

/// name table-exists
/// impl tableexists
/// mina 2
/// maxa 2
/// spec false
/// args TABLE KEY
/// retv t/nil
/// docs
///   Return t if KEY exists in TABLE, nil else.
/// endd
func tableexists(arglist lob.Object, nargs int) lob.Object {
	_, ok := tableArg(arglist.Car(), "table-get").Get(arglist.Cdr().Car())
	return lob.Bool2Ob(ok)
}

/// name table-remove
/// impl tabledel
/// mina 2
/// maxa 2
/// spec false
/// args TABLE KEY
/// retv table
/// docs
///   Remove KEY from TABLE and return TABLE.
/// endd
func tabledel(arglist lob.Object, nargs int) lob.Object {
	theTable := tableArg(arglist.Car(), "table-get")
	theTable.Delete(arglist.Cdr().Car())
	return theTable
}

/// name table-keys
/// impl tableKeys
/// mina 1
/// maxa 1
/// spec false
/// args TABLE
/// retv list
/// docs
///   Return a list with all keys in TABLE.
/// endd
func tableKeys(arglist lob.Object, nargs int) lob.Object {
	table := tableArg(arglist.Car(), "table-keys")
	return table.Keys()
}

/// name table-values
/// impl tableValues
/// mina 1
/// maxa 1
/// spec false
/// args TABLE
/// retv list
/// docs
///   Return a list with all values in TABLE.
/// endd
func tableValues(arglist lob.Object, nargs int) lob.Object {
	table := tableArg(arglist.Car(), "table-values")
	return table.Values()
}

/// name table-inc
/// impl tableInc
/// mina 2
/// maxa 3
/// spec false
/// args TABLE KEY &optional INCREMENT
/// retv new number value
/// docs
///   Increment the numeric value for KEY in TABLE by INCREMENT (or 1).
/// endd
func tableInc(arglist lob.Object, nargs int) lob.Object {
	table := tableArg(arglist.Car(), "table-inc")
	value, present := table.Get(arglist.Cdr().Car())
	var val float64
	if present {
		val = numberArg(value, "table-inc:")
	}
	increment := float64(1)
	if nargs > 2 {
		increment = numberArg(arglist.Cdr().Cdr().Car(), "table-inc:")
	}
	newval := lob.NewNumber(val + increment)
	table.Put(arglist.Cdr().Car(), newval)
	return newval
}

/// name table-pairs
/// impl tablePairs
/// mina 1
/// maxa 1
/// spec false
/// args TABLE
/// retv list
/// docs
///   Return a list with all (key . value) pairs in TABLE.
/// endd
func tablePairs(arglist lob.Object, nargs int) lob.Object {
	return tableArg(arglist.Car(), "table-pairs").KVPairs()
}

// EOF
