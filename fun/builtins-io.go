// I/O builtins
package fun

import (
	"fmt"
	"io"

	"../hlp"
	"../lob"
	"../msg"
)

/// name with-input-from-string
/// impl withInputFromString
/// mina 1
/// maxa -1
/// spec true
/// args "(SYMBOL STRING) &rest BODYFORMS"
/// retv result
/// docs
///   Binds SYMBOL to a port made from STRING and evaluates BODYFORMS.
/// endd
func withInputFromString(arglist lob.Object, nargs int) lob.Object {
	firstArg, bodyForms := arglist.Cxr()

	bindForm :=
		pairArg(firstArg, "with-input-from-string: bind form")
	portSymbol := symbolArg(bindForm.Car(),
		"with-input-from-string: first element of bind form")
	bindFormCdr := pairArg(bindForm.Cdr(),
		"with-input-from-string: cdr of bind form")
	if !bindFormCdr.Cdr().IsNil() {
		msg.Bail("with-input-from-string: malformed bind form")
	}
	savedEnv := lob.EnterEnvironment()
	defer savedEnv.BackTo()
	stringArg := lob.EvalCurEnv(bindFormCdr.Car()).ValueString(nil)
	portSymbol.Bind(lob.NewStringPort(stringArg, "*input-string*"))

	return hlp.EvalProgn(bodyForms)
}

/// name with-output-to-string
/// impl withOutputToString
/// mina 1
/// maxa -1
/// spec true
/// args "(SYMBOL) &rest BODYFORMS"
/// retv result
/// docs
///   Binds SYMBOL to a an output port and evaluates BODYFORMS.
///   Returns the string resulting from the writes to the port.
/// endd
func withOutputToString(arglist lob.Object, nargs int) lob.Object {
	firstArg, bodyForms := arglist.Cxr()
	bindForm :=
		pairArg(firstArg, "with-output-to-string: bind form")
	portSymbol := symbolArg(bindForm.Car(),
		"with-output-to-string: first element of bind form")
	if !bindForm.Cdr().IsNil() {
		msg.Bail("with-output-to-string: malformed bind form")
	}
	savedEnv := lob.EnterEnvironment()
	defer savedEnv.BackTo()
	port := lob.NewBufferPort()
	defer port.Close()
	portSymbol.Bind(port)

	hlp.EvalProgn(bodyForms)
	port.Close()
	return lob.NewString(port.GetString())
}

/// name read-line
/// impl readLine
/// mina 0
/// maxa 3
/// spec false
/// args &optional INPUT-STREAM EOF-ERROR-P EOF-VALUE
/// retv string
/// docs
///   Read a line from stdin (or INPUT-PORT) and return it as a string.
///   If EOF-ERROR-P is non-nil (that is the default), return error on EOF.
///   Otherwise, return EOF-VALUE (or nil).
/// endd
func readLine(arglist lob.Object, nargs int) lob.Object {
	inputPort := lob.Object(lob.Stdin())
	eofErrorP := true
	eofValue := lob.Object(lob.Nil)
	if nargs > 0 {
		inputPort = arglist.Car()
		if nargs > 1 {
			eofErrorP = !arglist.Cdr().Car().IsNil()
			if nargs > 2 {
				eofValue = arglist.Cdr().Cdr().Car()
			}
		}
	}

	if inputPort == lob.T {
		inputPort = lob.Stdin()
	}
	if lob.IsString(inputPort) {
		inputPort = lob.NewStringPort(inputPort.ValueString(nil),
			"*input-line*")
	}
	result, err := portArg(inputPort, "read-line: port").ReadLineObj()
	if err != nil && err != io.EOF {
		msg.Bail("error reading %v: %s", inputPort, err)
	} else if err == io.EOF {
		if eofErrorP {
			msg.Bail("EOF reading", inputPort)
		} else {
			return eofValue
		}
	}
	return result
}

// Get the symbolic port designator from the argument list, if there is an
// argument left, and translate it. It may be t, nil, or a port. Defaults to
// nil.
//
func getOutputPort(arglist lob.Object, where string) *lob.Port {
	designator := arglist.Car()
	switch maybePort := designator.(type) {
	case *lob.Symbol:
		switch maybePort {
		case lob.Nil:
			return lob.Stdout()
		case lob.T:
			return lob.Stderr()
		}
	case *lob.Port:
		return maybePort
	}
	msg.Bailf("%s: not a valid port designator: %v", where, designator)
	return nil
}

/// name terpri
/// impl terpri
/// mina 0
/// maxa 1
/// spec false
/// args &optional PORT
/// retv nil
/// docs
///   Terminate a print line by sending a newline to PORT (or standard output).
/// endd
func terpri(arglist lob.Object, nargs int) lob.Object {
	port := getOutputPort(arglist, "terpri")
	port.Write("\n")
	return lob.Nil
}

/// name print-to-string
/// impl print2string
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv string
/// docs
///   Print the argument to a string (as with print) and return the string.
/// endd
func print2string(arglist lob.Object, nargs int) lob.Object {
	return lob.NewString(arglist.Car().ReaderString(nil))
}

/// name open
/// impl open
/// mina 1
/// maxa -1
/// spec false
/// args FNAME &key direction if-exists if-does-not-exist
/// retv port
/// docs
///   Open a file (or reopen a port) and return the connected port.
///   Options: :direction followed by :input or :output or :io,
///            :if-exists followed by :new-version or :append or :overwrite
///                                or :supersede or :error or nil
///            :if-does-not-exist followed by :error or :create or nil
/// endd
func open(arglist lob.Object, nargs int) lob.Object {
	channel := arglist.Car()
	if lob.IsPort(channel) {
		return channel
	}
	fname := channel.ValueString(nil)
	port := lob.NewFilePort(fname,
		getKeywords(arglist.Cdr(), lob.Obmap{
			lob.OptIfDoesNotExist: nil,
			lob.OptIfExists:       nil,
			lob.OptDirection:      lob.OptInput,
		}))
	if port == nil {
		return lob.Nil
	}
	return port
}

/// name open-interactive-stream
/// impl open_interactive_stream
/// mina 2
/// maxa 3
/// spec false
/// args PORT PROMPT &optional HISTORY-FILE
/// retv port
/// docs
///   Open an interactive input port for the specified port.
///   Return a new port from which can be read using the interactive line
///   editor using PROMPT. If PROMPT is a function, it will be called to
///   determine the line editor prompt. If optional HISTORY-FILE is non-nil,
///   read and write a history of the session to that file name (a string).
/// endd
func open_interactive_stream(arglist lob.Object, nargs int) lob.Object {
	port := portArg(lob.Pop(&arglist), "port")
	prompt := lob.Pop(&arglist)
	histfile := ""
	if nargs > 2 {
		histfile = lob.Pop(&arglist).ValueString(nil)
	}
	port = lob.NewInteractivePort(port, prompt, histfile)
	if port == nil {
		return lob.Nil
	}
	return port
}

/// name set-port-prompt
/// impl set_port_prompt
/// mina 2
/// maxa 2
/// spec false
/// args PORT PROMPT
/// retv prompt
/// docs
///   Set the line input editor PROMPT of interactive input PORT.
///   PROMPT may be a function that is called to return the actual prompt
///   when it is needed, or any other object; in the latter case, its
///   string value is used.
/// endd
func set_port_prompt(arglist lob.Object, nargs int) lob.Object {
	port := portArg(arglist.Car(), "port")
	prompt := arglist.Cdr().Car()
	port.SetPrompt(prompt)
	return prompt
}

/// name get-port-prompt
/// impl get_port_prompt
/// mina 1
/// maxa 1
/// spec false
/// args PORT
/// retv prompt
/// docs
///   Get the line input editor PROMPT of interactive input PORT.
///   PROMPT may be a function that is called to return the actual prompt
///   when it is needed, or any other object.
/// endd
func get_port_prompt(arglist lob.Object, nargs int) lob.Object {
	port := portArg(arglist.Car(), "port")
	return port.GetPrompt()
}

/// name close
/// impl close
/// mina 1
/// maxa 1
/// spec false
/// args PORT
/// retv t/nil
/// docs
///   Close an open port. Return t if the port was open, nil else
/// endd
func close(arglist lob.Object, nargs int) lob.Object {
	port := portArg(arglist.Car(), "close:")
	if port.Close() {
		return lob.T
	}
	return lob.Nil
}

/// name prin1
/// impl prin1
/// mina 1
/// maxa 2
/// spec false
/// args ARG &optional PORT
/// retv arg
/// docs
///   Print ARG to PORT (or standard output) quoted to be read by (read).
/// endd
func prin1(arglist lob.Object, nargs int) lob.Object {
	ob := lob.Pop(&arglist)
	port := getOutputPort(arglist, "prin1")
	port.Write(ob.ReaderString(nil))
	return ob
}

/// name prin1-to-string
/// impl prin1_to_string
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv arg
/// docs
///   Print ARG to a string, quoted to be read by (read).
/// endd
func prin1_to_string(arglist lob.Object, nargs int) lob.Object {
	return lob.NewString(arglist.Car().ReaderString(nil))
}

/// name princ
/// impl princ
/// mina 1
/// maxa 2
/// spec false
/// args ARG &optional PORT
/// retv arg
/// docs
///   Print ARG to PORT (or standard output) without quoting.
/// endd
func princ(arglist lob.Object, nargs int) lob.Object {
	ob := lob.Pop(&arglist)
	port := getOutputPort(arglist, "princ")
	port.Write(ob.ValueString(nil))
	return ob
}

/// name princs
/// impl princs
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv string
/// docs
///   Print ARG to a string without quoting and return the string.
/// endd
func princs(arglist lob.Object, nargs int) lob.Object {
	return lob.NewString(arglist.Car().ValueString(nil))
}

/// name print
/// impl print
/// mina 1
/// maxa 2
/// spec false
/// args ARG &optional PORT
/// retv arg
/// docs
///   Print ARG to PORT (or standard output) without quoting, preceded by a
///   newline and followed by a blank.
/// endd
func print(arglist lob.Object, nargs int) lob.Object {
	ob := lob.Pop(&arglist)
	port := getOutputPort(arglist, "print")
	port.Write("\n", ob.ValueString(nil), " ")
	return ob
}

/// name format
/// impl format
/// mina 2
/// maxa -1
/// spec false
/// args DEST FORMAT-STRING &rest ARGS
/// retv nil or string
/// docs
///   Format ARGS according to FORMAT-STRING and write to DEST (port, t or nil).
///   Nil means return the result as a string, t means write to standard output.
///   The format string is interpreted by the Go library, meaning it is mostly
///   like the format string in the related C functions.
/// endd
func format(arglist lob.Object, nargs int) lob.Object {
	portarg := lob.Pop(&arglist)
	format := lob.Pop(&arglist)

	var port *lob.Port
	toString := false

	if portarg == lob.T {
		port = lob.Stdout()
	} else if portarg == lob.Nil {
		port = lob.NewBufferPort()
		toString = true
	} else if lob.IsPort(portarg) {
		port = portarg.(*lob.Port)
	} else {
		msg.Bail("format: invalid port:", portarg)
	}

	ifs := make([]interface{}, 0, 5)
	for lob.IsPair(arglist) {
		ob := lob.Pop(&arglist)
		switch val := ob.(type) {
		case *lob.Number:
			ifs = append(ifs, val.MaybeIntVal())
		case *lob.String, *lob.Char:
			ifs = append(ifs, val.ValueString(nil))
		default:
			ifs = append(ifs, val)
		}
	}
	//msg.Debug("format", port, ifs)
	port.Write(fmt.Sprintf(format.ValueString(nil), ifs...))
	if toString {
		return lob.NewString(port.GetString())
	}
	return lob.Nil
}

/// name read
/// impl read
/// mina 0
/// maxa 3
/// spec false
/// args &optional input eof-error-p eof-value
/// retv object
/// docs
///   Read an expression from INPUT (or the standard input) and return it.
///   INPUT may be a port or a string.
/// endd
func read(arglist lob.Object, nargs int) lob.Object {
	var result lob.Object
	input := lob.Stdin()
	errorEofP := true
	var eofValue lob.Object = lob.Nil
	if nargs > 0 {
		switch source := arglist.Car().(type) {
		case *lob.String:
			input = lob.NewStringPort(source.ValueString(nil),
				"*input-string*")
		case *lob.Port:
			input = source
		case *lob.Symbol:
			if source != lob.T {
				msg.Bailf("read: cannot read from symbol: %v",
					source)
			}
		default:
			msg.Bailf("read: cannot read from %s: %v",
				source.Type().Name(), source)
		}
		if nargs > 1 {
			errorEofP = lob.Ob2Bool(arglist.Cdr().Car())
			if nargs > 2 {
				eofValue = arglist.Cdr().Cdr().Car()
			}
		}
	}
	result = lob.Read(input)
	if result == nil {
		if errorEofP {
			msg.Bailf("read: EOF on %v", input)
		}
		result = eofValue
	}
	return result
}

/// name load
/// impl load
/// mina 1
/// maxa 2
/// spec false
/// args FNAME &key VERBOSE ERROR
/// retv t/nil
/// docs
///   Load specified file; return t if the contents was evaluated without error.
///   If keyword ERROR is nil (the default is true), do not raise an error for
///   an unfound file. If keyword verbose is nil (the default is true), do not
///   print an informational message after loading.
/// endd
func load(arglist lob.Object, nargs int) lob.Object {
	fname := arglist.Car().ValueString(nil)
	keywords := getKeywords(arglist.Cdr(), lob.Obmap{
		lob.OptVerbose: lob.T,
		lob.OptError:   lob.T,
	})
	result, found := lob.Load(fname, keywords)
	if !found {
		if keywords[lob.OptError].IsNil() {
			result = false
		} else {
			msg.Bailf("load: cannot load \"%s\"", fname)
		}
	}
	return lob.Bool2Ob(result)
}

// EOF
