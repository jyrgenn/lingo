// Sequences builtins
package fun

import (
	"../lob"
	"../msg"
)

/// name remove
/// impl remove
/// mina 2
/// maxa 2
/// spec false
/// args ITEM SEQUENCE
/// retv result-sequence
/// docs
///   Return a sequence from which the occurences of ITEM have been removed.
///   The original sequence is not modified.
/// endd
func remove(arglist lob.Object, nargs int) lob.Object {
	return sequenceArg(arglist.Cdr().Car(), "remove: sequence").
		Remove(arglist.Car())
}

/// name delete
/// impl delete
/// mina 2
/// maxa 2
/// spec false
/// args ITEM SEQUENCE
/// retv result-sequence
/// docs
///   Return a sequence from which the occurences of ITEM have been removed.
///   The sequence may be modified.
/// endd
func delete(arglist lob.Object, nargs int) lob.Object {
	return sequenceArg(arglist.Cdr().Car(),
		"delete: sequence").Delete(arglist.Car())
}

/// name elements
/// impl elements
/// mina 1
/// maxa 1
/// spec false
/// args SEQUENCE
/// retv element-list
/// docs
///   Return the elements of SEQUENCE as a list.
/// endd
func elements(arglist lob.Object, nargs int) lob.Object {
	return sequenceArg(arglist.Car(), "elements:").ToList()
}

/// name sequencep
/// impl sequencep
/// mina 1
/// maxa 1
/// spec false
/// args OBJECT
/// retv boolish
/// docs
///   Return t if OBJECT is a sequence, nil otherwise.
///   Lists, strings, and vectors are sequences.
/// endd
func sequencep(arglist lob.Object, nargs int) lob.Object {
	ob := arglist.Car()
	return lob.Bool2Ob(lob.IsSequence(ob))
}

/// name position
/// impl position
/// mina 2
/// maxa 5
/// spec false
/// args ITEM SEQUENCE &optional FROM-END START END
/// retv result
/// docs
///   Find ITEM in SEQUENCE and return its position (zero-based).
///   Return nil if ITEM is not found.
///   With optional FROM-END non-nil, search from the end of SEQUENCE.
///   With optional START and optional END, limit the search accordinly.
/// endd
func position(arglist lob.Object, nargs int) lob.Object {
	item := arglist.Car()
	sequence := sequenceArg(arglist.Cdr().Car(), "position: sequence")
	fromEnd := false
	start := 0
	end := -1
	argc := nargs
	if argc > 2 {
		fromEnd = arglist.Cdr().Cdr().Car() != lob.Nil
		if argc > 3 {
			start = int(numberArg(arglist.Cdr().Cdr().Cdr().Car(), "position: start"))
			if argc > 4 {
				end = int(numberArg(arglist.Cdr().Cdr().Cdr().Cdr().Car(), "position: end"))
			}
		}
	}
	result := sequence.Position(item, fromEnd, start, end)
	if result < 0 {
		return lob.Nil
	}
	return lob.NewIntNumber(result)
}

/// name dolist
/// impl dosequence
/// mina 1
/// maxa -1
/// spec true
/// args "(VAR LISTFORM [RESULTFORM [START [END]]]) BODY ..."
/// retv result
/// docs
///   Iterate over LISTFORM, binding symbol VAR to each element in turn.
///   Return the value of RESULTFORM (if specified) or nil.
/// endd
//
/// name doseq
/// impl dosequence
/// mina 1
/// maxa -1
/// spec true
/// args "(VAR SEQFORM [RESULTFORM [START [END]]]) BODY ..."
/// retv result
/// docs
///   Iterate over SEQFORM, binding symbol VAR to each element in turn.
///   Return the value of RESULTFORM (if specified) or nil.
/// endd
func dosequence(arglist lob.Object, nargs int) lob.Object {
	binding, bodyforms := arglist.Cxr()
	if !lob.IsPair(binding) {
		msg.Bail("doseq/dolist: binding form malformed")
	}
	elsym := symbolArg(binding.Car(), "doseq/dolist: bind var")
	binding = binding.Cdr()
	if !lob.IsPair(binding) {
		msg.Bail("doseq/dolist: bindings form too short")
	}
	seqform, rest := binding.Cxr()
	resultform := lob.Pop(&rest)
	startVal := lob.EvalCurEnv(lob.Pop(&rest))
	endVal := lob.EvalCurEnv(lob.Pop(&rest))
	start := int(numberArgOr(startVal, "doseq/dolist: start value", 0))
	end := int(numberArgOr(endVal, "doseq/dolist: end value", -1))
	to_end := end < 0
	savedEnv := lob.EnterEnvironment()
	defer savedEnv.BackTo()

	seq := lob.EvalCurEnv(seqform)
	if lob.IsList(seq) {
		for (to_end || end > 0) && lob.IsPair(seq) {
			element := lob.Pop(&seq)
			end--
			if start > 0 {
				start--
			} else {
				elsym.Bind(element)
				body := bodyforms
				for lob.IsPair(body) {
					form := lob.Pop(&body)
					lob.EvalCurEnv(form)
				}
			}
		}
	} else if lob.IsSequence(seq) {
		sequence := seq.(lob.Sequence)
		seqlen := sequence.Length()
		if end < 0 || end > seqlen {
			end = seqlen
		}
		for i := start; i < end; i++ {
			element := sequence.Element(i)
			elsym.Bind(element)
			body := bodyforms
			for lob.IsPair(body) {
				form := lob.Pop(&body)
				lob.EvalCurEnv(form)
			}
		}
	} else {
		msg.Bail("doseq/dolist: not a sequence:", seq)
	}
	elsym.Bind(lob.Nil)
	if resultform != nil {
		return lob.EvalCurEnv(resultform)
	}
	return lob.Nil
}

/// name subseq
/// impl subseq
/// mina 2
/// maxa 3
/// spec false
/// args SEQUENCE START &optional END
/// retv subsequence
/// docs
///   Create a copy of the subsequence of SEQUENCE bounded by START and END.
///   If END is omitted, the end of the sequence is assumed.
/// endd
func subseq(arglist lob.Object, nargs int) lob.Object {
	end := -1
	if nargs > 2 {
		end = int(numberArg(arglist.Cdr().Cdr().Car(), "subseq: end"))
	}
	start := int(numberArg(arglist.Cdr().Car(), "subseq start"))
	var result lob.Object
	switch seq := arglist.Car().(type) {
	case lob.Sequence:
		result = seq.SubSequence(start, end)
	default:
		msg.Bail("subseq: not a sequence:", seq)
	}
	return result
}

/// name elt
/// impl elt
/// mina 2
/// maxa 3
/// spec false
/// args SEQUENCE INDEX &optional DEFAULT
/// retv element
/// docs
///   Return the value of SEQUENCE (list, string, vector) element at INDEX.
///   Throw an error if the (zero-based) INDEX is not in the sequence.
///   If optional DEFAULT is specified, return this instead of throwing
///   an error for out-of-bounds access.
/// endd
func elt(arglist lob.Object, nargs int) lob.Object {
	index := int(numberArg(arglist.Cdr().Car(), "elt:"))
	haveDefault := nargs == 3
	if index < 0 {
		msg.Bailf("elt: negative index %d for %s %v",
			index, arglist.Car().Type().Name(), arglist.Car())
	}
	var result lob.Object
	switch seq := arglist.Car().(type) {
	case lob.Sequence:
		result = seq.Element(index)
		if result == nil {
			if haveDefault {
				return arglist.Cdr().Cdr().Car()
			}
			msg.Bailf("index %d out of bounds for %s %v",
				index, map[string]string{
					"vector": "vector",
					"string": "string",
					"symbol": "list",
					"pair":   "list",
				}[seq.Type().Name()], seq)
		}
	default:
		msg.Bail("elt: not a sequence:", seq)
	}
	return result
}

/// name setelt
/// impl setelt
/// mina 3
/// maxa 3
/// spec false
/// args SEQUENCE INDEX VALUE
/// retv VALUE
/// docs
///   Set SEQUENCE (list, string, vector) element at INDEX to VALUE.
///   Throw an error if the (zero-based) INDEX is not in the sequence.
/// endd
func setelt(arglist lob.Object, nargs int) lob.Object {
	index := int(numberArg(arglist.Cdr().Car(), "elt:"))
	if index < 0 {
		msg.Bailf("elt: negative index %d for %s %v",
			index, arglist.Car().Type().Name(), arglist.Car())
	}
	value := arglist.Cdr().Cdr().Car()
	switch seq := arglist.Car().(type) {
	case lob.Sequence:
		seq.SetElement(index, value)
	default:
		msg.Bail("elt: not a sequence:", seq)
	}
	return value
}

/// name length
/// impl length
/// mina 1
/// maxa 1
/// spec false
/// args SEQUENCE
/// retv number
/// docs
///   Return the length of a sequence (list, string, vector).
/// endd
func length(arglist lob.Object, nargs int) lob.Object {
	switch seq := arglist.Car().(type) {
	case lob.Sequence:
		return lob.NewIntNumber(seq.Length())
	case *lob.Table:
		return lob.NewIntNumber(
			tableArg(arglist.Car(), "table-count: table").Count())

	default:
		msg.Bail("length: not a sequence:", seq)
	}
	return lob.Nil
}

/// name copy-seq
/// impl copy_seq
/// mina 1
/// maxa 1
/// spec false
/// args SEQUENCE
/// retv copied-sequence
/// docs
///   Return a copy of SEQUENCE.
///   The elements of the new sequence are the same as the corresponding
///   elements of the given sequence.
/// endd
func copy_seq(arglist lob.Object, nargs int) lob.Object {
	switch seq := arglist.Car().(type) {
	case lob.Sequence:
		return seq.Copy()
	default:
		msg.Bail("copy-seq: not a sequence:", seq)
	}
	return lob.Nil
}

// EOF
