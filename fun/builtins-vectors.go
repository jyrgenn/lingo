// Vector builtins
package fun

import (
	"../call"
	"../lob"
	"../msg"
)

/// name vector
/// impl vector
/// mina 0
/// maxa -1
/// spec false
/// args &rest ARGS
/// retv vector
/// docs
///   Return a new vector with ARGS as elements.
/// endd
func vector(arglist lob.Object, nargs int) lob.Object {
	return lob.NewVectorList(arglist, nargs)
}

/// name make-vector
/// impl makeVector
/// mina 1
/// maxa 2
/// spec false
/// args LENGTH &options VALUE
/// retv vector
/// docs
///   Return a new vector of length LENGTH and each element set to VALUE.
/// endd
func makeVector(arglist lob.Object, nargs int) lob.Object {
	size := int(numberArg(arglist.Car(), "make-vector:"))
	fillValue := lob.Object(lob.Nil)
	var haveFunction bool
	var function call.Callable
	if nargs > 1 {
		switch initarg := arglist.Cdr().Car().(type) {
		case call.Callable:
			haveFunction = true
			function = initarg
		default:
			fillValue = initarg
		}
	}
	if size < 0 {
		msg.Bailf("make-vector: negative size(%d)", size)
	}
	if haveFunction {
		objects := make([]lob.Object, size, size)
		for i := 0; i < size; i++ {
			objects[i] = function.Call(0, lob.Nil)
		}
		return lob.NewVectorInit(objects, false)
	} else {
		return lob.NewVector(size, fillValue)
	}
}

/// name vector-set
/// impl vectorset
/// mina 3
/// maxa 3
/// spec false
/// args VECTOR INDEX VALUE
/// retv value
/// docs
///   Set the element of VECTOR at INDEX to VALUE, return VALUE.
/// endd
func vectorset(arglist lob.Object, nargs int) lob.Object {
	v := vectorArg(arglist.Car(), "vector-set")
	index := int(numberArg(arglist.Cdr().Car(), "vector-set: index"))
	v.Set(index, arglist.Cdr().Cdr().Car())
	return arglist.Cdr().Cdr().Car()
}

/// name vector-get
/// impl vectorget
/// mina 2
/// maxa 2
/// spec false
/// args VECTOR INDEX
/// retv value
/// docs
///   Return the element of VECTOR at INDEX
/// endd
func vectorget(arglist lob.Object, nargs int) lob.Object {
	v := vectorArg(arglist.Car(), "vector-get")
	index := int(numberArg(arglist.Cdr().Car(), "vector-get: index"))
	return v.Get(index)
}

// EOF
