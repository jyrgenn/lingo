// Bit operation builtins
package fun

import (
	"../lob"
)

/// name bit-and
/// impl bit_and
/// mina 2
/// maxa 2
/// spec false
/// args INT1 INT2
/// retv integer
/// docs
///   Return the bitwise "and" of integers INT1 and INT2.
///   If an argument is not an integer, it is rounded to the nearest one.
/// endd
func bit_and(arglist lob.Object, nargs int) lob.Object {
	int1 := roundInt(numberArg(arglist.Car(), "bit-and: int1"))
	int2 := roundInt(numberArg(arglist.Cdr().Car(), "bit-and: int2"))
	return lob.NewInt64Number(int1 & int2)
}

/// name bit-ior
/// impl bit_ior
/// mina 2
/// maxa 2
/// spec false
/// args INT1 INT2
/// retv integer
/// docs
///   Return the bitwise "inclusive or" of integers INT1 and INT2.
///   If an argument is not an integer, it is rounded to the nearest one.
/// endd
func bit_ior(arglist lob.Object, nargs int) lob.Object {
	int1 := roundInt(numberArg(arglist.Car(), "bit-and: int1"))
	int2 := roundInt(numberArg(arglist.Cdr().Car(), "bit-and: int2"))
	return lob.NewInt64Number(int64(int1 | int2))
}

/// name bit-xor
/// impl bit_xor
/// mina 2
/// maxa 2
/// spec false
/// args INT1 INT2
/// retv integer
/// docs
///   Return the bitwise "exclusive or" of integers INT1 and INT2.
///   If an argument is not an integer, it is rounded to the nearest one.
/// endd
func bit_xor(arglist lob.Object, nargs int) lob.Object {
	int1 := roundInt(numberArg(arglist.Car(), "bit-and: int1"))
	int2 := roundInt(numberArg(arglist.Cdr().Car(), "bit-and: int2"))
	return lob.NewInt64Number(int64(int1 ^ int2))
}

// /// name bit-eqv
// /// impl bit_eqv
// /// mina 2
// /// maxa 2
// /// spec false
// /// args INT1 INT2
// /// retv integer
// /// docs
// ///   Return the bitwise equivalence of integers INT1 and INT2.
// ///   If an argument is not an integer, it is rounded to the nearest one.
// /// endd
// func bit_eqv(arglist lob.Object, nargs int) lob.Object {
// 	int1 := roundInt(numberArg(arglist.Car(), "bit-and: int1"))
// 	int2 := roundInt(numberArg(arglist.Cdr().Car(), "bit-and: int2"))
// 	return lob.NewInt64Number(int64(int1 ^ int2))
// }

// EOF
