// Definitions of all builtin Lisp functions
package fun

import (
	"../lob"
	"../msg"
)

/// name list-collector
/// impl list_collector
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv closure
/// docs
///   Create a list collector closure and return it as a function.
///
///   The returned function takes an arbitrary number of arguments, which
///   are then added to the end of the list, and returns the list. The
///   normal use case would be to call it a number of times to add items to
///   the list, and then call it once without arguments to return the
///   resulting list. This is more efficient than using append repeatedly.
///
///   Example:
///       (let ((lc (list-collector)))
///         (lc 'see)
///         (lc 0 'evil)
///         (lc 'hear "no" 'evil)
///         (lc))
///   => (see 0 evil hear "no" evil)
/// endd
func list_collector(arglist lob.Object, nargs int) lob.Object {
	lp := lob.NewListPusher()
	lpfun := func(arglist lob.Object, nargs int) lob.Object {
		for lob.IsPair(arglist) {
			var arg lob.Object
			arg, arglist = arglist.Cxr()
			lp.Push(arg)
		}
		return lp.List()
	}
	return &Function{
		builtin: lpfun, fi: nil, minargs: 0,
		maxargs: -1, doc: "[arg ...]) => the-list",
		name: "*anon-list-collector*", ftype: BUILTIN,
		special: false, trace: false,
	}
}

/// name nthcdr
/// impl nthcdr
/// mina 2
/// maxa 2
/// spec false
/// args n list
/// retv tail
/// docs
///   Return the tail LIST, as if calling cdr N times in succession.
/// endd
func nthcdr(arglist lob.Object, nargs int) lob.Object {
	n := int(numberArg(arglist.Car(), "n"))
	listarg := listArg(arglist.Cdr().Car(), "the list")
	if n < 0 {
		msg.Bailf("nthcdr: negative index %d", n)
	}

	result := listarg.(lob.Object)
	for lob.IsPair(result) && n > 0 {
		result = result.Cdr()
		n--
	}
	if n > 0 && result != lob.Nil {
		msg.Bail("nthcdr: not a proper list:", listarg)
	}
	return result
}

/// name last
/// impl lastPair
/// mina 1
/// maxa 1
/// spec false
/// args list
/// retv tail
/// docs
///   Return the last dotted pair of the argument list L, which must be nil
///   or a well-formed list. To get the last element, use (car (last l)).
/// endd
func lastPair(arglist lob.Object, nargs int) lob.Object {
	listarg := listArg(arglist.Car(), "the list")
	result := listarg.(lob.Object)
	previous := result
	for lob.IsPair(result) {
		previous = result
		result = result.(*lob.Pair).Cdr()
	}
	return previous
}

/// name nth
/// impl nth
/// mina 2
/// maxa 2
/// spec false
/// args n list
/// retv object
/// docs
///   Return the Nth element of LIST.
/// endd
func nth(arglist lob.Object, nargs int) lob.Object {
	n := numberArg(arglist.Car(), "n")
	listarg := listArg(arglist.Cdr().Car(), "the list")
	if n < 0 {
		msg.Bailf("nthcdr: negative index %d", n)
	}

	list := listarg.(lob.Object)
	for lob.IsPair(list) && n > 0 {
		list = list.Cdr()
		n--
	}
	if n > 0 && list != lob.Nil {
		msg.Bail("nth: not a proper list:", listarg)
	}
	return list.Car()
}

/// name mapcar
/// impl mapcar
/// mina 2
/// maxa -1
/// spec false
/// args FUNCTION LIST
/// retv a list
/// docs
///   Apply FUNCTION to each member of LIST, return list of resulting values.
/// endd
func mapcar(arglist lob.Object, nargs int) lob.Object {
	function := callableArg(arglist.Car(), "mapcar")
	lists := arglist.Cdr()

	// function: return true iff all lists are still a pair
	allpairs := func(lists lob.Object) bool {
		for lob.IsPair(lists) {
			list := lob.Pop(&lists)
			if !lob.IsPair(list) {
				return false
			}
		}
		return true
	}
	// function: return a list of the cars of all lists
	allcarlist := func(lists lob.Object) (int, lob.Object) {
		nlists := 0

		lp := lob.NewListPusher()
		for lob.IsPair(lists) {
			nlists++
			list := lob.Pop(&lists)
			lp.Push(list.Car())
		}
		return nlists, lp.List()
	}

	lp := lob.NewListPusher()
	for allpairs(lists) {
		lp.Push(function.Call(allcarlist(lists)))
		for li := lists; lob.IsPair(li); li = li.Cdr() {
			li.(*lob.Pair).Rplaca(li.Car().Cdr())
		}
	}
	return lp.List()
}

/// name nreverse
/// impl nreverse
/// mina 1
/// maxa 1
/// spec false
/// args SEQUENCE
/// retv a list
/// docs
///   Reverse SEQUENCE (maybe by modifying a list's cdrs) and return the result.
/// endd
func nreverse(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	switch ob := arg.(type) {
	case lob.Sequence:
		return ob.NReverse()
	}
	msg.Bailf("nreverse: not a sequence:", arg)
	return lob.Nil
}

/// name reverse
/// impl reverse
/// mina 1
/// maxa 1
/// spec false
/// args SEQUENCE
/// retv a list
/// docs
///   Reverse SEQUENCE (by copying) and return the result.
/// endd
func reverse(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	switch ob := arg.(type) {
	case lob.Sequence:
		return ob.Reverse()
	}
	msg.Bailf("reverse: not a sequence:", arg)
	return lob.Nil
}

/// name list
/// impl list
/// mina 0
/// maxa -1
/// spec false
/// args &rest ARGS
/// retv list
/// docs
///   Return a new list with ARGS as members.
/// endd
func list(arglist lob.Object, nargs int) lob.Object {
	return arglist
}

/// name list*
/// impl listStar
/// mina 0
/// maxa -1
/// spec false
/// args &rest ARGS+
/// retv list
/// docs
///   Return a list with ARGS as members, with the last as the end of the list.
///   list* is like list except that the last argument to list becomes the car
///   of the last cons constructed, while the last argument to list* becomes
///   the cdr of the last cons constructed.
/// endd
func listStar(arglist lob.Object, nargs int) lob.Object {
	return spreadArglist(arglist)
}

/// name append
/// impl nappend
/// mina 0
/// maxa -1
/// spec false
/// args &rest lists
/// retv concatenated list
/// docs
///   Returns a new list that is the concatenation of LISTS.
///   The list structure of all but the last list is copied.
/// endd
func nappend(arglist lob.Object, nargs int) lob.Object {
	lp := lob.NewListPusher()
	for lob.IsPair(arglist) {
		list := lob.Pop(&arglist)
		if list.IsNil() {
			continue
		}
		if arglist.IsNil() && !lp.IsEmpty() {
			lp.AddEnd(list)
			break
		}
		start := list
		for lob.IsPair(list) {
			lp.Push(lob.Pop(&list))
		}
		if !list.IsNil() {
			msg.Bail("append argument not a proper list: %v", start)
		}
	}
	return lp.List()
}

/// name nconc
/// impl nconc
/// mina 0
/// maxa -1
/// spec false
/// args &rest lists
/// retv concatenated list
/// docs
///   Returns a new list that is the concatenation of LISTS.
///   The list structure of all arguments but the last is modified.
/// endd
func nconc(arglist lob.Object, nargs int) lob.Object {
	var result lob.Object = lob.Nil
	var lastpair lob.Object

	for lob.IsPair(arglist) {
		list := lob.Pop(&arglist)
		if list.IsNil() {
			continue
		}
		if lastpair == nil {
			result = list
		} else {
			lastpair.(*lob.Pair).Rplacd(list)
		}
		start := list
		for lob.IsPair(list) {
			lastpair = list
			list = list.Cdr()
		}
		if !list.IsNil() {
			msg.Bail("append argument not a proper list: %v", start)
		}
	}
	return result
}

// EOF
