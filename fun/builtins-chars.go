// Character operation builtins
package fun

//import "fmt"
import (
	"unicode"
	"unicode/utf8"

	"../lob"
)

/// name code-char
/// impl code_char
/// mina 1
/// maxa 1
/// spec false
/// args CODE
/// retv char (or nil)
/// docs
///   Return a character with the code attribute given by CODE. If no such
///   character exists and one cannot be created, nil is returned.
/// endd
func code_char(arglist lob.Object, nargs int) lob.Object {
	r := rune(numberArg(arglist.Car(), "code-char: code"))
	if utf8.ValidRune(r) {
		return lob.NewChar(r)
	} else {
		return lob.Nil
	}
}

/// name char-int
/// impl char_int
/// mina 1
/// maxa 1
/// spec false
/// args CHARACTER
/// retv integer
/// docs
///   Return a non-negative integer encoding the CHARACTER object.
/// endd
func char_int(arglist lob.Object, nargs int) lob.Object {
	return lob.NewIntNumber(int(charArg(arglist.Car(),
		"char-int: character")))
}

// implementation body of several cmp_* functions
func char_cmp(arglist lob.Object, maybe1, maybe2 int, name string, nargs int) lob.Object {
	if nargs < 2 {
		return lob.T
	}
	cmp := func(c1, c2 rune) int {
		if c1 < c2 {
			return -1
		} else if c1 > c2 {
			return 1
		} else {
			return 0
		}
	}
	start := unicode.ToLower(charArg(arglist.Car(), name+": first"))
	arglist = arglist.Cdr()
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		next := unicode.ToLower(charArg(arg, name+":"))
		switch cmp(start, next) {
		default:
			return lob.Nil
		case maybe1, maybe2:
			// These may be the case without makeng the comparison
			// false
		}
		start = next
	}
	return lob.T
}

/// name char-equal
/// impl char_equal
/// mina 0
/// maxa -1
/// spec false
/// args &rest characters
/// retv bool
/// docs
///   Return true iff all characters are the same except for case.
/// endd
func char_equal(arglist lob.Object, nargs int) lob.Object {
	return char_cmp(arglist, 0, 0, "char-equal", nargs)
}

/// name char-not-equal
/// impl char_not_equal
/// mina 0
/// maxa -1
/// spec false
/// args &rest characters
/// retv bool
/// docs
///   Return true iff all characters are unequal, without regarding case.
/// endd
func char_not_equal(arglist lob.Object, nargs int) lob.Object {
	switch nargs {
	case 0, 1:
		return lob.T
	case 2:
		val0 := charArg(arglist.Car(), "char-not-equal: first")
		val1 := charArg(arglist.Cdr().Car(),
			"char-not-equal: second")
		if unicode.ToLower(val0) == unicode.ToLower(val1) {
			return lob.Nil
		}
		return lob.T
	default:
		have := map[rune]bool{}
		for lob.IsPair(arglist) {
			c := lob.Pop(&arglist)
			value := unicode.ToLower(charArg(c,
				"char-not-equal: character"))
			if have[value] {
				return lob.Nil
			}
			have[value] = true
		}
		return lob.T
	}
}

/// name char-lessp
/// impl char_lessp
/// mina 0
/// maxa -1
/// spec false
/// args &rest characters
/// retv bool
/// docs
///   True iff all characters are monotonically increasing, except for case.
/// endd
func char_lessp(arglist lob.Object, nargs int) lob.Object {
	return char_cmp(arglist, -1, -1, "char-lessp", nargs)
}

/// name char-greaterp
/// impl char_greaterp
/// mina 0
/// maxa -1
/// spec false
/// args &rest characters
/// retv bool
/// docs
///   True iff all characters are monotonically decreasing, except for case.
/// endd
func char_greaterp(arglist lob.Object, nargs int) lob.Object {
	return char_cmp(arglist, 1, 1, "char-greaterp", nargs)
}

/// name char-not-greaterp
/// impl char_not_greaterp
/// mina 0
/// maxa -1
/// spec false
/// args &rest characters
/// retv bool
/// docs
///   True iff all characters are monotonically nondecreasing, except for case.
/// endd
func char_not_greaterp(arglist lob.Object, nargs int) lob.Object {
	return char_cmp(arglist, 0, -1, "char-not-greaterp", nargs)
}

/// name char-not-lessp
/// impl char_not_lessp
/// mina 0
/// maxa -1
/// spec false
/// args &rest characters
/// retv bool
/// docs
///   True iff all characters are monotonically nonincreasing, except for case.
/// endd
func char_not_lessp(arglist lob.Object, nargs int) lob.Object {
	return char_cmp(arglist, 0, 1, "char-not-lessp", nargs)
}

// EOF
