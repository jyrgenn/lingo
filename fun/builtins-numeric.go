// Numeric builtins
package fun

import (
	"math"
	"math/rand"
	"strings"

	"../lob"
	"../msg"
	"../num"

	// "../odep"
	"../olu"
)

// These first few have a function definition in order to increase compatibility
// with Common Lisp, but without complex and rational numbers they cannot be
// implemented. So, getting the function definition of the respective symbols
// works, but calling the functions raises an error.

/// name conjugate
/// impl not_implemented
/// mina 0
/// maxa -1
/// spec false
/// args
/// retv nothing
/// docs
///   Function not implemented; will throw an error.
/// endd
//
/// name cis
/// impl not_implemented
/// mina 0
/// maxa -1
/// spec false
/// args
/// retv nothing
/// docs
///   Function not implemented; will throw an error.
/// endd
//
/// name rationalize
/// impl not_implemented
/// mina 0
/// maxa -1
/// spec false
/// args
/// retv nothing
/// docs
///   Function not implemented; will throw an error.
/// endd
//
/// name rational
/// impl not_implemented
/// mina 0
/// maxa -1
/// spec false
/// args
/// retv nothing
/// docs
///   Function not implemented; will throw an error.
/// endd
func not_implemented(arglist lob.Object, nargs int) lob.Object {
	msg.Bailf("function is not implemented")
	return nil
}

/// name minusp
/// impl minusp
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv boolean
/// docs
///   Return non-nil iff NUMBER is less than zero.
/// endd
func minusp(arglist lob.Object, nargs int) lob.Object {
	if numberArg(arglist.Car(), "minusp: number") < 0 {
		return lob.T
	}
	return lob.Nil
}

/// name plusp
/// impl plusp
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv boolean
/// docs
///   Return non-nil iff NUMBER is greater than zero.
/// endd
func plusp(arglist lob.Object, nargs int) lob.Object {
	if numberArg(arglist.Car(), "plusp: number") > 0 {
		return lob.T
	}
	return lob.Nil
}

/// name integer-length
/// impl integer_length
/// mina 1
/// maxa 1
/// spec false
/// args INTEGER
/// retv boolean
/// docs
///   Returns the number of bits needed to represent INTEGER.
///   If the argument is not an integer, it will be rounded to the nearest one.
/// endd
func integer_length(arglist lob.Object, nargs int) lob.Object {
	integer := roundInt(numberArg(arglist.Car(), "integer-length: integer"))
	mask := int64(0)
	i := 0
	if integer < 0 {
		integer = -integer - 1
	}
	for ; integer != mask&integer; i++ {
		mask = (mask << 1) | 1
	}
	return lob.NewIntNumber(i)
}

// return the number arg as an int, rounded
func roundInt(arg float64) int64 {
	return int64(arg + math.Copysign(0.5, arg))
}

/// name ash
/// impl ash
/// mina 2
/// maxa 2
/// spec false
/// args INTEGER COUNT
/// retv shifted-integer
/// docs
///   Return INTEGER arithmetically shifted by COUNT bit positions.
///   Ash shifts left if COUNT is positive, right if COUNT is negative.
///   The shifted value has the same sign as INTEGER.
/// endd
func ash(arglist lob.Object, nargs int) lob.Object {
	integer := roundInt(numberArg(arglist.Car(), "ash: integer"))
	count := roundInt(numberArg(arglist.Cdr().Car(), "ash: count"))
	if count >= 0 {
		return lob.NewInt64Number(integer << uint(count))
	} else {
		return lob.NewInt64Number(integer >> uint(-count))
	}
}

/// name numerator
/// impl numerator
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv number
/// docs
///   Return the numerator of NUMBER.
///   As only real numbers are supported, the numerator of a number is
///   always the number itself.
/// endd
func numerator(arglist lob.Object, nargs int) lob.Object {
	numob := arglist.Car()
	numberArg(numob, "numerator: number")
	return numob
}

/// name denominator
/// impl denominator
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv 1
/// docs
///   Return the denominator of NUMBER.
///   As only real numbers are supported, the denominator of a number is
///   always 1.
/// endd
func denominator(arglist lob.Object, nargs int) lob.Object {
	numberArg(arglist.Car(), "denominator: number")
	return lob.One
}

/// name realpart
/// impl realpart
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv number
/// docs
///   Return the real part of NUMBER.
///   As only real numbers are supported, the real part of a number is
///   always the number itself.
/// endd
func realpart(arglist lob.Object, nargs int) lob.Object {
	numob := arglist.Car()
	numberArg(numob, "realpart: number")
	return numob
}

/// name imagpart
/// impl imagpart
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv 0
/// docs
///   Return the imaginary part of NUMBER.
///   As only real numbers are supported, the imaginary part of a
///   number is always zero.
/// endd
func imagpart(arglist lob.Object, nargs int) lob.Object {
	numberArg(arglist.Car(), "imagpart: number")
	return lob.Zero
}

/// name phase
/// impl phase
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv phase
/// docs
///   Return the angle part of NUMBER's polar representation.
///   As only real numbers are supported, the return value is only -Pi for
///   negative numbers, Pi for positive numbers, and zero for zero.
/// endd
func phase(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "round: number")
	var result float64
	if num < 0 {
		result = -math.Pi
	} else if num > 0 {
		result = math.Pi
	} else {
		result = 0
	}
	return lob.NewNumber(result)
}

/// name pi
/// impl pi
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv pi
/// docs
///   Return the value of the constant Pi.
/// endd
func pi(arglist lob.Object, nargs int) lob.Object {
	return lob.NewNumber(math.Pi)
}

/// name e
/// impl e
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv e
/// docs
///   Return the value of the Euler constant.
/// endd
func e(arglist lob.Object, nargs int) lob.Object {
	return lob.NewNumber(math.E)
}

/// name ceiling
/// impl ceiling
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv result
/// docs
///   Return NUMBER truncated to an integer towards positive infinity.
/// endd
/// name fceiling
/// impl ceiling
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv result
/// docs
///   Return NUMBER truncated to an integer towards positive infinity.
/// endd
func ceiling(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "ceiling: number")
	return lob.NewNumber(math.Ceil(num))
}

/// name round
/// impl round
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv result
/// docs
///   Return NUMBER rounded to the nearest integer.
/// endd
/// name fround
/// impl round
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv result
/// docs
///   Return NUMBER rounded to the nearest integer.
/// endd
func round(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "round: number")
	return lob.NewNumber(math.Trunc(num + math.Copysign(0.5, num)))
}

/// name floor
/// impl floor
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv result
/// docs
///   Return NUMBER truncated to an integer towards negative infinity.
/// endd
/// name ffloor
/// impl floor
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv result
/// docs
///   Return NUMBER truncated to an integer towards negative infinity.
/// endd
func floor(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "floor: number")
	return lob.NewNumber(math.Floor(num))
}

/// name log
/// impl log
/// mina 1
/// maxa 2
/// spec false
/// args NUMBER &optional BASE
/// retv result
/// docs
///   Return the logarithm of NUMBER in base BASE.
///   If BASE is not supplied, its value is e.
/// endd
func log(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "log: number")
	if nargs == 1 {
		return lob.NewNumber(math.Log(num))
	}
	base := numberArg(arglist.Cdr().Car(), "log: base")
	if base == 10 {
		return lob.NewNumber(math.Log10(num))
	}
	if base == 2 {
		return lob.NewNumber(math.Log2(num))
	}
	return lob.NewNumber(math.Log(num) / math.Log(base))
}

/// name exp
/// impl exp
/// mina 1
/// maxa 1
/// spec false
/// args POWER
/// retv result
/// docs
///   Return e raised to the power of POWER.
/// endd
func exp(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "exp: number")
	return lob.NewNumber(math.Exp(num))
}

/// name atanh
/// impl atanh
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv arc-htangent
/// docs
///   Return the arc hyperbolic tangent of RADIANS.
/// endd
func atanh(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "atanh: number")
	return lob.NewNumber(math.Atanh(num))
}

/// name acosh
/// impl acosh
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv arc-hcosine
/// docs
///   Return the arc hyperbolic cosine of RADIANS.
/// endd
func acosh(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "acosh: number")
	return lob.NewNumber(math.Acosh(num))
}

/// name asinh
/// impl asinh
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv arc-hsine
/// docs
///   Return the arc hyperbolic sine of RADIANS.
/// endd
func asinh(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "asinh: number")
	return lob.NewNumber(math.Asinh(num))
}

/// name tanh
/// impl tanh
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv hyp-tangent
/// docs
///   Return the hyperbolic tangent of RADIANS.
/// endd
func tanh(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "tanh: number")
	return lob.NewNumber(math.Tanh(num))
}

/// name cosh
/// impl cosh
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv hyp-cosine
/// docs
///   Return the hyperbolic cosine of RADIANS.
/// endd
func cosh(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "cosh: number")
	return lob.NewNumber(math.Cosh(num))
}

/// name sinh
/// impl sinh
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv hyp-sine
/// docs
///   Return the hyperbolic sine of RADIANS.
/// endd
func sinh(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "sinh: number")
	return lob.NewNumber(math.Sinh(num))
}

/// name atan
/// impl atan
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv radians
/// docs
///   Return the arc tangent of RADIANS.
/// endd
func atan(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "atan: number")
	return lob.NewNumber(math.Atan(num))
}

/// name acos
/// impl acos
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv radians
/// docs
///   Return the arc cosine of RADIANS.
/// endd
func acos(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "acos: number")
	return lob.NewNumber(math.Acos(num))
}

/// name asin
/// impl asin
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv radians
/// docs
///   Return the arc sine of RADIANS.
/// endd
func asin(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "asin: number")
	return lob.NewNumber(math.Asin(num))
}

/// name tan
/// impl tan
/// mina 1
/// maxa 1
/// spec false
/// args RADIANS
/// retv tangent
/// docs
///   Return the tangent of RADIANS.
/// endd
func tan(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "tan: number")
	return lob.NewNumber(math.Tan(num))
}

/// name cos
/// impl cos
/// mina 1
/// maxa 1
/// spec false
/// args RADIANS
/// retv cosine
/// docs
///   Return the cosine of RADIANS.
/// endd
func cos(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "cos: number")
	return lob.NewNumber(math.Cos(num))
}

/// name sin
/// impl sin
/// mina 1
/// maxa 1
/// spec false
/// args RADIANS
/// retv sine
/// docs
///   Return the sine of RADIANS.
/// endd
func sin(arglist lob.Object, nargs int) lob.Object {
	num := numberArg(arglist.Car(), "sin: number")
	return lob.NewNumber(math.Sin(num))
}

/// name integerp
/// impl integerp
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv bool
/// docs
///   Return t if NUMBER is an integer, nil otherwise
/// endd
func integerp(arglist lob.Object, nargs int) lob.Object {
	theObject := arglist.Car()
	if lob.IsNumber(theObject) {
		num := theObject.(*lob.Number).Value()
		if num == float64(int64(num)) {
			return lob.T
		}
	}
	return lob.Nil
}

/// name abs
/// impl abs
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv absolute-value
/// docs
///   Return the absolute value of NUMBER.
/// endd
func abs(arglist lob.Object, nargs int) lob.Object {
	theNumber := arglist.Car()
	num := numberArg(theNumber, "abs: number")
	if num < 0 {
		return lob.NewNumber(-num)
	}
	return theNumber
}

/// name signum
/// impl signum
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv sign
/// docs
///   Return -1 / 0 / 1 if NUMBER is negative / zero / positive, respectively.
/// endd
func signum(arglist lob.Object, nargs int) lob.Object {
	theNumber := arglist.Car()
	num := numberArg(theNumber, "abs: number")
	if num < 0 {
		return lob.MinusOne
	} else if num > 0 {
		return lob.One
	}
	return lob.Zero
}

/// name truncate
/// impl truncate
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv int
/// docs
///   Truncate NUMBER towards zero.
/// endd
/// name ftruncate
/// impl truncate
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv int
/// docs
///   Truncate NUMBER to an integer towards zero.
/// endd
func truncate(arglist lob.Object, nargs int) lob.Object {
	return lob.NewNumber(math.Trunc(
		numberArg(arglist.Car(), "truncate: number")))
}

/// name random
/// impl random
/// mina 0
/// maxa 2
/// spec false
/// args &optional LIMIT INT
/// retv random-number
/// docs
///   Return a non-negative pseudo-random number less than 1 (or optional LIMIT).
///   If optional INT is non-nil, the returned number is an integer.
/// endd
func random(arglist lob.Object, nargs int) lob.Object {
	limit := 1.0
	intg := false
	if nargs > 0 {
		limit = numberArg(arglist.Car(), "random: limit")
		if nargs > 1 {
			intg = arglist.Cdr().Car() != lob.Nil
		}
	}
	result := rand.Float64() * limit
	if intg {
		result = float64(int(result))
	}
	return lob.NewNumber(result)
}

/// name max
/// impl max
/// mina 1
/// maxa -1
/// spec false
/// args NUMBER &rest NUMBERS
/// retv result
/// docs
///   Return the largest of all arguments.
/// endd
func max(arglist lob.Object, nargs int) lob.Object {
	result := numberArg(arglist.Car(), "max: first")

	arglist = arglist.Cdr()
	for lob.IsPair(arglist) {
		elem := lob.Pop(&arglist)
		val := numberArg(elem, "max: number")
		if val > result {
			result = val
		}
	}
	return lob.NewNumber(result)
}

/// name min
/// impl min
/// mina 1
/// maxa -1
/// spec false
/// args NUMBER &rest NUMBERS
/// retv result
/// docs
///   Return the smallest of all arguments.
/// endd
func min(arglist lob.Object, nargs int) lob.Object {
	result := numberArg(arglist.Car(), "min: first")

	arglist = arglist.Cdr()
	for lob.IsPair(arglist) {
		elem := lob.Pop(&arglist)
		val := numberArg(elem, "min: number")
		if val < result {
			result = val
		}
	}
	return lob.NewNumber(result)
}

/// name prime-number-p
/// impl primeNumberP
/// mina 1
/// maxa 1
/// spec false
/// args INT
/// retv boolish
/// docs
///   Return return t if INT is a prime number, nil otherwise.
/// endd
func primeNumberP(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(num.IsPrime(
		uint64(numberArg(arglist.Car(), "prime-number-p"))))
}

/// name factor
/// impl factor
/// mina 1
/// maxa 1
/// spec false
/// args INT
/// retv list
/// docs
///   Return the list of prime factors of INT.
/// endd
func factor(arglist lob.Object, nargs int) lob.Object {
	return lob.Uint64Slice2List(num.Factor(
		uint64(numberArg(arglist.Car(), "factor"))))
}

/// name next-prime
/// impl next_prime
/// mina 1
/// maxa 1
/// spec false
/// args START
/// retv prime
/// docs
///   Return the first prime number greater than START.
/// endd
func next_prime(arglist lob.Object, nargs int) lob.Object {
	arg := numberArg(arglist.Car(), "start")
	if arg < 0 {
		arg = 0
	}
	return lob.NewInt64Number(int64(num.NextPrime(uint64(arg))))
}

/// name expt
/// impl expt
/// mina 2
/// maxa 2
/// spec false
/// args BASE POWER
/// retv result
/// docs
///   Return BASE raised to the power POWER.
/// endd
//
/// name **
/// impl expt
/// mina 2
/// maxa 2
/// spec false
/// args BASE POWER
/// retv result
/// docs
///   Return BASE raised to the power POWER.
/// endd
func expt(arglist lob.Object, nargs int) lob.Object {
	return lob.NewNumber(math.Pow(
		numberArg(arglist.Car(), "expt: base"),
		numberArg(arglist.Cdr().Car(), "expt: power")))
}

/// name sqrt
/// impl sqrt
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv number
/// docs
///   Return the square root of NUMBER.
/// endd
func sqrt(arglist lob.Object, nargs int) lob.Object {
	return lob.NewNumber(math.Sqrt(numberArg(arglist.Car(), "sqrt:")))
}

/// name isqrt
/// impl isqrt
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv number
/// docs
///   Return the integer square root of NUMBER.
/// endd
func isqrt(arglist lob.Object, nargs int) lob.Object {
	n := numberArg(arglist.Car(), "isqrt:")
	op := uint64(n)
	res := uint64(0)
	one := uint64(1 << 62)

	for one > op {
		one >>= 2
	}
	for one != 0 {
		if op >= res+one {
			op -= res + one
			res += one << 1 // <-- faster than 2 * one
		}
		res >>= 1
		one >>= 2
	}
	return lob.NewInt64Number(int64(res))
}

/// name zerop
/// impl zerop
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv t/nil
/// docs
///   Return t if NUMBER is zero, nil else.
/// endd
func zerop(arglist lob.Object, nargs int) lob.Object {
	n := numberArg(arglist.Car(), "zerop:")
	return lob.Bool2Ob(n == 0)
}

/// name 1-
/// impl sub1
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv number
/// docs
///   Return a value of NUMBER minus 1.
/// endd
func sub1(arglist lob.Object, nargs int) lob.Object {
	n := numberArg(arglist.Car(), "1-:")
	return lob.NewNumber(n - 1)
}

/// name 1+
/// impl add1
/// mina 1
/// maxa 1
/// spec false
/// args NUMBER
/// retv number
/// docs
///   Return a value of NUMBER plus 1.
/// endd
func add1(arglist lob.Object, nargs int) lob.Object {
	n := numberArg(arglist.Car(), "1+:")
	return lob.NewNumber(n + 1)
}

/// name /=
/// impl cmp_ne
/// mina 0
/// maxa -1
/// spec false
/// args &rest NUMBERS
/// retv t/nil
/// docs
///   Return t if all NUMBERS are different, nil else.
/// endd
func cmp_ne(arglist lob.Object, nargs int) lob.Object {
	switch nargs {
	case 0, 1:
		return lob.T
	case 2:
		val0 := comparableArg(arglist.Car(), "/=: first")
		val1 := comparableArg(arglist.Cdr().Car(), "/=: second")
		if val0.Cmp(val1) == 0 {
			return lob.Nil
		}
		return lob.T
	default:
		val0 := comparableArg(arglist.Car(), "/=: first")
		return lob.Bool2Ob(val0.NotEqual(arglist.Cdr()))
	}
}

/// name <=>
/// impl cmp
/// mina 2
/// maxa 2
/// spec false
/// args ITEM1 ITEM2
/// retv int
/// docs
///   Return -1, 0, or 1 if ITEM1 is less than, equal to, or greater than ITEM2.
///   Comparable are symbols, numbers, strings, and characters.
/// endd
func cmp(arglist lob.Object, nargs int) lob.Object {
	item1 := comparableArg(arglist.Car(), "cmp: first")
	item2 := comparableArg(arglist.Cdr().Car(), "cmp: second")
	return lob.NewIntNumber(item1.Cmp(item2))
}

// implementation body of several cmp_* functions
func cmp_all(arglist lob.Object, maybe1, maybe2 int, name string, nargs int) lob.Object {
	if nargs < 2 {
		return lob.T
	}
	start := comparableArg(arglist.Car(), name+": first")
	arglist = arglist.Cdr()
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		next := comparableArg(arg, name+":")
		switch start.Cmp(next) {
		default:
			return lob.Nil
		case maybe1, maybe2:
		}
		start = next
	}
	return lob.T
}

/// name <
/// impl cmp_lt
/// mina 0
/// maxa -1
/// spec false
/// args &rest ITEMS
/// retv t/nil
/// docs
///   Return t if all ITEMS are in strictly ascending order.
///   Comparable are symbols, numbers, strings, and characters.
/// endd
func cmp_lt(arglist lob.Object, nargs int) lob.Object {
	return cmp_all(arglist, -1, -1, "<", nargs)
}

/// name <=
/// impl cmp_le
/// mina 0
/// maxa -1
/// spec false
/// args &rest ITEMS
/// retv t/nil
/// docs
///   Return t if all ITEMS are in ascending order.
///   Comparable are symbols, numbers, strings, and characters.
/// endd
func cmp_le(arglist lob.Object, nargs int) lob.Object {
	return cmp_all(arglist, -1, 0, "<=", nargs)
}

/// name =
/// impl cmp_eq
/// mina 0
/// maxa -1
/// spec false
/// args &rest ITEMS
/// retv t/nil
/// docs
///   Return t if all comparable ITEMS are equal, nil else.
///   Comparable are symbols, numbers, strings, and characters.
/// endd
func cmp_eq(arglist lob.Object, nargs int) lob.Object {
	return cmp_all(arglist, 0, 0, "=", nargs)
}

/// name >=
/// impl cmp_ge
/// mina 0
/// maxa -1
/// spec false
/// args &rest ITEMS
/// retv t/nil
/// docs
///   Return t if all ITEMS are in descending order.
///   Comparable are symbols, numbers, strings, and characters.
/// endd
func cmp_ge(arglist lob.Object, nargs int) lob.Object {
	return cmp_all(arglist, 0, 1, ">=", nargs)
}

/// name >
/// impl cmp_gt
/// mina 0
/// maxa -1
/// spec false
/// args &rest ITEMS
/// retv t/nil
/// docs
///   Return t if all ITEMS are in strictly descending order.
///   Comparable are symbols, numbers, strings, and characters.
/// endd
func cmp_gt(arglist lob.Object, nargs int) lob.Object {
	return cmp_all(arglist, 1, 1, ">", nargs)
}

/// name %
/// impl mod
/// mina 2
/// maxa 2
/// spec false
/// args NUM1 NUM2
/// retv number
/// docs
///   Return modulo of NUM1 divided by NUM2 (which must be integers).
/// endd
func mod(arglist lob.Object, nargs int) lob.Object {
	arg1 := numberArg(arglist.Car(), "%: first")
	arg2 := numberArg(arglist.Cdr().Car(), "%: second")
	return lob.NewInt64Number(int64(arg1) % int64(arg2))
}

/// name mod
/// impl mMod
/// mina 2
/// maxa 2
/// spec false
/// args NUM1 NUM2
/// retv number
/// docs
///   Return modulo of NUM1 divided by NUM2.
/// endd
func mMod(arglist lob.Object, nargs int) lob.Object {
	arg1 := numberArg(arglist.Car(), "mod: first")
	arg2 := numberArg(arglist.Cdr().Car(), "mod: second")
	return lob.NewNumber(math.Mod(arg1, arg2))
}

/// name div
/// impl iDiv
/// mina 2
/// maxa 2
/// spec false
/// args NUM1 NUM2
/// retv number
/// docs
///   Return the result of the integer division of NUM1 by NUM2.
/// endd
func iDiv(arglist lob.Object, nargs int) lob.Object {
	arg1 := numberArg(arglist.Car(), "mod: first")
	arg2 := numberArg(arglist.Cdr().Car(), "mod: second")
	return lob.NewNumber(math.Trunc(arg1 / arg2))
}

/// name rem
/// impl mRem
/// mina 2
/// maxa 2
/// spec false
/// args NUM1 NUM2
/// retv number
/// docs
///   Return remainder of NUM1 divided by NUM2.
/// endd
func mRem(arglist lob.Object, nargs int) lob.Object {
	arg1 := numberArg(arglist.Car(), "rem: first")
	arg2 := numberArg(arglist.Cdr().Car(), "rem: second")
	return lob.NewNumber(math.Remainder(arg1, arg2))
}

/// name /
/// impl div
/// mina 1
/// maxa -1
/// spec false
/// args NUM1 &rest NUMBERS
/// retv number
/// docs
///   Return NUM1 divided by all NUMBERS, or the inverse of sole arg NUM1.
/// endd
func div(arglist lob.Object, nargs int) lob.Object {
	first, rest := arglist.Cxr()
	result := numberArg(first, "/: first")
	if rest.IsNil() {
		return lob.NewNumber(1 / result)
	}
	for lob.IsPair(rest) {
		first = lob.Pop(&rest)
		result /= numberArg(first, "/:")
	}
	return lob.NewNumber(result)
}

/// name *
/// impl mult
/// mina 0
/// maxa -1
/// spec false
/// args &rest NUMBERS
/// retv number
/// docs
///   Return the product of all NUMBERS.
/// endd
func mult(arglist lob.Object, nargs int) lob.Object {
	result := float64(1)
	for lob.IsPair(arglist) {
		first := lob.Pop(&arglist)
		result *= numberArg(first, "*:")
	}
	return lob.NewNumber(result)
}

/// name -
/// impl minus
/// mina 1
/// maxa -1
/// spec false
/// args NUM1 &rest NUMBERS
/// retv number
/// docs
///   Return NUM1 minus all NUMBERS, or the negation of sole arg NUM1.
/// endd
func minus(arglist lob.Object, nargs int) lob.Object {
	first, rest := arglist.Cxr()
	result := numberArg(first, "-: first")
	if rest.IsNil() {
		return lob.NewNumber(-result)
	}
	for lob.IsPair(rest) {
		first = lob.Pop(&rest)
		result -= numberArg(first, "-:")
	}
	return lob.NewNumber(result)
}

/// name +
/// impl plus
/// mina 0
/// maxa -1
/// spec false
/// args &rest NUMBERS
/// retv number
/// docs
///   Return the sum of the NUMBERS.
/// endd
func plus(arglist lob.Object, nargs int) lob.Object {
	result := float64(0)
	for lob.IsPair(arglist) {
		first := lob.Pop(&arglist)
		result += numberArg(first, "+:")
	}
	return lob.NewNumber(result)
}

// see _morgue/digits.c
var digitWeights = [128]int{
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7,
	8, 9, -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18,
	19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1,
	-1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
	23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1,
}

/// name digit-char-p
/// impl digit_char_p
/// mina 0
/// maxa 2
/// spec false
/// args char &optional radix
/// retv weight
/// docs
///   If CHAR is a digit in the given RADIX, return its integer value, or nil.
///   The radix must be in the range 2..36.
/// endd
func digit_char_p(arglist lob.Object, nargs int) lob.Object {
	c := charArg(arglist.Car(), "character")
	radix := 10
	if nargs > 1 {
		radix = int(numberArg(arglist.Cdr().Car(), "radix"))
	}
	if radix < 2 || radix > 36 {
		msg.Bailf("radix must be in the range 2..36 (is %d)", radix)
	}
	if c > 'z' || c < '0' {
		return lob.Nil
	}
	pval := digitWeights[c]
	if pval >= radix || pval < 0 {
		return lob.Nil
	}
	return lob.NewIntNumber(pval)
}

/// name parse-integer
/// impl parse_integer
/// mina 1
/// maxa 5
/// spec false
/// args string &optional start end radix junk-allowed
/// retv integer
/// docs
///   If CHAR is a digit in the given RADIX, return its integer value, or nil.
///   The radix must be in the range 2..36.
/// endd
func parse_integer(arglist lob.Object, nargs int) lob.Object {
	the_string := lob.Pop(&arglist).ValueString(nil)
	start := 0
	end := -1
	radix := 10
	junk_allowed := lob.Object(lob.Nil)
	if nargs > 1 {
		start = intArg(lob.Pop(&arglist), "start")
		if nargs > 2 {
			end = intArgOr(lob.Pop(&arglist), "end", -1)
			if nargs > 3 {
				radix = intArg(lob.Pop(&arglist), "radix")
				if nargs > 4 {
					junk_allowed = lob.Pop(&arglist)
				}
			}
		}
	}
	if start != 0 || end != -1 {
		the_string = olu.RuneSubstr(the_string, start, end)
	}
	if junk_allowed != lob.Nil {
		the_string = strings.TrimSpace(the_string)
	}

	result := 0
	sign := 1
	pre_sign := true
	for _, ch := range the_string {
		switch ch {
		case '-':
			if pre_sign {
				sign = -1
				pre_sign = false
			} else {
				msg.Bailf("parse-integer: invalid char '-' "+
					"for integer of radix %d", radix)
			}
		case '+':
			pre_sign = false
		default:
			if ch >= 128 ||
				digitWeights[ch] < 0 ||
				digitWeights[ch] > radix {

				if junk_allowed != lob.Nil {
					return lob.Nil
				}
				msg.Bailf("parse-integer: invalid char '%c' "+
					"for integer of radix %d", ch, radix)

			} else {
				result *= radix
				result += digitWeights[ch]
			}
		}
	}
	return lob.NewIntNumber(result * sign)
}

/// name easter
/// impl easter
/// mina 1
/// maxa 1
/// spec false
/// args year
/// retv (year month mday)
/// docs
///   Calculate the easter date for YEAR. Return a list (year month mday).
///   The formula to calculate the date is according to Lichtenberg as cited by
///   Wikipedia in
///   https://de.wikipedia.org/wiki/Gau%C3%9Fsche_Osterformel#Eine_erg.C3.A4nzte_Osterformel
/// endd
func easter(arglist lob.Object, nargs int) lob.Object {
	X := intArg(arglist.Car(), "year")
	K := X / 100
	M := 15 + (3*K+3)/4 - (8*K+13)/25
	S := 2 - (3*K+3)/4
	A := X % 19
	D := (19*A + M) % 30
	R := (D + A/11) / 29
	OG := 21 + D - R
	SZ := 7 - (X+X/4+S)%7
	OE := 7 - (OG-SZ)%7
	OS := OG + OE
	month := 3
	mday := OS
	if OS > 31 {
		month = 4
		mday -= 31
	}
	return lob.Cons(lob.NewIntNumber(X),
		lob.Cons(lob.NewIntNumber(month),
			lob.Cons(lob.NewIntNumber(mday), lob.Nil)))
}

// EOF
