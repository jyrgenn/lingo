// Lingo system interface builtins
package fun

import (
	"bytes"
	"fmt"
	"runtime"
	"strings"

	"../call"
	"../dep"
	"../eval"
	"../hlp"
	"../lob"
	"../msg"
	"../sys"
)

/// name function-definition
/// impl function_definition
/// mina 1
/// maxa 1
/// spec false
/// args function
/// retv function definition form
/// docs
///   Return a FUNCTION's definition form.
///   FUNCTION must be a lambda function or a macro, not a builtin.
///   The function definition returned contains the real function body,
///   not a copy.
/// endd
func function_definition(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	return functionArg(arg, "function-definition:").Definition()
}

/// name function-arglist
/// impl function_arglist
/// mina 1
/// maxa 1
/// spec false
/// args function
/// retv argument list
/// docs
///   Return a FUNCTION's argument list.
///   FUNCTION must be a lambda function or a macro, not a builtin.
/// endd
func function_arglist(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	return functionArg(arg, "function-arglist:").Arglist()
}

/// name function-body
/// impl function_body
/// mina 1
/// maxa 1
/// spec false
/// args function
/// retv function body
/// docs
///   Return a FUNCTION's body.
///   FUNCTION must be a lambda function or a macro, not a builtin.
///   The function body returned is the real function body, not a copy.
/// endd
func function_body(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	return functionArg(arg, "function-body:").Body()
}

/// name function-docstring
/// impl function_docstring
/// mina 1
/// maxa 1
/// spec false
/// args function
/// retv function docstring
/// docs
///   Return a FUNCTION's documentation string.
///   FUNCTION must be a lambda function, not a builtin or a macro.
/// endd
func function_docstring(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	return lob.NewString(callableArg(arg, "function-docstring:").
		Docstring())
}

/// name env-table
/// impl env_table
/// mina 0
/// maxa 2
/// spec false
/// args &optional ENVIRONMENT NOPARENTS
/// retv var-table
/// docs
///   Return the variables in the environment and their values as a table
/// endd
func env_table(arglist lob.Object, nargs int) lob.Object {
	var env *lob.Environment
	var noparents lob.Object = lob.Nil
	if arglist.IsNil() {
		env = lob.TheEnvironment()
	} else {
		arg0 := lob.Pop(&arglist)
		env = envArg(arg0, "env-table:")
		noparents = arglist.Car()
	}
	themap := env.KeyMap(!noparents.IsNil())
	table := lob.NewMapTable(themap)
	return table
}

/// name env-vars
/// impl env_vars
/// mina 0
/// maxa 2
/// spec false
/// args &optional ENVIRONMENT NOPARENTS
/// retv var-list
/// docs
///   Return the variables defined in the current (or specified) environment
/// endd
func env_vars(arglist lob.Object, nargs int) lob.Object {
	var env *lob.Environment
	var noparents lob.Object = lob.Nil
	if arglist.IsNil() {
		env = lob.TheEnvironment()
	} else {
		arg0 := lob.Pop(&arglist)
		env = envArg(arg0, "env-vars:")
		noparents = arglist.Car()
	}
	return env.Vars(!noparents.IsNil())
}

/// name the-environment
/// impl the_environment
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv environment
/// docs
///   Return the current environment
/// endd
func the_environment(arglist lob.Object, nargs int) lob.Object {
	return lob.TheEnvironment()
}

/// name collect-performance-data
/// impl collectPerfdata
/// mina 0
/// maxa -1
/// spec true
/// args &rest BODYFORMS
/// retv "(evals pairs seconds)"
/// docs
///   Evaluate BODYFORMS and return a list of performance data.
///   This list consists of the number of evals performed, the number of pairs
///   created, and the number of seconds it took to do the evaluations.
/// endd
func collectPerfdata(arglist lob.Object, nargs int) lob.Object {
	perf := hlp.StartPerfdata()
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		lob.EvalCurEnv(arg)
	}
	evals, pairs, seconds := perf.Stop().Values()
	return lob.ListOf(
		lob.Cons(lob.Intern("evals"), lob.NewIntNumber(int(evals))),
		lob.Cons(lob.Intern("pairs"), lob.NewIntNumber(int(pairs))),
		lob.Cons(lob.Intern("secs"), lob.NewNumber(seconds)))
}

/// name doc
/// impl functionDoc
/// mina 1
/// maxa 3
/// spec false
/// args CALLABLE &opt BRIEF NOPRINT
/// retv docstring
/// docs
///   Print documentation or, with non-nil BRIEF, only synopsis of CALLABLE.
///   If NOPRINT is non-nil, return the string instead.
/// endd
func functionDoc(arglist lob.Object, nargs int) lob.Object {
	fun := call.EvalCallable(arglist.Car(), false)
	brief := !(nargs <= 1 || arglist.Cdr().Car().IsNil())
	noprint := nargs > 2 && !arglist.Cdr().Cdr().Car().IsNil()

	docbuf := bytes.NewBufferString("")
	docbuf.WriteString(fun.Deftype())
	docbuf.WriteRune(' ')
	docbuf.WriteString(fun.Synopsis())
	if !brief {
		docs := fun.Docstring()
		if docs != "" {
			docbuf.WriteRune('\n')
			docbuf.WriteString(docs)
		}
	}
	if noprint {
		return lob.NewString(docbuf.String())
	}
	fmt.Println(docbuf)
	return lob.T
}

/// name systats
/// impl systats
/// mina 0
/// maxa 1
/// spec false
/// args &optional ARG
/// retv a map
/// docs
///   Return a system statistics count value for ARG, or a map of all values.
///   The keys of the map are builtins, lambdas, pairs, symbols.
/// endd
func systats(arglist lob.Object, nargs int) lob.Object {
	var stats = make(map[lob.Key]lob.Object)
	stats[lob.Intern("pairs")] = lob.NewIntNumber(lob.NPairs())
	stats[lob.Intern("symbols")] = lob.NewIntNumber(lob.NSymbols())
	stats[lob.Intern("lambdas")] = lob.NewIntNumber(NLambdas())
	stats[lob.Intern("builtins")] = lob.NewIntNumber(NBuiltins())
	if nargs == 0 {
		return lob.NewMapTable(stats)
	}
	value, present := stats[arglist.Car()]
	if present {
		return value
	}
	return lob.Nil
}

func make_format_args(arglist lob.Object, nargs int) (string, []interface{}) {
	format := arglist.Car().ValueString(nil)
	arglist = arglist.Cdr()
	fargs := make([]interface{}, nargs-1)
	index := 0
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		switch val := arg.(type) {
		case *lob.Number:
			fargs[index] = val.MaybeIntVal()
		case *lob.String, *lob.Char:
			fargs[index] = val.ValueString(nil)
		default:
			fargs[index] = val
		}
		index++
	}
	return format, fargs
}

/// name warning
/// impl warning
/// mina 1
/// maxa -1
/// spec false
/// args MESSAGE &rest ARGS
/// retv nil
/// docs
///   Raise warning with MESSAGE (a format string) and optional ARGS.
///   If warnings are treated as errors (i.e. sys:warnings-as-errors is
///   true, the warning exits all active calls immediately, except for
///   errset. Otherwise, only the MESSAGE is printed as a warning,
///   formatted as specified.
/// endd
func warning(arglist lob.Object, nargs int) lob.Object {
	if nargs > 1 {
		format, fargs := make_format_args(arglist, nargs)
		msg.Warnf(format, fargs...)
	} else {
		msg.Warn(arglist.Car().ValueString(nil))
	}
	return lob.Nil
}

/// name error
/// impl error
/// mina 1
/// maxa -1
/// spec false
/// args MESSAGE &rest ARGS
/// retv "(does not return)"
/// docs
///   Raise error with the message MESSAGE (a format string) and optional ARGS.
///   The error exits all active calls immediately, except for errset.
/// endd
func error(arglist lob.Object, nargs int) lob.Object {
	if nargs > 1 {
		format, fargs := make_format_args(arglist, nargs)
		msg.Bailf(format, fargs...)
	} else {
		msg.Bail(arglist.Car().ValueString(nil))
	}
	return nil
}

/// name apropos-list
/// impl apropos_list
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv list of symbols
/// docs
///   Return a list of all symbols whose name matches ARG.
///   ARG may be a regexp object; otherwise, a substring match is done.
/// endd
func apropos_list(arglist lob.Object, nargs int) lob.Object {
	search := arglist.Car()
	var result lob.Object = lob.Nil
	switch match := search.(type) {
	case *lob.Regexp:
		re := match.Regexp()
		for _, sym := range lob.Symbols() {
			if re.MatchString(sym.String()) {
				result = lob.Cons(sym, result)
			}
		}
	default:
		substring := match.ValueString(nil)
		for _, sym := range lob.Symbols() {
			if strings.Index(sym.String(), substring) > -1 {
				result = lob.Cons(sym, result)
			}
		}
	}
	return result
}

/// name apropos
/// impl apropos
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv list of symbols
/// docs
///   Show all symbols whose name matches ARG.
///   ARG may be a regexp object; otherwise, a substring match is done.
/// endd
func apropos(arglist lob.Object, nargs int) lob.Object {
	list := apropos_list(arglist, nargs)
	for lob.IsPair(list) {
		elem := lob.Pop(&list)
		sym := elem.(*lob.Symbol)
		fmt.Printf("%-32s", sym.ValueString(nil))
		var needComma bool
		function := sym.Function()
		if function != nil {
			if IsMacro(function) {
				fmt.Print("macro")
			} else {
				fmt.Print("function")
			}
			needComma = true
		}
		iskeyword := lob.IsKeyword(sym)
		if iskeyword {
			if needComma {
				fmt.Print(", ")
			}
			fmt.Print("keyword")
			needComma = true
		} else if sym.Value() != nil {
			if needComma {
				fmt.Print(", ")
			}
			fmt.Print("variable")
			needComma = true
		}
		props := sym.Properties().(lob.Sequence)
		sysprop := !sym.SysProp().IsNil()
		if !props.IsNil() && !(props.Length() == 1 && sysprop) {
			if needComma {
				fmt.Print(", ")
			}
			fmt.Print("properties")
			needComma = true
		}
		if sym.IsImmutable() && !iskeyword {
			if needComma {
				fmt.Print(", ")
			}
			fmt.Print("immutable")
			needComma = true
		}
		if sysprop {
			if needComma {
				fmt.Print(", ")
			}
			fmt.Print("system")
			needComma = true
		}

		fmt.Println("")
	}
	return lob.Nil
}

/// name fmakunbound
/// impl fmakunbound
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv the symbol
/// docs
///   Make SYMBOL's function be undefined, return SYMBOL.
/// endd
func fmakunbound(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "makunbound: first")
	sym.SetFunction(nil)
	return sym
}

/// name makunbound
/// impl makunbound
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv the symbol
/// docs
///   Make SYMBOL's value be undefined, return SYMBOL.
/// endd
func makunbound(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "makunbound: first")
	sym.SetValue(nil)
	return sym
}

/// name describe
/// impl describe
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv a list
/// docs
///   Return a list of SYMBOL's type, name, value, function, and property list.
///   The third element is a list of 'val and the value, if defined, otherwise
///   a singleton list (val). Similarly the fourth element; the fifth element is
///   a pair of 'props and the property list.
/// endd
func describe(arglist lob.Object, nargs int) lob.Object {
	typesym := lob.Intern(arglist.Car().Type().Name())
	switch item := arglist.Car().(type) {
	case *lob.Symbol:
		fun := item.Function()
		if fun == nil {
			fun = lob.Nil
		} else {
			fun = lob.Cons(fun, lob.Nil)
		}
		value := item.Value()
		var valuelist lob.Object
		if value == nil {
			valuelist = lob.Nil
		} else {
			valuelist = lob.ListOf(value)
		}
		props := item.Properties()
		return lob.ListOf(typesym, item,
			lob.Cons(lob.Intern("val"), valuelist),
			lob.Cons(lob.Intern("fun"), fun),
			lob.Cons(lob.Intern("props"), props))
	case *lob.Vector, *lob.Pair, *lob.String:
		return lob.ListOf(typesym,
			lob.NewIntNumber(item.(lob.Sequence).Length()), item)
	case *Function:
		return lob.ListOf(lob.Intern(item.Ftype()),
			lob.Intern(item.Deftype()),
			lob.NewString(item.Synopsis()),
			item)
	case *lob.Number:
		return lob.ListOf(typesym, item)
	case *lob.Char:
		return lob.ListOf(typesym, item)
	case *lob.Table:
		return lob.ListOf(typesym, lob.NewIntNumber(item.Count()), item)
	case *lob.Port:
		return lob.ListOf(typesym, item)
	case *lob.Netaddr:
		return lob.ListOf(typesym, item)
	case *lob.Regexp:
		return lob.ListOf(typesym, item)
	default:
		return lob.ListOf(typesym, item)
	}
}

/// name trace
/// impl trace
/// mina 1
/// maxa 2
/// spec false
/// args ITEM &optional ON
/// retv t/nil
/// docs
///   Read or set the trace status of ITEM; t for tracing on, nil for off.
///   ITEM can be a function (traces calls), a symbol (traces value and
///   function get/set and property changes) or one of sys:call, sys:eval.
/// endd
func trace(arglist lob.Object, nargs int) lob.Object {
	switch item := arglist.Car().(type) {
	case lob.Traceable:
		if nargs > 1 {
			item.SetTrace(lob.Ob2Bool(arglist.Cdr().Car()))
		}
		return lob.Bool2Ob(item.GetTrace())
	default:
		msg.Bail("trace: argument not function or symbol:",
			arglist.Car())
	}
	return nil
}

/// name garbage-collect
/// impl gc
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv nil
/// docs
///   Run a garbage collection and return nil.
/// endd
func gc(arglist lob.Object, nargs int) lob.Object {
	runtime.GC()
	return lob.Nil
}

/// name sysprops
/// impl sysprops
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv list
/// docs
///   Return all system properties as (SYMBOL . VALUE) pairs.
/// endd
func sysprops(arglist lob.Object, nargs int) lob.Object {
	return sys.AllProps()
}

/// name exit
/// impl exit
/// mina 0
/// maxa 1
/// spec false
/// args &optional STATUS
/// retv "(does not return)"
/// docs
///   Exit the process and return STATUS (or zero) to the operating system.
/// endd
func exit(arglist lob.Object, nargs int) lob.Object {
	var status int
	if nargs == 0 {
		status = 0
	} else {
		switch arg := arglist.Car().(type) {
		case *lob.Number:
			status = int(arg.Value())
		default:
			if arg.IsNil() {
				status = 0
			} else {
				status = 1
			}
		}
	}
	dep.Exit(status)
	panic("dep.Exit() did not exit!")
}

/// name sys:symbols
/// impl sys_symbols
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv list
/// docs
///   Return a sorted list of all defined symbols.
/// endd
func sys_symbols(arglist lob.Object, nargs int) lob.Object {
	result := lob.Object(lob.Nil)
	for _, sym := range lob.Symbols() {
		result = lob.Cons(sym, result)
	}
	return result
}

/// name with-environment
/// impl with_environment
/// mina 1
/// maxa -1
/// spec true
/// args ENVIRONMENT &rest BODYFORMS
/// retv last value
/// docs
///   Evaluate BODYFORMS in ENVIRONMENT and return the last value.
/// endd
func with_environment(arglist lob.Object, nargs int) lob.Object {
	envarg, bodyforms := arglist.Cxr()
	var newEnv *lob.Environment
	if !envarg.IsNil() {
		newEnv = envArg(lob.EvalCurEnv(envarg), "environment")
	}
	result := lob.Object(lob.Nil)
	for lob.IsPair(bodyforms) {
		expr := lob.Pop(&bodyforms)
		result = lob.Eval(expr, newEnv)
	}
	return result
}

/// name new-environment
/// impl new_environment
/// mina 0
/// maxa 2
/// spec false
/// args &optional PARENT-ENVIRONMENT VALUE-TABLE
/// retv the environment
/// docs
///   Return a new environment. Optional PARENT-ENVIRONMENT is the parent,
///   otherwise the current environment. If PARENT-ENVIRONMENT is nil, there
///   is no parent environment, i.e. a top-level environment is created; if
///   PARENT-ENVIRONMENT is t (the default), the parent is the current
///   environment. If VALUE-TABLE is non-nil, it is a table with symbol/value
///   pairs to populate the new environment.
/// endd
func new_environment(arglist lob.Object, nargs int) lob.Object {
	var parent *lob.Environment
	var values *lob.Table
	if nargs > 0 {
		arg := arglist.Car()
		if arg.IsNil() {
			parent = nil
		} else if arg.Eq(lob.T) {
			parent = lob.TheEnvironment()
		} else {
			parent = envArg(arg, "parent environment")
		}
		if nargs > 1 {
			values = tableArg(arglist.Cdr().Car(), "value table")
		}
	} else {
		parent = lob.TheEnvironment()
	}
	return lob.NewEnvironment(parent, values)
}

/// name assert
/// impl assert
/// mina 1
/// maxa 2
/// spec true
/// args test-form &optional message
/// retv nil
/// docs
///   Evaluates TEST-FORM, and if nil, raises an error.
///   The error messages includes TEST-FORM, or, if present, MESSAGE
///   (which is evaluated in that case).
/// endd
func assert(arglist lob.Object, nargs int) lob.Object {
	testform, arglist := arglist.Cxr()
	var message lob.Object
	evalMessage := false
	if nargs > 1 {
		message = arglist.Car()
		evalMessage = true
	} else {
		message = testform
	}

	if eval.EvalCurEnv(testform) == lob.Nil {
		var text lob.Object
		if evalMessage {
			text = eval.EvalCurEnv(message)
		} else {
			text = message
		}
		msg.Bailf("assertion failed: %s", text.ValueString(nil))
	}

	return lob.Nil
}

// EOF
