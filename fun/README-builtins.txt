For each Lisp builtin function, all these attributes MUST be present
in exactly this order, with the comment starting in the first column
with three slashes and a blank before and after the attribute name
(except docs):

/// name symbol name of the builtin function in Lisp
/// impl name of the Go implementation function in the "fun" module
/// mina minimum number of arguments
/// maxa maximum number of arguments, or -1 for arbitrary
/// spec true if function is a special form, false otherwise
/// args names of arguments and &rest/&optional for Documentation
/// retv terse symbolic return value description
/// docs
///   The documentation string of the function, indented by two blanks, and
///   ended by "endd" as shown here
/// endd

