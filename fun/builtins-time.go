// Time function builtins
package fun

import (
	"fmt"
	"math"
	"time"

	"../lob"
	"../olu"
)

// This is start of the Unix epoch, 1970-01-01Z00:00:00, in CL's universal time,
// i.e. the number of seconds after 1900-01-01Z00:00:00.
const universalUnixEpochOffset int64 = 2208988800

// convert Go's Time CL's universal time
func time2universal(t time.Time) int64 {
	return t.Unix() + universalUnixEpochOffset
}

// convert Go's Time CL's universal time with (fractional) nanoseconds
func time2universal_ns(t time.Time) float64 {
	return float64(t.Unix()+universalUnixEpochOffset) +
		(float64(t.Nanosecond()) / 1e9)
}

// convert CL's universal time with (fractional) nanoseconds to Go's Time
func universal2time_ns(ut float64) time.Time {
	ut_int := int64(ut)
	ut_frac := ut - float64(ut_int)
	return time.Unix(ut_int-universalUnixEpochOffset,
		int64(ut_frac*1e9))
}

// decode the Go Time t (which is already in the destination time zone) to the
// (second minute hour mday month year weekday daylight-p time-zone) list; the
// daylight-p is faked to nil, currently
func time_decode(the_time time.Time) lob.Object {
	hour, min, sec := the_time.Clock()
	year, month, mday := the_time.Date()
	wday := the_time.Weekday()
	_, zone := the_time.Zone()
	return lob.Slice2list([]lob.Object{
		lob.NewIntNumber(sec),
		lob.NewIntNumber(min),
		lob.NewIntNumber(hour),
		lob.NewIntNumber(mday),
		lob.NewIntNumber(int(month)),
		lob.NewIntNumber(year),
		lob.NewIntNumber(int(wday)),
		lob.Nil, // faked daylight-p
		lob.NewNumber(float64(zone) / 3600)})
}

/// name decode-universal-time
/// impl decode_universal_time
/// mina 1
/// maxa 2
/// spec false
/// args universal-time &optional time-zone
/// retv (second minute hour mday month year weekday daylight-p time-zone)
/// docs
///   Return the decoded time represented by the given UNIVERSAL-TIME.
///   Due to (current?) limitations of the underlying Go language, the
///   daylight-p component of the result will always be nil. The
///   time-zone offset component should be correctly change with daylight
///   savings time, though.
/// endd
func decode_universal_time(arglist lob.Object, nargs int) lob.Object {
	var timezone float64
	have_timezone := false
	univ_time := int64(intArg(lob.Pop(&arglist),
		"decode-universal-time: universal-time"))
	if nargs > 1 {
		timezone = numberArg(lob.Pop(&arglist),
			"decode-universal-time: time-zone")
		have_timezone = true
	}
	var the_time time.Time
	if have_timezone {
		the_time = time.Unix(univ_time-universalUnixEpochOffset, 0).
			In(timezone2location(timezone))
	} else {
		the_time = time.Unix(univ_time-universalUnixEpochOffset, 0)
	}
	return time_decode(the_time)
}

func timezone2location(timezone float64) *time.Location {
	seconds := timezone * 3600
	minutes := int(math.Abs(float64(int(seconds) % 60)))
	tz_name := fmt.Sprintf("%+02d%02d", int(timezone), minutes)
	return time.FixedZone(tz_name, int(seconds))
}

/// name encode-universal-time
/// impl encode_universal_time
/// mina 6
/// maxa 7
/// spec false
/// args second minute hour date month year &optional time-zone
/// retv universal-time
/// docs
///   Convert a time from decoded time format to universal time and return it.
/// endd
func encode_universal_time(arglist lob.Object, nargs int) lob.Object {
	second := intArg(lob.Pop(&arglist), "encode-universal-time: second")
	minute := intArg(lob.Pop(&arglist), "encode-universal-time: minute")
	hour := intArg(lob.Pop(&arglist), "encode-universal-time: hour")
	date := intArg(lob.Pop(&arglist), "encode-universal-time: date")
	month := intArg(lob.Pop(&arglist), "encode-universal-time: month")
	year := intArg(lob.Pop(&arglist), "encode-universal-time: year")
	var timezone float64
	have_timezone := false
	if nargs > 6 {
		timezone = numberArg(lob.Pop(&arglist),
			"encode-universal-time): time-zone")
		have_timezone = true
	}
	var tzloc *time.Location
	if have_timezone {
		tzloc = timezone2location(timezone)
	} else {
		tzloc = time.Local
	}
	the_time := time.Date(year, time.Month(month), date,
		hour, minute, second, 0, tzloc)
	return lob.NewInt64Number(time2universal(the_time))
}

/// name get-universal-time
/// impl get_universal_time
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv universal-time
/// docs
///   Return the current time as universal time.
///   The universal time are the seconds since 1900-01-01Z00:00:00.
///
/// endd
func get_universal_time(arglist lob.Object, nargs int) lob.Object {
	return lob.NewInt64Number(time2universal(time.Now()))
}

/// name get-universal-time-ns
/// impl get_universal_time_ns
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv universal-time with nanoseconds
/// docs
///   Return the current time as universal time with nanoseconds
///   The universal time are the seconds since 1900-01-01Z00:00:00.
///
/// endd
func get_universal_time_ns(arglist lob.Object, nargs int) lob.Object {
	return lob.NewNumber(time2universal_ns(time.Now()))
}

/// name get-unix-time
/// impl get_unix_time
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv unix-timestamp
/// docs
///   Return the current time as a Unix time stamp in whole seconds.
///   The Unix time stamp are the seconds since 1970-01-01Z00:00:00.
///
/// endd
func get_unix_time(arglist lob.Object, nargs int) lob.Object {
	return lob.NewInt64Number(time.Now().Unix())
}

/// name get-decoded-time
/// impl get_decoded_time
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv (second minute hour mday month year weekday daylight-p time-zone)
/// docs
///   Return the current time as decoded time.
///   Due to (current?) limitations of the underlying Go language, the
///   daylight-p component of the result will always be nil. The
///   time-zone offset component should be correctly change with daylight
///   savings time, though.
/// endd
func get_decoded_time(arglist lob.Object, nargs int) lob.Object {
	return time_decode(time.Now())
}

/// name format-universal-time
/// impl format_universal_time
/// mina 1
/// maxa 2
/// spec false
/// args format-string &optional universal-time
/// retv formatted time string
/// docs
///   Format the universal-time argument (default: now) according to the format
///   and return the formatted string.
///   This implements a subset of the POSIX strftime(3) functionality.
///   Locales and modifiers (E, O, field width, plus, minus) are not
///   supported, but gracefully ignored. All conversion specifiers should
///   otherwise work as specified in POSIX. The specifiers for fractional
///   seconds and epoch nanoseconds (%i, %J, %K, %L) are non-standard
///   extensions.
///
///   Conversion specifiers are written as '%" and a letter. Everything
///   else is treated as text to be printed literally. Specifiers and their
///   replacements are:
///   %a : abbreviated weekday name
///   %A : full weekday name
///   %b : abbreviated month name
///   %B : full month name
///   %c : appropriate date and time representation
///   %C : year divided by 100 and truncated to an integer, as a decimal number
///   %d : day of the month as a decimal number [01, 31]
///   %D : american date format %m/%d/%y, e.g. "10/11/12"
///   %e : day of the month as a decimal number [1,31]; a single digit is
///        preceded by a space
///   %F : Equivalent to %Y-%m-%d, e.g. "2017-02-14"
///   %g : last 2 digits of the week-based year as a decimal number [00,99]
///   %G : week-based year as a decimal number, e.g. "1977"
///   %h : the same as %b
///   %H : hour (24-hour clock) as a decimal number [00,23]
///   %i : second fraction as milliseconds [000,999]
///   %I : hour (12-hour clock) as a decimal number [01,12]
///   %j : day of the year as a decimal number [001,366]
///   %J : second fraction as microseconds [000000,999999]
///   %k : hour as a decimal number [ 0,23]; single digits are preceded by a
///        blank
///   %K : second fraction as nanoseconds [000000000,999999999]
///   %l : hour as a decimal number [ 1,12]; single digits are preceded by a
///        blank
///   %L : nanoseconds since begin of the Unix epoch
///   %m : month as a decimal number [01,12]
///   %M : minute as a decimal number [00,59]
///   %n : a <newline>
///   %p : locale's equivalent of either AM or PM.
///   %r : the time in AM and PM notation, e.g. "02:43:59 PM"
///   %R : time in 24-hour notation, e.g. 19:21
///   %s : seconds since begin of the Unix epoch (1970-01-01 00:00:00 UTC)
///   %S : second as a decimal number [00,61]
///   %t : a <tab>
///   %T : the time in ISO 8601 format (%H:%M:%S), e.g. "14:45:34"
///   %u : the weekday as a decimal number [1,7], with 1 representing Monday
///   %U : week number of the year as a decimal number [00,53]
///        The first Sunday of January is the first day of week 1; days in the
///        new year before this are in week 0
///   %v : %e-%b-%Y, e.g. " 2-Feb-2017" (BSD extension? is in FreeBSD and OS X)
///   %V : (ISO week number) week number of the year (Monday as the first day of
///        the week) as a decimal number [01,53]. If the week containing 1
///        January has four or more days in the new year, then it is considered
///        week 1. Otherwise, it is the last week of the previous year, and the
///        next week is week 1. Both January 4th and the first Thursday of
///        January are always in week 1.
///   %w : weekday as a decimal number [0,6], with 0 representing Sunday
///   %W : week number of the year as a decimal number [00,53]
///   %x : ISO 8601 date representation
///   %X : ISO 8601 time representation
///   %y : last two digits of the year as a decimal number [00,99]
///   %Y : year as a decimal number (for example, 1997)
///   %z : offset from UTC in the ISO 8601:2000 standard format (+hhmm or
///        -hhmm), or no characters if no timezone is determinable. For
///        example, "-0430" means 4 hours 30 minutes behind UTC (west of
///        Greenwich). [CX] If tm_isdst is zero, the standard time offset is
///        used. If tm_isdst is greater than zero, the daylight savings time
///        offset is used. If tm_isdst is negative, no characters are returned
///   %Z : timezone name or abbreviation, or no characters if no timezone
///        information exists
///   %% : A literal '%' character.
///  Example:
///    (format-universal-time "%Y-%m-%d %H:%M:%S") => "2017-02-14 15:28:02"
/// endd
func format_universal_time(arglist lob.Object, nargs int) lob.Object {
	format := stringArg(lob.Pop(&arglist),
		"format-universal-time: format string")
	var utime float64
	if nargs > 1 {
		utime = numberArg(arglist.Car(),
			"format-universal-time: format universal-time")
	} else {
		utime = time2universal_ns(time.Now())
	}
	return lob.NewString(olu.Strftime(format, universal2time_ns(utime)))
}

// EOF
