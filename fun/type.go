// The Function object type
package fun

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"

	"../dep"
	"../lob"
	"../msg"
)

var lambdaCounter = 0
var builtinCounter = 0

func NLambdas() int {
	return lambdaCounter
}
func NBuiltins() int {
	return builtinCounter
}

type funcType uint8

const (
	BUILTIN funcType = 0 + iota
	LAMBDA
	MACRO
)

var ftypeS = [...]string{
	"builtin",
	"lambda",
	"macro",
}
var deftypeS = [...]string{
	"builtin", // unused, actually
	"defun",
	"defmacro",
}

type Function struct {
	builtin Builtin   // for builtins only, obviously
	fi      *FormInfo // nil for a Builtin
	minargs int
	maxargs int
	doc     string
	name    string // function/macro name
	ftype   funcType
	special bool // for macros (always special) and builtins
	trace   bool
}

// lambda list information; see Ordinary Lambda Lists in
// http://www.lispworks.com/documentation/lw50/CLHS/Body/03_da.htm
//
type LLinfo struct {
	ordParams    []lob.Object // ordinary parameters, symbols slice
	optParams    []lob.Object // &optional parameters
	optInitforms []lob.Object // corresponding init forms
	restParam    lob.Object   // &rest parameters
	keywParams   lob.VarMap   // &key parameters => init forms
}

type FormInfo struct {
	LLinfo
	bodyForms lob.Object // list of lambda/macro bodyforms
	env       *lob.Environment
}

// used in the builtin registration mechanism, elements of []allBuiltins
type builtinDescriptor struct {
	name      string
	function  Builtin
	minargs   int
	maxargs   int
	isspecial bool
	doc       string
	source    string
}

type Builtin func(arglist lob.Object, nargs int) lob.Object

func (fun *Function) Type() lob.Obtype {
	return lob.FUNCTION
}

func (fun *Function) Equal(ob lob.Object) bool {
	return fun.Eq(ob)
}

func (fun *Function) Eq(ob lob.Object) bool {
	return fun == ob
}

func (fun *Function) Ftype() string {
	if fun.special {
		return "special"
	}
	return ftypeS[fun.ftype]
}

func (fun *Function) Synopsis() string {
	switch fun.ftype {
	case BUILTIN:
		nlpos := strings.IndexRune(fun.doc, '\n')
		if nlpos < 0 {
			nlpos = len(fun.doc)
		}
		return "(" + fun.name + " " + fun.doc[0:nlpos]
	case LAMBDA, MACRO:
		// prepend function name to lambda args
		wbuf := bytes.NewBufferString("(")
		wbuf.WriteString(fun.name)
		wbuf.WriteRune(' ')
		wbuf.WriteString(fun.ArgsString()[1:])
		return wbuf.String()
	}
	return "unknown function type"
}

func (fun *Function) Deftype() string {
	tDesc := ftypeS[fun.ftype]
	if IsMacro(fun) {
		return tDesc
	} else if fun.IsSpecial() {
		return tDesc + " special form"
	} else {
		return tDesc + " function"
	}
}

func (fun *Function) Docstring() string {
	switch fun.ftype {
	case LAMBDA, MACRO:
		return fun.doc
	case BUILTIN:
		nlpos := strings.IndexRune(fun.doc, '\n')
		if nlpos < 0 {
			return fun.doc
		}
		if nlpos < len(fun.doc) {
			return fun.doc[nlpos+1:]
		}
	}
	return ""
}

func (fun *Function) ArgsString() string {
	arglist := fun.Arglist()
	if arglist.IsNil() {
		return "()"
	}
	return arglist.ReaderString(nil)
}

func (fun *Function) ReaderString(pmap lob.Obset) string {
	if pmap == nil {
		pmap = make(lob.Obset)
	} else {
		if pmap[fun] {
			return "[...]"
		}
	}
	pmap[fun] = true
	defer func() { pmap[fun] = false }()

	switch fun.ftype {
	case BUILTIN:
		ftype := fun.Ftype()
		docs := strings.Split(fun.doc, "\n")
		argsstring := docs[0]
		return fmt.Sprintf("#<%s %s (%s %s>",
			ftype, fun.name, argsstring,
			strconv.Quote(strings.Join(docs[1:], "\n")))
	case LAMBDA, MACRO:
		wbuf := bytes.NewBufferString("#<")
		if fun.ftype == MACRO {
			wbuf.WriteString("macro ")
		} else {
			wbuf.WriteString("function ")
		}
		if fun.name != "" {
			wbuf.WriteString(fun.name)
			wbuf.WriteRune(' ')
		}
		wbuf.WriteString(fun.ArgsString())
		if fun.doc != "" {
			wbuf.WriteRune(' ')
			wbuf.WriteString(strconv.Quote(fun.doc))
		}
		var bodystring string
		if fun.fi.bodyForms.IsNil() {
			bodystring = "()"
		} else {
			wbuf.WriteRune(' ')
			bodystring = fun.fi.bodyForms.ReaderString(nil)
		}
		wbuf.WriteString(bodystring[1 : len(bodystring)-1])
		wbuf.WriteString(">")
		return wbuf.String()
	}
	return "" // not reached
}

func (fun *Function) ValueString(pmap lob.Obset) string {
	return fun.ReaderString(pmap)
}
func (fun *Function) String() string {
	return fun.ValueString(nil)
}

// Get the function definition of a lambda function. Bail on builtins and
// macros.
func (fun *Function) Definition() lob.Object {
	if fun.ftype != LAMBDA && fun.ftype != MACRO {
		msg.Bail("not a lambda function or macro:", fun)
	}
	docs := lob.NewString(fun.doc)
	if fun.name == "" {
		return lob.Cons(lob.Lambda,
			lob.Cons(fun.Arglist(),
				lob.Cons(docs, fun.Body())))
	}
	return lob.Cons(lob.Intern(deftypeS[fun.ftype]),
		lob.Cons(lob.Intern(fun.name),
			lob.Cons(fun.Arglist(),
				lob.Cons(docs, fun.Body()))))
}

// Get the body forms of a lambda function.
func (fun *Function) Body() lob.Object {
	if fun.ftype != LAMBDA && fun.ftype != MACRO {
		msg.Bail("not a lambda function or macro:", fun)
	}
	return fun.fi.bodyForms
}

// Get the argument list of a lambda function.
func (fun *Function) Arglist() lob.Object {
	if fun.ftype != LAMBDA && fun.ftype != MACRO {
		msg.Bail("not a lambda function:", fun)
	}
	argl := lob.NewListPusher()
	for _, arg := range fun.fi.ordParams {
		argl.Push(arg)
	}
	hadOptional := false
	for i, arg := range fun.fi.optParams {
		if !hadOptional {
			argl.Push(lob.AndOptional)
			hadOptional = true
		}
		if !fun.fi.optInitforms[i].IsNil() {
			argl.Push(lob.ListOf(arg, fun.fi.optInitforms[i]))
		} else {
			argl.Push(arg)
		}
	}
	if fun.fi.restParam != nil {
		argl.Push(lob.AndRest)
		argl.Push(fun.fi.restParam)
	}
	// need to have the keys sorted for reproducible results; unfortunately
	// this is quite tedious in Go
	keys := sortedKeys(fun.fi.keywParams)
	hadKey := false
	for _, keyw := range keys {
		if !hadKey {
			argl.Push(lob.AndKey)
			hadKey = true
		}
		keyName := keyw.Keyword2Param()
		value := fun.fi.keywParams[keyw]
		if value == lob.Nil {
			argl.Push(keyName)
		} else {
			argl.Push(lob.ListOf(keyName, value))
		}
	}
	return argl.List()
}

func (fun *Function) IsNil() bool {
	return false
}

func (ob *Function) Cxr() (lob.Object, lob.Object) {
	msg.Bail("Cxr on non-pair:", ob)
	return nil, nil
}

func (ob *Function) Car() lob.Object {
	msg.Bail("Car on non-pair:", ob)
	return nil
}

func (ob *Function) Cdr() lob.Object {
	msg.Bail("Cdr on non-pair:", ob)
	return nil
}

func (ob *Function) Rplacd(_ lob.Object) {
	msg.Bail("Cdr on non-pair:", ob)
}

func (fun *Function) SetTrace(doit bool) {
	fun.trace = doit
}

func (fun *Function) GetTrace() bool {
	return fun.trace
}

func (fun *Function) IsSpecial() bool {
	//msg.Trace(fun, "special?", fun.special)
	return fun.special
}

func IsMacro(ob lob.Object) bool {
	return lob.IsFunction(ob) && ob.(*Function).ftype == MACRO
}

func fname(fun *Function) string {
	if fun.name == "" {
		// as there are no anonymous macros or builtins, we can skip
		// looking up the function type string
		fun.name = "(lambda " + fun.ArgsString() + " " +
			fun.fi.bodyForms.ValueString(nil)[1:] + ":"
	}
	return fun.name
}

type llState uint8

const (
	inOrdParams llState = 0 + iota
	inOptional
	inRest
	afterRest
	inKeywords
)

// parse a pair that is expected to be a (var-symbol init-form) list and
// return both objects, the default value evaluated
//
func splitVarDefList(vdl lob.Object) (varsym *lob.Symbol, initform lob.Object) {
	variable := lob.Pop(&vdl)
	if !lob.IsSymbol(variable) || variable.IsNil() {
		msg.Bail("variable symbol expected in default value clause,"+
			" found:", variable)
	}
	varsym = variable.(*lob.Symbol)
	if vdl.IsNil() {
		msg.Bail("default value clause too short")
	}
	initform = lob.Pop(&vdl)
	if !vdl.IsNil() {
		msg.Bail("default value clause too long")
	}
	return varsym, initform
}

// parse a lambda list with ordinary parameters, &optional, &rest, and &key
// parameters.
//
func parseLambdaList(ll lob.Object) (fi *FormInfo, minargs, maxargs int) {
	origll := ll
	fi = &FormInfo{}
	state := inOrdParams
	for lob.IsPair(ll) {
		nextt := lob.Pop(&ll)
		switch state {
		case inOrdParams:
			switch nextt {
			case lob.AndOptional:
				state = inOptional
			case lob.AndRest:
				state = inRest
			case lob.AndKey:
				state = inKeywords
				fi.keywParams = lob.VarMap{}
			default:
				if lob.IsSymbol(nextt) {
					fi.ordParams =
						append(fi.ordParams, nextt)
					minargs++
					maxargs++
				} else {
					msg.Bailf("unexpected token ``%s'' in"+
						" lambda list: %s", origll)
				}
			}
		case inOptional:
			switch nextt {
			case lob.AndRest:
				state = inRest
			case lob.AndKey:
				state = inKeywords
				fi.keywParams = lob.VarMap{}
			default:
				if lob.IsSymbol(nextt) {
					fi.optParams =
						append(fi.optParams, nextt)
					fi.optInitforms =
						append(fi.optInitforms, lob.Nil)
				} else if lob.IsPair(nextt) {
					varsym, initform :=
						splitVarDefList(nextt)
					fi.optParams =
						append(fi.optParams, varsym)
					fi.optInitforms =
						append(fi.optInitforms, initform)
				} else {
					msg.Bailf("unexpected token ``%s''"+
						" after %s in lambda list: %s",
						nextt, lob.AndOptional, origll)
				}
				maxargs++
			}
		case inRest:
			if lob.IsSymbol(nextt) {
				fi.restParam = nextt
				state = afterRest
				maxargs = -1
			} else {
				msg.Bailf("unexpected token ``%s'' after %s in"+
					" lambda list: %s",
					nextt, lob.AndRest, origll)
			}
		case afterRest:
			switch nextt {
			case lob.AndKey:
				state = inKeywords
				fi.keywParams = lob.VarMap{}
			default:
				msg.Bailf("unexpected token ``%s'' after %s"+
					"variable in lambda list: %s",
					nextt, lob.AndRest, origll)
			}
		case inKeywords:
			switch keyw := nextt.(type) {
			case *lob.Symbol:
				fi.keywParams[keyw.Param2Keyword()] = lob.Nil
			case *lob.Pair:
				varsym, initform := splitVarDefList(nextt)
				fi.keywParams[varsym.Param2Keyword()] =
					initform
			default:
				msg.Bailf("unexpected token ``%s'' after %s"+
					"variable in lambda list: %s",
					nextt, lob.AndKey, origll)
			}
			maxargs = -1
		}
	}
	if !ll.IsNil() {
		msg.Bail("lambda list is not a proper list:", origll)
	}
	return fi, minargs, maxargs
}

func makeForm(name string, arglAndBody lob.Object, ftype funcType) *Function {
	formInfo, minargs, maxargs := parseLambdaList(arglAndBody.Car())
	bodyForms := arglAndBody.Cdr()
	maybeDoc, otherForms := bodyForms.Cxr()
	docString := ""
	if lob.IsString(maybeDoc) && lob.IsPair(otherForms) {
		docString = maybeDoc.ValueString(nil)
		bodyForms = otherForms
	}
	formInfo.bodyForms = MacroExpandForm(bodyForms)
	formInfo.env = lob.Env
	return &Function{
		builtin: nil, fi: formInfo, minargs: minargs, maxargs: maxargs,
		doc: docString, name: name, ftype: ftype,
		special: ftype == MACRO, trace: false,
	}
}

func Init_funtype() {
	//fmt.Println("fun/type.init")
	dep.NLambdas = NLambdas
	dep.NBuiltins = NBuiltins
	dep.IsBuiltin = IsBuiltin
	lob.MacroExpandForm = MacroExpandForm

	for _, bd := range allBuiltins {
		builtinCounter++
		sym := lob.Intern(bd.name)
		f := &Function{
			builtin: bd.function, fi: nil, minargs: bd.minargs,
			maxargs: bd.maxargs, doc: strings.TrimSpace(bd.doc),
			name: bd.name, ftype: BUILTIN, special: bd.isspecial,
			trace: false,
		}
		sym.SetFunction(f)
		sym.PutProp(lob.FuncDefinedIn, lob.NewString(bd.source))
	}
}

// EOF
