// Macro-related builtins
// unquote and unquote-splicing are not really functions at all, but they are
// defined here as such so they can throw an error when called outside of a
// quasiquote context.
package fun

import (
	"../lob"
	"../msg"
)

/// name macroexpand
/// impl macroexpand
/// mina 1
/// maxa 1
/// spec false
/// args FORM
/// retv expanded-form
/// docs
///   Expand FORM as a macro with the current bindings.
/// endd
func macroexpand(arglist lob.Object, nargs int) lob.Object {
	return lob.MacroExpandForm(arglist.Car())
}

/// name unquote
/// impl unquote
/// mina 1
/// maxa 1
/// spec true
/// args FORM
/// retv none
/// docs
///   Throw an error if called as a function outside of a quasiquote.
/// endd

/// name unquote-splicing
/// impl unquote
/// mina 1
/// maxa 1
/// spec true
/// args FORM
/// retv none
/// docs
///   Throw an error if called as a function outside of a quasiquote.
/// endd
func unquote(arglist lob.Object, nargs int) lob.Object {
	msg.Bail("unquote outside of a quasiquote context")
	return nil
}
