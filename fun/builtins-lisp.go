// Lisp-typical builtins
// I am well aware that this is not a very clear distinction.
package fun

import (
	"../call"
	"../hlp"
	"../lob"
	"../msg"
)

/// name intern
/// impl intern
/// mina 1
/// maxa 1
/// spec false
/// args STRING-OR-SYMBOL
/// retv symbol
/// docs
///   Return the (potentially new) interned symbol whose name is STRING.
/// endd
func intern(arglist lob.Object, nargs int) lob.Object {
	return lob.Intern(stringArg(arglist.Car(), "intern:"))
}

/// name gensym
/// impl gensym
/// mina 0
/// maxa 1
/// spec false
/// args &optional PREFIX
/// retv symbol
/// docs
///   Return a new, uninterned and otherwise unused symbol.
///   The symbol is not bound or fbound and has an empty property list.
///   If optional PREFIX is non-nil, it is tried as the name for the
///   symbol or, if the name is already in use, as the stem of the name.
/// endd
func gensym(arglist lob.Object, nargs int) lob.Object {
	stem := ""
	if nargs > 0 {
		stem = arglist.Car().ValueString(nil)
	}
	return lob.Gensym(stem)
}

/// name gentemp
/// impl gentemp
/// mina 0
/// maxa 1
/// spec false
/// args &optional PREFIX
/// retv new-symbol
/// docs
///   Create and return a new interned symbol.
///
///   The symbol is guaranteed to be one that did not previously exist.
///   It is neither bound nor fbound, and has a null property list.
///
///   The name of the new-symbol is the concatenation of the prefix and
///   a suffix, which is taken from an internal counter used only by
///   gentemp. If a symbol by that name already exists, the counter is
///   incremented as many times as is necessary to produce a name that
///   is not already the name of an existing symbol.
/// endd
func gentemp(arglist lob.Object, nargs int) lob.Object {
	prefix := ""
	if nargs > 0 {
		prefix = stringArg(arglist.Car(), "gentemp:")
	}
	return lob.Gentemp(prefix)
}

/// name make-symbol
/// impl make_symbol
/// mina 1
/// maxa 1
/// spec false
/// args STRING
/// retv symbol
/// docs
///   Return a new uninterned symbol whose name is STRING.
/// endd
func make_symbol(arglist lob.Object, nargs int) lob.Object {
	return lob.NewSymbol(stringArg(arglist.Car(), "make-symbol:"), false)
}

/// name eval
/// impl eeval
/// mina 1
/// maxa 2
/// spec false
/// args EXPR &optional ENVIRONMENT
/// retv an object
/// docs
///   Evaluate the expression EXPR in optional ENVIRONMENT and return the value.
///   ENVIRONMENT may be nil, in which case the current environment is used.
/// endd
func eeval(arglist lob.Object, nargs int) lob.Object {
	expr, arglist := arglist.Cxr()
	var env *lob.Environment = nil
	if !arglist.IsNil() {
		arg := arglist.Car()
		if !arg.IsNil() {
			env = envArg(arg, "second eval")
		}
	}
	return lob.Eval(expr, env)
}

/// name boundp
/// impl boundp
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv bool
/// docs
///   Return t if a value is bound to SYMBOL, nil otherwise.
/// endd
func boundp(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "boundp: symbol")
	return lob.Bool2Ob(sym.Value() != nil)
}

/// name fboundp
/// impl fboundp
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv bool
/// docs
///   Return t if a function is bound to SYMBOL, nil otherwise.
/// endd
func fboundp(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "fboundp: symbol")
	return lob.Bool2Ob(sym.Function() != nil)
}

/// name defparameter
/// impl defparameter
/// mina 2
/// maxa 3
/// spec true
/// args SYMBOL INITIAL-VALUE &optional DOCSTRING
/// retv SYMBOL
/// docs
///   Define variable SYMBOL with optional INITIAL-VALUE and DOCSTRING.
///   If the variable is already bound, its value is changed nonetheless.
/// endd
func defparameter(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "defparameter: name")
	sym.DefVar(lob.Nil)
	if nargs > 1 && arglist.Cdr().Car() != nil {
		sym.SetValue(lob.EvalCurEnv(arglist.Cdr().Car()))
		if nargs > 2 {
			sym.PutProp(lob.DocString, arglist.Cdr().Cdr().Car())
		}
	}
	sym.PutProp(lob.VarDefinedIn, lob.CurrentLoadFile.Value())
	return sym
}

/// name defvar
/// impl defvar
/// mina 1
/// maxa 3
/// spec true
/// args SYMBOL &optional INITIAL-VALUE DOCSTRING
/// retv SYMBOL
/// docs
///   Define variable SYMBOL with optional INITIAL-VALUE and DOCSTRING.
///   If the variable is already bound, its value is not changed.
/// endd
func defvar(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "defvar: name")
	valueForm := lob.Object(lob.Nil)
	if nargs > 1 && arglist.Cdr().Car() != nil {
		valueForm = arglist.Cdr().Car()
	}
	if sym.Value() == nil {
		sym.DefVar(lob.EvalCurEnv(valueForm))
		if nargs > 2 {
			sym.PutProp(lob.DocString, arglist.Cdr().Cdr().Car())
		}
		sym.PutProp(lob.VarDefinedIn, lob.CurrentLoadFile.Value())
	}
	return sym
}

/// name symbol-value
/// impl symbolValue
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv value
/// docs
///   Return the value bound to SYMBOL in the current environment.
/// endd
func symbolValue(arglist lob.Object, nargs int) lob.Object {
	var result lob.Object = lob.Nil
	value := symbolArg(arglist.Car(), "symbol-function:").NeedValue()
	if value != nil {
		result = value
	}
	return result
}

/// name symbol-function
/// impl symbolFunction
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv function
/// docs
///   Return the function bound to SYMBOL.
/// endd
func symbolFunction(arglist lob.Object, nargs int) lob.Object {
	return symbolArg(arglist.Car(), "symbol-function:").NeedFunction()
}

/// name ignore
/// impl ignore
/// mina 0
/// maxa -1
/// spec false
/// args &rest ARGS
/// retv nil
/// docs
///   Return nil.
/// endd
func ignore(arglist lob.Object, nargs int) lob.Object {
	return lob.Nil
}

/// name identity
/// impl identity
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv arg
/// docs
///   Return ARG unchanged.
/// endd
func identity(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car()
}

/// name type-of
/// impl type_of
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv symbol
/// docs
///   Return a symbol with the type name of ARG.
/// endd
func type_of(arglist lob.Object, nargs int) lob.Object {
	return lob.Intern(arglist.Car().Type().Name())
}

/// name symbol-name
/// impl symbolName
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv string
/// docs
///   Return the name of SYMBOL as a string.
/// endd
//
/// name symbol-string
/// impl symbolName
/// mina 1
/// maxa 1
/// spec false
/// args SYMBOL
/// retv string
/// docs
///   Return the name of SYMBOL as a string.
/// endd
func symbolName(arglist lob.Object, nargs int) lob.Object {
	return lob.NewString(
		symbolArg(arglist.Car(), "symbol-name:").ValueString(nil))
}

/// name putprop
/// impl putprop
/// mina 3
/// maxa 3
/// spec false
/// args SYMBOL VALUE PROPERTY
/// retv value
/// docs
///   Give SYMBOL a PROPERTY of VALUE and return VALUE.
/// endd
func putprop(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "putprop: first")
	prop := symbolArg(arglist.Cdr().Cdr().Car(), "putprop: property")
	return sym.PutProp(prop, arglist.Cdr().Car())
}

/// name propput
/// impl propput
/// mina 3
/// maxa 3
/// spec false
/// args SYMBOL PROPERTY VALUE
/// retv value
/// docs
///   Give SYMBOL a PROPERTY of VALUE and return VALUE.
///   A putprop by another name and another argument order,
///   for the benefit of setf.
/// endd
func propput(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "putprop: first")
	prop := symbolArg(arglist.Cdr().Car(), "putprop: property")
	return sym.PutProp(prop, arglist.Cdr().Cdr().Car())
}

/// name get
/// impl get
/// mina 2
/// maxa 2
/// spec false
/// args SYMBOL PROPERTY
/// retv property value
/// docs
///   Return the value of SYMBOL's property PROPERTY (or nil, if it is unset).
/// endd
func get(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "get: first")
	prop := symbolArg(arglist.Cdr().Car(), "get: property")
	return sym.GetProp(prop)
}

/// name listp
/// impl listp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a list (nil or a PAIR), nil else.
/// endd
func listp(arglist lob.Object, nargs int) lob.Object {
	if lob.IsList(arglist.Car()) {
		return lob.T
	}
	return lob.Nil
}

/// name symbolp
/// impl symbolp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a symbol, nil else.
/// endd
func symbolp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsSymbol(arglist.Car()))
}

/// name consp
/// impl consp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a pair, nil else.
/// endd
func consp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsPair(arglist.Car()))
}

/// name functionp
/// impl functionp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a function, nil else.
/// endd
func functionp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsFunction(arglist.Car()))
}

/// name macrop
/// impl macrop
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a macro, nil else.
/// endd
func macrop(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(IsMacro(arglist.Car()))
}

/// name numberp
/// impl numberp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a number, nil else.
/// endd
func numberp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsNumber(arglist.Car()))
}

/// name environmentp
/// impl environmentp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is an environment, nil else.
/// endd
func environmentp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsEnvironment(arglist.Car()))
}

/// name structp
/// impl structp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a struct, nil else.
/// endd
func structp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsStruct(arglist.Car()))
}

/// name stringp
/// impl stringp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a string, nil else.
/// endd
func stringp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsString(arglist.Car()))
}

/// name charp
/// impl charp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a char, nil else.
/// endd
func charp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsChar(arglist.Car()))
}

/// name tablep
/// impl tablep
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a table, nil else.
/// endd
func tablep(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsTable(arglist.Car()))
}

/// name vectorp
/// impl vectorp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a vector, nil else.
/// endd
func vectorp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsVector(arglist.Car()))
}

/// name portp
/// impl portp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a port, nil else.
/// endd
func portp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsPort(arglist.Car()))
}

/// name funcall
/// impl funcall
/// mina 1
/// maxa -1
/// spec false
/// args FUNCTION ARGS...
/// retv value
/// docs
///   Apply FUNCTION to arguments ARGS and return the result value.
/// endd
func funcall(arglist lob.Object, nargs int) lob.Object {
	fun, arglist := arglist.Cxr()
	return call.EvalCallable(fun, false).Call(nargs-1, arglist)
}

func spreadArglist(arglist lob.Object) lob.Object {
	var last, lastBut2nd lob.Object
	if arglist.IsNil() {
		return arglist
	}
	next := arglist
	for lob.IsPair(next) {
		lastBut2nd = last
		last = next
		next = next.Cdr()
	}
	tail := last.Car()
	if lastBut2nd != nil {
		lastBut2nd.Rplacd(tail)
	} else {
		arglist = tail
	}
	return arglist
}

/// name apply
/// impl apply
/// mina 2
/// maxa -1
/// spec false
/// args FUNCTION ARGS+
/// retv value
/// docs
///   Apply FUNCTION to (spreadable) arguments ARGS and return the result value.
/// endd
func apply(arglist lob.Object, nargs int) lob.Object {
	// arguments come in the form (arg1 ... (argn ... argm)) and must be
	// spread
	function, arguments := arglist.Cxr()
	arguments = spreadArglist(arguments)
	proper, nargs := hlp.IsProperList(arguments)
	if !proper {
		msg.Bail("apply: improper argument list:", arguments)
	}
	return call.EvalCallable(function, false).Call(nargs, arguments)
}

/// name or
/// impl or
/// mina 0
/// maxa -1
/// spec true
/// args &rest ARGS
/// retv value
/// docs
///   Evaluate ARGS until one is non-nil; return the last evaluated value.
/// endd
func or(arglist lob.Object, nargs int) lob.Object {
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		value := lob.EvalCurEnv(arg)
		if !value.IsNil() {
			return value
		}
	}
	return lob.Nil
}

/// name and
/// impl and
/// mina 0
/// maxa -1
/// spec true
/// args &rest ARGS
/// retv value
/// docs
///   Evaluate ARGS until one is nil; return the last evaluated value.
/// endd
func and(arglist lob.Object, nargs int) lob.Object {
	var result lob.Object
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		value := lob.EvalCurEnv(arg)
		if value.IsNil() {
			return lob.Nil
		}
		result = value
	}
	return result
}

/// name eq
/// impl eq
/// mina 2
/// maxa 2
/// spec false
/// args ARG1 ARG2
/// retv t/nil
/// docs
///   Return t if arguments have the same atomic value or are the same object.
///   Strings are atomic, and equal strings are also eq.
/// endd
func eq(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(arglist.Car().Eq(arglist.Cdr().Car()))
}

/// name equal
/// impl equal
/// mina 2
/// maxa 2
/// spec false
/// args ARG1 ARG2
/// retv t/nil
/// docs
///   Return t if the arguments are the same or have the same contents.
/// endd
func equal(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(arglist.Car().Equal(arglist.Cdr().Car()))
}

/// name atom
/// impl atom
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is atomic (i.e. symbol, number, string, char), nil else.
/// endd
func atom(arglist lob.Object, nargs int) lob.Object {
	if lob.IsAtom(arglist.Car()) {
		return lob.T
	}
	return lob.Nil
}

/// name fset
/// impl fset
/// mina 2
/// maxa 2
/// spec false
/// args SYMBOL NEW-FUNC
/// retv new-func
/// docs
///   Set function cell of SYMBOL to NEW-FUNC (a function) and return NEW-FUNC.
/// endd
func fset(arglist lob.Object, nargs int) lob.Object {
	sym := symbolArg(arglist.Car(), "fset: first")
	fun := callableArg(arglist.Cdr().Car(), "fset")
	sym.SetFunction(fun)
	return fun
}

/// name rplacd
/// impl rplacd
/// mina 2
/// maxa 2
/// spec false
/// args PAIR NEW-CDR
/// retv pair
/// docs
///   Replace the cdr of PAIR with NEW-CDR and return PAIR.
/// endd
func rplacd(arglist lob.Object, nargs int) lob.Object {
	pair := pairArg(arglist.Car(), "rplacd: first")
	pair.Rplacd(arglist.Cdr().Car())
	return pair
}

/// name rplaca
/// impl rplaca
/// mina 2
/// maxa 2
/// spec false
/// args PAIR NEW-CAR
/// retv pair
/// docs
///   Replace the car of PAIR with NEW-CAR and return PAIR.
/// endd
func rplaca(arglist lob.Object, nargs int) lob.Object {
	pair := pairArg(arglist.Car(), "rplaca: first")
	pair.Rplaca(arglist.Cdr().Car())
	return pair
}

/// name rplacd-ret-value
/// impl rplacd_ret_value
/// mina 2
/// maxa 2
/// spec false
/// args PAIR NEW-CDR
/// retv new-cdr
/// docs
///   Replace the cdr of PAIR with NEW-CDR and return NEW-CDR.
///   Intended for use by setf.
/// endd
func rplacd_ret_value(arglist lob.Object, nargs int) lob.Object {
	pair := pairArg(arglist.Car(), "rplacd: first")
	value := arglist.Cdr().Car()
	pair.Rplacd(value)
	return value
}

/// name rplaca-ret-value
/// impl rplaca_ret_value
/// mina 2
/// maxa 2
/// spec false
/// args PAIR NEW-CAR
/// retv new-car
/// docs
///   Replace the car of PAIR with NEW-CAR and return NEW-CAR.
///   Intended for use by setf.
/// endd
func rplaca_ret_value(arglist lob.Object, nargs int) lob.Object {
	pair := pairArg(arglist.Car(), "rplaca: first")
	value := arglist.Cdr().Car()
	pair.Rplaca(value)
	return value
}

/// name function
/// impl function
/// mina 1
/// maxa 1
/// spec true
/// args ARG
/// retv function
/// docs
///   Return the function value of a symbol, or the argument if it is a function.
/// endd
func function(arglist lob.Object, nargs int) lob.Object {
	switch funob := arglist.Car().(type) {
	default:
		msg.Bailf("function: argument is not a function or "+
			"function symbol: %s", funob)
	case *lob.Pair:
		funsym := funob.Car()
		if funsym == lob.Lambda || funsym == lob.Λ {
			return lambda(funob.Cdr(),
				funob.Cdr().(lob.List).Length())
		}
	case *lob.Symbol:
		return funob.NeedFunction()
	case *Function:
		return funob
	}
	return lob.Nil
}

/// name cons
/// impl cons
/// mina 2
/// maxa 2
/// spec false
/// args CAR CDR
/// retv pair
/// docs
///   Return a new pair consisting of CAR and CDR.
/// endd
func cons(arglist lob.Object, nargs int) lob.Object {
	return lob.Cons(arglist.Car(), arglist.Cdr().Car())
}

/// name null
/// impl null
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is nil, nil else.
/// endd
func null(arglist lob.Object, nargs int) lob.Object {
	if arglist.Car().IsNil() {
		return lob.T
	}
	return lob.Nil
}

func defineForm(arglist lob.Object, argmsg string, whatf funcType) lob.Object {
	car, cdr := arglist.Cxr()
	name := symbolArg(car, argmsg)
	name.SetFunction(makeForm(name.ValueString(nil), cdr, whatf))
	name.PutProp(lob.FuncDefinedIn, lob.CurrentLoadFile.Value())
	return name
}

/// name defmacro
/// impl defmacro
/// mina 1
/// maxa -1
/// spec true
/// args ARGS &rest BODY
/// retv macro
/// docs
///   Define a macro to be called with ARGS; BODY builds a form to be evaluated.
/// endd
func defmacro(arglist lob.Object, nargs int) lob.Object {
	return defineForm(arglist, "defmacro: name of macro", MACRO)
}

/// name defun
/// impl defun
/// mina 2
/// maxa -1
/// spec true
/// args "SYM (PARAMS...) [DOCSTRING] BODY ..."
/// retv symbol
/// docs
///   Define a function SYM with parameters PARAMS and forms BODY.
/// endd
func defun(arglist lob.Object, nargs int) lob.Object {
	return defineForm(arglist, "defun: name of function", LAMBDA)
}

/// name car
/// impl car
/// mina 1
/// maxa 2
/// spec false
/// args PAIR
/// retv value
/// docs <<EOD
///   Return the contents of the address part of the PAIR register.
///   The car of nil is nil.
/// endd
func car(arglist lob.Object, nargs int) lob.Object {
	if arglist.Car().IsNil() {
		return lob.Nil
	}
	return pairArg(arglist.Car(), "car:").Car()
}

/// name cdr
/// impl cdr
/// mina 1
/// maxa 1
/// spec false
/// args PAIR
/// retv value
/// docs
///   Return the contents of the decrement part of the PAIR register.
///   The cdr of nil is nil.
/// endd
func cdr(arglist lob.Object, nargs int) lob.Object {
	if arglist.Car().IsNil() {
		return lob.Nil
	}
	return pairArg(arglist.Car(), "cdr:").Cdr()
}

/// name caar
/// impl caar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caar of LIST.
/// endd
func caar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car()
}

/// name cadr
/// impl cadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cadr of LIST.
/// endd
func cadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car()
}

/// name cdar
/// impl cdar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdar of LIST.
/// endd
func cdar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr()
}

/// name cddr
/// impl cddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cddr of LIST.
/// endd
func cddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr()
}

/// name caaar
/// impl caaar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caaar of LIST.
/// endd
func caaar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car().Car()
}

/// name caadr
/// impl caadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caadr of LIST.
/// endd
func caadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car().Car()
}

/// name cadar
/// impl cadar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cadar of LIST.
/// endd
func cadar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr().Car()
}

/// name caddr
/// impl caddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caddr of LIST.
/// endd
func caddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr().Car()
}

/// name cdaar
/// impl cdaar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdaar of LIST.
/// endd
func cdaar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car().Cdr()
}

/// name cdadr
/// impl cdadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdadr of LIST.
/// endd
func cdadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car().Cdr()
}

/// name cddar
/// impl cddar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cddar of LIST.
/// endd
func cddar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr().Cdr()
}

/// name cdddr
/// impl cdddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdddr of LIST.
/// endd
func cdddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr().Cdr()
}

/// name caaaar
/// impl caaaar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caaaar of LIST.
/// endd
func caaaar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car().Car().Car()
}

/// name caaadr
/// impl caaadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caaadr of LIST.
/// endd
func caaadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car().Car().Car()
}

/// name caadar
/// impl caadar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caadar of LIST.
/// endd
func caadar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr().Car().Car()
}

/// name caaddr
/// impl caaddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caaddr of LIST.
/// endd
func caaddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr().Car().Car()
}

/// name cadaar
/// impl cadaar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cadaar of LIST.
/// endd
func cadaar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car().Cdr().Car()
}

/// name cadadr
/// impl cadadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cadadr of LIST.
/// endd
func cadadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car().Cdr().Car()
}

/// name caddar
/// impl caddar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the caddar of LIST.
/// endd
func caddar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr().Cdr().Car()
}

/// name cadddr
/// impl cadddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cadddr of LIST.
/// endd
func cadddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr().Cdr().Car()
}

/// name cdaaar
/// impl cdaaar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdaaar of LIST.
/// endd
func cdaaar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car().Car().Cdr()
}

/// name cdaadr
/// impl cdaadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdaadr of LIST.
/// endd
func cdaadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car().Car().Cdr()
}

/// name cdadar
/// impl cdadar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdadar of LIST.
/// endd
func cdadar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr().Car().Cdr()
}

/// name cdaddr
/// impl cdaddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdaddr of LIST.
/// endd
func cdaddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr().Car().Cdr()
}

/// name cddaar
/// impl cddaar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cddaar of LIST.
/// endd
func cddaar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Car().Cdr().Cdr()
}

/// name cddadr
/// impl cddadr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cddadr of LIST.
/// endd
func cddadr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Car().Cdr().Cdr()
}

/// name cdddar
/// impl cdddar
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cdddar of LIST.
/// endd
func cdddar(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Car().Cdr().Cdr().Cdr()
}

/// name cddddr
/// impl cddddr
/// mina 1
/// maxa 1
/// spec false
/// args LIST
/// retv value
/// docs
///   Return the cddddr of LIST.
/// endd
func cddddr(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car().Cdr().Cdr().Cdr().Cdr()
}

/// name quote
/// impl quote
/// mina 1
/// maxa 1
/// spec true
/// args ARG
/// retv arg
/// docs
///   Return the argument, without evaluating it.
/// endd
func quote(arglist lob.Object, nargs int) lob.Object {
	return arglist.Car()
}

/// name lambda
/// impl lambda
/// mina 1
/// maxa -1
/// spec true
/// args (PARAMS...) [DOCSTRING] BODY...
/// retv function
/// docs
///   Return a function object with parameters PARAMS and forms BODY.
/// endd
/// name λ
/// impl lambda
/// mina 1
/// maxa -1
/// spec true
/// args (PARAMS...) [DOCSTRING] BODY...
/// retv function
/// docs
///   Return a function object with parameters PARAMS and forms BODY.
/// endd
func lambda(arglist lob.Object, nargs int) lob.Object {
	return makeForm("", arglist, LAMBDA)
}

/// name setq
/// impl setq_symbol
/// mina 2
/// maxa 2
/// spec true
/// args SYM VALUE
/// retv value
/// docs
///   Set value of SYMBOL (not evaluated) to VALUE (evaluated) and return VALUE.
/// endd
func setq_symbol(arglist lob.Object, nargs int) lob.Object {
	symbolarg, arglist := arglist.Cxr()
	symbol := symbolArg(symbolarg, "setq: first")
	value := lob.EvalCurEnv(arglist.Car())
	symbol.SetValue(value)
	return value
}

/// name set
/// impl set_symbol
/// mina 2
/// maxa 2
/// spec false
/// args SYMBOL VALUE
/// retv value
/// docs
///   Set value of SYMBOL to VALUE and return VALUE.
///   Other than with setq, both SYMBOL and VALUE args are evaluated.
/// endd
func set_symbol(arglist lob.Object, nargs int) lob.Object {
	symbolarg, arglist := arglist.Cxr()
	symbol := symbolArg(symbolarg, "set: first")
	value := arglist.Car()
	symbol.SetValue(value)
	return value
}

// Core of quasiquote, the internal recursion.
func qq_recurse(form lob.Object) lob.Object {
	if !lob.IsPair(form) {
		return form
	}
	head, tail := form.Cxr()
	if head.Eq(lob.Unquote) {
		if tail.(*lob.Pair).Length() == 1 {
			return lob.EvalCurEnv(tail.Car())
		} else {
			msg.Bail("wrong # args to unquote:", head)
			return nil // not reached
		}
	}
	tailResult := qq_recurse(tail)
	if !lob.IsPair(head) {
		return lob.Cons(head, tailResult)
	}
	headhead := head.Car()
	if headhead.Eq(lob.UnquoteSplicing) {
		if head.(*lob.Pair).Length() == 2 {
			expandedArg := lob.EvalCurEnv(head.Cdr().Car())
			if !lob.IsList(expandedArg) {
				msg.Bail("unquote-splicing arg not a list:",
					expandedArg)
			}
			return hlp.Append(expandedArg, tailResult)
		} else {
			msg.Bail("wrong # args to unquote-splicing:", head)
			return nil // not reached
		}
	} else {
		return lob.Cons(qq_recurse(head), tailResult)
	}
}

/// name quasiquote
/// impl quasiquote
/// mina 1
/// maxa 1
/// spec true
/// args EXPR
/// retv expanded form
/// docs
///   In EXPR, replace unquoted items them with their values as appropriate.
///   Return the resulting form. Unquoted items are those preceded by an
///   »unquote« sign (»,«) or an »unquote-splicing« (»,@«). In the latter case,
///   if the value is a list, splice it into the surrounding list.
/// endd
func quasiquote(arglist lob.Object, nargs int) lob.Object {
	return qq_recurse(arglist.Car())
}

/// name keywordp
/// impl keywordp
/// mina 1
/// maxa 1
/// spec false
/// args expr
/// retv t/f
/// docs
///   Return t if the argument is a keyword symbol, nil otherwise.
///   A keyword is a symbol whose name starts with a colon, but is
///   longer than just the colon.
/// endd
func keywordp(arglist lob.Object, nargs int) lob.Object {
	switch arg := arglist.Car().(type) {
	case *lob.Symbol:
		return lob.Bool2Ob(arg.IsKeyword())
	default:
		return lob.Nil
	}
}

// EOF
