// Helper functions mostly for builtins
package fun

import (
	"sort"

	"../call"
	"../lob"
	"../msg"
)

var emptyString lob.Object

// All these *Arg(arg, whatarg) functions check the expected type of the
// argument and return it cast to that type if it conforms. If not, an error is
// thrown, using the whatarg description in the error message. In addition, a
// *ArgOr() function will return a default value if the argument is nil
// (NumberArgOr() only right now).

func envArg(arg lob.Object, whatarg string) *lob.Environment {
	if lob.IsEnvironment(arg) {
		return arg.(*lob.Environment)
	}
	msg.Bail(whatarg, "argument is not an environment:", arg)
	return nil
}

func comparableArg(arg lob.Object, whatarg string) lob.Comparable {
	if lob.IsComparable(arg) {
		return arg.(lob.Comparable)
	}
	msg.Bail(whatarg, "argument is not comparable:", arg)
	return lob.Nil
}

// need this in vector too, so defined in lob.object
func numberArg(arg lob.Object, whatarg string) float64 {
	return lob.NumberArg(arg, whatarg)
}

func numberArgOr(arg lob.Object, whatarg string, defaultval float64) float64 {
	if arg.IsNil() {
		return defaultval
	}
	if lob.IsNumber(arg) {
		return arg.(*lob.Number).Value()
	}
	msg.Bail(whatarg, "argument is not a number:", arg)
	return 0
}

func intArg(arg lob.Object, whatarg string) int {
	if lob.IsNumber(arg) {
		value := arg.(*lob.Number).Value()
		if float64(int(value)) != value {
			msg.Bail(whatarg, "argument is not an integer:", arg)
		}
		return int(value)
	}
	msg.Bail(whatarg, "argument is not an integer:", arg)
	return 0
}

func intArgOr(arg lob.Object, whatarg string, defaultval int) int {
	if arg.IsNil() {
		return defaultval
	}
	if lob.IsNumber(arg) {
		value := arg.(*lob.Number).Value()
		if float64(int(value)) != value {
			msg.Bail(whatarg, "argument is not an integer:", arg)
		}
		return int(value)
	}
	msg.Bail(whatarg, "argument is not an integer:", arg)
	return 0
}

func pairArg(arg lob.Object, whatarg string) *lob.Pair {
	if lob.IsPair(arg) {
		return arg.(*lob.Pair)
	}
	msg.Bail(whatarg, "argument is not a pair:", arg)
	return nil
}

func sequenceArg(arg lob.Object, whatarg string) lob.Sequence {
	if lob.IsSequence(arg) {
		return arg.(lob.Sequence)
	}
	msg.Bail(whatarg, "argument is not a sequence:", arg)
	return nil
}

func listArg(arg lob.Object, whatarg string) lob.List {
	if lob.IsList(arg) {
		return arg.(lob.List)
	}
	msg.Bail(whatarg, "argument is not a list:", arg)
	return nil
}

func portArg(arg lob.Object, whatarg string) *lob.Port {
	if lob.IsPort(arg) {
		return arg.(*lob.Port)
	}
	msg.Bail(whatarg, "argument is not a port:", arg)
	return nil
}

func stringArg(arg lob.Object, whatarg string) string {
	return lob.StringArg(arg, whatarg)
}

func symbolArg(arg lob.Object, whatarg string) *lob.Symbol {
	if lob.IsSymbol(arg) {
		return arg.(*lob.Symbol)
	}
	msg.Bail(whatarg, "argument is not a symbol:", arg)
	return nil
}

func tableArg(arg lob.Object, whatarg string) *lob.Table {
	if lob.IsTable(arg) {
		return arg.(*lob.Table)
	}
	msg.Bailf("%s argument is not a table: %s", whatarg, arg)
	return nil
}

func vectorArg(arg lob.Object, whatarg string) *lob.Vector {
	if lob.IsVector(arg) {
		return arg.(*lob.Vector)
	}
	msg.Bailf("%s argument is not a vector: %s", whatarg, arg)
	return nil
}

func callableArg(arg lob.Object, whatarg string) call.Callable {
	function := call.EvalCallable(arg, true)
	if function == nil {
		msg.Bailf("%s argument is not a callable: %s", whatarg, arg)
	}
	return function
}

func functionArg(arg lob.Object, whatarg string) *Function {
	function := callableArg(arg, whatarg)
	if !lob.IsFunction(function) {
		msg.Bailf("%s argument is not a function: %s", whatarg, arg)
	}
	return function.(*Function)
}

func charArg(arg lob.Object, whatarg string) rune {
	if lob.IsChar(arg) {
		return arg.(*lob.Char).Value()
	}
	msg.Bailf("%s argument is not a char: %s", whatarg, arg)
	return 0
}

func structArg(arg lob.Object, whatarg string) *lob.Struct {
	if lob.IsStruct(arg) {
		return arg.(*lob.Struct)
	}
	msg.Bailf("%s argument is not a struct: %s", whatarg, arg)
	return nil
}

// get the keyword values from arglist, as far as they are present; "allowed"
// contains the allowed keywords with default values,
//
func getKeywords(arglist lob.Object, allowed map[lob.Object]lob.Object) (
	result map[lob.Object]lob.Object) {

	result = allowed
	for lob.IsPair(arglist) {
		elem1 := lob.Pop(&arglist)
		// msg.Info("GetKeywords: have", elem1, ", rest ", arglist)
		if lob.IsKeyword(elem1) {
			if _, ok := allowed[elem1]; !ok {
				msg.Bailf("keyword %s unexpected", elem1)
			}
			if !lob.IsPair(arglist) {
				msg.Bailf("keyword %s without value", elem1)
			}
			elem2 := lob.Pop(&arglist)
			result[elem1] = elem2
		} else {
			msg.Bail("orphan argument where keyword expected:",
				elem1)
		}
	}
	// msg.Info("GetKeywords: return ", result)
	return result
}

type symSlice []*lob.Symbol

// The length of the slice; part of symSlice's interface for sorting.
func (s symSlice) Len() int {
	return len(s)
}

// The swap function of the slice; part of symSlice's interface for sorting.
func (s symSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// The comparison function of the slice; part of symlice's interface for
// sorting.
func (s symSlice) Less(i, j int) bool {
	return s[i].ValueString(nil) < s[j].ValueString(nil)
}

func sortedKeys(m lob.VarMap) (keys symSlice) {
	keys = make(symSlice, 0, len(m))
	for key, _ := range m {
		keys = append(keys, key)
	}
	sort.Sort(keys)
	return keys
}

func Init_bi_helpers() {
	//fmt.Println("fun/bi-helpers.init")
	emptyString = lob.NewString("")
}

// EOF
