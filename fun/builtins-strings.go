// String builtins
package fun

// Definitions of all builtin Lisp functions

//import "fmt"
import (
	"bytes"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"

	"../call"
	"../lob"
	"../msg"
	"../olu"
)

/// name string<
/// impl str_lt
/// mina 2
/// maxa 2
/// spec false
/// args OBJECT1 OBJECT2
/// retv boolish
/// docs
///   Compare the string representations of OBJECT1 and OBJECT2,
///   return non-nil if OBJECT1 < OBJECT2.
/// endd
func str_lt(arglist lob.Object, nargs int) lob.Object {
	s1 := arglist.Car().ValueString(nil)
	s2 := arglist.Cdr().Car().ValueString(nil)
	return lob.Bool2Ob(s1 < s2)
}

/// name string>
/// impl str_gt
/// mina 2
/// maxa 2
/// spec false
/// args OBJECT1 OBJECT2
/// retv boolish
/// docs
///   Compare the string representations of OBJECT1 and OBJECT2,
///   return non-nil if OBJECT1 > OBJECT2.
/// endd
func str_gt(arglist lob.Object, nargs int) lob.Object {
	s1 := arglist.Car().ValueString(nil)
	s2 := arglist.Cdr().Car().ValueString(nil)
	return lob.Bool2Ob(s1 > s2)
}

/// name string<=
/// impl str_le
/// mina 2
/// maxa 2
/// spec false
/// args OBJECT1 OBJECT2
/// retv boolish
/// docs
///   Compare the string representations of OBJECT1 and OBJECT2,
///   return non-nil if OBJECT1 <= OBJECT2.
/// endd
func str_le(arglist lob.Object, nargs int) lob.Object {
	s1 := arglist.Car().ValueString(nil)
	s2 := arglist.Cdr().Car().ValueString(nil)
	return lob.Bool2Ob(s1 <= s2)
}

/// name string>=
/// impl str_ge
/// mina 2
/// maxa 2
/// spec false
/// args OBJECT1 OBJECT2
/// retv boolish
/// docs
///   Compare the string representations of OBJECT1 and OBJECT2,
///   return non-nil if OBJECT1 >= OBJECT2.
/// endd
func str_ge(arglist lob.Object, nargs int) lob.Object {
	s1 := arglist.Car().ValueString(nil)
	s2 := arglist.Cdr().Car().ValueString(nil)
	return lob.Bool2Ob(s1 >= s2)
}

/// name string=
/// impl str_eq
/// mina 2
/// maxa 2
/// spec false
/// args OBJECT1 OBJECT2
/// retv boolish
/// docs
///   Compare the string representations of OBJECT1 and OBJECT2,
///   return non-nil if OBJECT1 = OBJECT2.
/// endd
func str_eq(arglist lob.Object, nargs int) lob.Object {
	s1 := arglist.Car().ValueString(nil)
	s2 := arglist.Cdr().Car().ValueString(nil)
	return lob.Bool2Ob(s1 == s2)
}

/// name string/=
/// impl str_ne
/// mina 2
/// maxa 2
/// spec false
/// args OBJECT1 OBJECT2
/// retv boolish
/// docs
///   Compare the string representations of OBJECT1 and OBJECT2,
///   return non-nil if OBJECT1 /= OBJECT2.
/// endd
func str_ne(arglist lob.Object, nargs int) lob.Object {
	s1 := arglist.Car().ValueString(nil)
	s2 := arglist.Cdr().Car().ValueString(nil)
	return lob.Bool2Ob(s1 != s2)
}

/// name make-string
/// impl makeString
/// mina 1
/// maxa 2
/// spec false
/// args LENGTH &optional INITIAL
/// retv string
/// docs
///   Build a string of specified LENGTH, with (optional) INITIAL as elements.
/// endd
func makeString(arglist lob.Object, nargs int) lob.Object {
	length := int(numberArg(arglist.Car(), "make-string: length"))
	initial := " "
	var haveFunction bool
	var function call.Callable
	if nargs > 1 {
		switch initarg := arglist.Cdr().Car().(type) {
		case call.Callable:
			haveFunction = true
			function = initarg
		default:
			initial = initarg.ValueString(nil)
			if len(initial) < 1 {
				msg.Bailf("make-string: initial argument empty")
			}
		}
	}
	buf := bytes.NewBufferString("")
	if haveFunction {
		// if we have a function argument as initial value, call it for
		// every element of the string to build, and use the first rune
		// of the resulting string value of the result for the string to
		// buil
		for i := 0; i < length; i++ {
			s := function.Call(0, lob.Nil).ValueString(nil)
			had_one := false
			for _, r := range s {
				buf.WriteRune(r)
				had_one = true
				break
			}
			if !had_one {
				msg.Bailf("make-string: initializer function " +
					"returns empty string value")
			}
		}
	} else {
		// first, convert the initial to something indexable with runes
		runes := make([]rune, len(initial)) // at worst too long
		rlen := 0                           // effective length of runes
		// slice
		for _, r := range initial {
			runes[rlen] = r
			rlen++
		}
		in_off := 0
		for i := 0; i < length; i++ {
			buf.WriteRune(runes[in_off%rlen])
			in_off++
		}
	}
	return lob.NewString(buf.String())
}

/// name join
/// impl join
/// mina 1
/// maxa -1
/// spec false
/// args STRING &rest ARGS
/// retv string
/// docs
///   Append all ARGS (or their elements if they are lists) into a single string
///   separated by STRING and return the result.
/// endd
func join(arglist lob.Object, nargs int) lob.Object {
	arg1, restArgs := arglist.Cxr()
	shim := arg1.ValueString(nil)
	buf := bytes.NewBufferString("")
	firstItem := true

	for !restArgs.IsNil() {
		// msg.Debug("restArgs:", restArgs)
		arg := lob.Pop(&restArgs)

		if lob.IsList(arg) {
			// msg.Debug("arg:", arg)
			for !arg.IsNil() {
				elem := lob.Pop(&arg)
				// msg.Debug("elem:", elem)
				if firstItem {
					firstItem = false
				} else {
					buf.WriteString(shim)
				}
				buf.WriteString(elem.ValueString(nil))
			}
		} else {
			if firstItem {
				firstItem = false
			} else {
				buf.WriteString(shim)
			}
			buf.WriteString(arg.ValueString(nil))
		}
	}
	return lob.NewString(buf.String())
}

/// name string-upcase
/// impl stringUpcase
/// mina 1
/// maxa 3
/// spec false
/// args STRING &optional START END
/// retv cased-string
/// docs
///   Return STRING with all lowercase chars replaced by uppercase chars.
///   START and END may specify the region of the string to be treated;
///   defaults are 0 and the end of the string.
/// endd
func stringUpcase(arglist lob.Object, nargs int) lob.Object {
	str := []rune(arglist.Car().ValueString(nil))
	start := 0
	end := len(str)
	if nargs > 1 {
		start = int(numberArg(arglist.Cdr().Car(),
			"string-upcase: start"))
		if start >= end {
			return lob.NewString(string(str))
		}
	}
	if nargs > 2 {
		end = int(numberArg(arglist.Cdr().Cdr().Car(),
			"string-upcase: end"))
		if end <= start {
			return lob.NewString(string(str))
		}
		if end > len(str) {
			end = len(str)
		}
	}
	result := make([]rune, len(str))
	for i := 0; i < len(str); i++ {
		if i >= start && i < end {
			result[i] = unicode.ToUpper(str[i])
		} else {
			result[i] = str[i]
		}
	}
	return lob.NewString(string(result))
}

/// name string-downcase
/// impl stringDowncase
/// mina 1
/// maxa 3
/// spec false
/// args STRING &optional START END
/// retv cased-string
/// docs
///   Return STRING with all uppercase chars replaced by lowercase chars.
///   START and END may specify the region of the string to be treated;
///   defaults are 0 and the end of the string.
/// endd
func stringDowncase(arglist lob.Object, nargs int) lob.Object {
	str := []rune(arglist.Car().ValueString(nil))
	start := 0
	end := len(str)
	if nargs > 1 {
		start = int(numberArg(arglist.Cdr().Car(),
			"string-downcase: start"))
		if start >= end {
			return lob.NewString(string(str))
		}
	}
	if nargs > 2 {
		end = int(numberArg(arglist.Cdr().Cdr().Car(),
			"string-downcase: end"))
		if end <= start {
			return lob.NewString(string(str))
		}
		if end > len(str) {
			end = len(str)
		}
	}
	result := make([]rune, len(str))
	for i := 0; i < len(str); i++ {
		if i >= start && i < end {
			result[i] = unicode.ToLower(str[i])
		} else {
			result[i] = str[i]
		}
	}
	return lob.NewString(string(result))
}

/// name string-capitalize
/// impl stringCapitalize
/// mina 1
/// maxa 3
/// spec false
/// args STRING &optional START END
/// retv cased-string
/// docs
///   Return STRING with all words capitalized.
///   START and END may specify the region of the string to be treated;
///   defaults are 0 and the end of the string.
/// endd
func stringCapitalize(arglist lob.Object, nargs int) lob.Object {
	str := []rune(arglist.Car().ValueString(nil))
	start := 0
	end := len(str)
	if nargs > 1 {
		start = int(numberArg(arglist.Cdr().Car(),
			"string-capitalize: start"))
		if start >= end {
			return lob.NewString(string(str))
		}
	}
	if nargs > 2 {
		end = int(numberArg(arglist.Cdr().Cdr().Car(),
			"string-capitalize: end"))
		if end <= start {
			return lob.NewString(string(str))
		}
		if end > len(str) {
			end = len(str)
		}
	}
	result := make([]rune, len(str))
	inword := false
	for i := 0; i < len(str); i++ {
		wordStart := false
		newc := str[i]
		if unicode.IsLetter(newc) || unicode.IsDigit(newc) {
			if !inword {
				wordStart = true
			}
			inword = true
		} else {
			inword = false
		}
		if i >= start && i < end {
			if wordStart {
				newc = unicode.ToUpper(newc)
			} else {
				newc = unicode.ToLower(newc)
			}
		}
		result[i] = newc
	}
	return lob.NewString(string(result))
}

/// name string
/// impl newString
/// mina 0
/// maxa -1
/// spec false
/// args &rest args
/// retv string
/// docs
///   Make a string from all arguments and return it.
/// endd
func newString(arglist lob.Object, nargs int) lob.Object {
	buf := bytes.NewBufferString("")
	for lob.IsPair(arglist) {
		arg := lob.Pop(&arglist)
		switch ob := arg.(type) {
		case *lob.Char:
			buf.WriteRune(ob.Value())
		default:
			buf.WriteString(ob.ValueString(nil))
		}
	}
	return lob.NewString(buf.String())
}

// replaced builtin regexp-match by macro

func makeRegex(arg lob.Object, where string) *regexp.Regexp {
	var expr string
	if lob.IsRegexp(arg) {
		return arg.(*lob.Regexp).Regexp()
	} else {
		expr = arg.ValueString(nil)
	}
	result, err := regexp.Compile(expr)
	if err != nil {
		msg.Bail(where+":", err)
	}
	return result
}

/// name regex-replace
/// impl regex_replace
/// mina 3
/// maxa 3
/// spec false
/// args REGEXP STRING NEWTXT
/// retv string
/// docs
///   Replace matches of REGEXP in STRING with NEWTXT.
/// endd
func regex_replace(arglist lob.Object, nargs int) lob.Object {
	regex := makeRegex(arglist.Car(), "regex-replace")
	source := stringArg(arglist.Cdr().Car(), "regex-replace: string")
	newtxt := stringArg(arglist.Cdr().Cdr().Car(),
		"regex-replace: newtxt")

	return lob.NewString(string(regex.ReplaceAllString(source, newtxt)))
}

/// name split-string
/// impl splitString
/// mina 1
/// maxa 4
/// spec false
/// args STRING &optional SEPARATOR LIMIT
/// retv list of strings
/// docs
///   Split STRING into parts separated by SEPARATOR; return them as list.
///   If SEPARATOR is a regexp object, a regexp match is done instead of a
///   string match. If it is nil or unspecified, it is assumed to be
///   whitespace. If LIMIT is non-nil and positive, it is the maximum
///   number of parts into which the string is split.
/// endd
func splitString(arglist lob.Object, nargs int) lob.Object {
	String := lob.Pop(&arglist)
	separator := lob.Pop(&arglist)
	limitArg := arglist.Car()

	s := stringArg(String, "split-string: string")
	var limit int
	if limitArg.IsNil() {
		limit = -1
	} else {
		limit = int(numberArg(limitArg, "split-string: limit"))
	}

	var parts []string
	if separator.IsNil() {
		if limit < 0 {
			parts = strings.Fields(s)
		} else {
			parts = regexp.MustCompile("\\s+").Split(s, limit)
		}
	} else if lob.IsRegexp(separator) {
		parts = separator.(*lob.Regexp).Regexp().Split(s, limit)
	} else if lob.IsString(separator) {
		parts = strings.SplitN(s, separator.ValueString(nil), limit)
	}
	var result lob.Object = lob.Nil
	for i := len(parts) - 1; i >= 0; i-- {
		result = lob.Cons(lob.NewString(parts[i]), result)
	}
	return result
}

/// name regexpp
/// impl regexpp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return t if ARG is a regexp, nil else.
/// endd
func regexpp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsRegexp(arglist.Car()))
}

/// name regexp
/// impl regExp
/// mina 1
/// maxa 1
/// spec false
/// args REGEXP
/// retv regexp object
/// docs
///   Return a new regexp object built from REGEXP (a kind of string).
/// endd
// (name needs to be different from that of the imported package "regexp", hence
// the hump)
func regExp(arglist lob.Object, nargs int) lob.Object {
	src := arglist.Car()
	if lob.IsRegexp(src) {
		return src
	}
	return lob.NewRegexp(src.ValueString(nil))
}

// Generate the charbag/cutset argument for Go's strings.Trim*() functions. The
// function argument can be a string or a list of things; in the latter case
// their value strings are concatenated.
func charbag(charBagArg lob.Object) string {
	switch bag := charBagArg.(type) {
	case *lob.String:
		return bag.ValueString(nil)
	case lob.Sequence:
		var charBag bytes.Buffer
		slength := bag.Length()
		for i := 0; i < slength; i++ {
			charBag.WriteString(bag.Element(i).ValueString(nil))
		}
		return charBag.String()
	default:
		return charBagArg.ValueString(nil)
	}
}

type side_t int // side(s) on which to trim the string
const (
	left_side side_t = 0 + iota
	right_side
	both_sides
)

// Do the string trimming for string-{,{left,right}-}trim.
func string_trim_help(arglist lob.Object, nargs int, side side_t) lob.Object {
	charBagArg := arglist.Car()
	stringArg := arglist.Cdr().Car()
	theString := stringArg.ValueString(nil)

	// which trim function to use for the specified side(s)?
	trimmer := []func(string, func(rune) bool) string{
		strings.TrimLeftFunc, strings.TrimRightFunc, strings.TrimFunc,
	}[side]

	// which function to decide if a character is to be trimmed?
	to_trim_func := unicode.IsSpace
	if !charBagArg.Eq(lob.T) {
		theCharBag := charbag(charBagArg)

		// true for all whitespace characters
		to_trim_func = func(r rune) bool {
			// true for all runes in theCharBag
			return strings.ContainsRune(theCharBag, r)
		}
	}

	// now trim and return the changed string or the original (to save us
	// allocating a new string)
	trimmedString := trimmer(theString, to_trim_func)
	if trimmedString == theString {
		return stringArg
	} else {
		return lob.NewString(trimmedString)
	}
}

/// name string-trim
/// impl string_trim
/// mina 2
/// maxa 2
/// spec false
/// args CHAR-BAG STRING
/// retv trimmed-string
/// docs
///   Return a substring of STRING, with characters in CHAR-BAG stripped off
///   the beginning and end. CHAR-BAG may be t, in which case all whitespace
///   characters will be stripped, or a sequence of characters.
/// endd
func string_trim(arglist lob.Object, nargs int) lob.Object {
	return string_trim_help(arglist, nargs, both_sides)
}

/// name string-left-trim
/// impl string_left_trim
/// mina 2
/// maxa 2
/// spec false
/// args CHAR-BAG STRING
/// retv trimmed-string
/// docs
///   Return a substring of STRING, with characters in CHAR-BAG stripped off
///   the beginning. CHAR-BAG may be t, in which case all whitespace
///   characters will be stripped, or a sequence of characters.
/// endd
func string_left_trim(arglist lob.Object, nargs int) lob.Object {
	return string_trim_help(arglist, nargs, left_side)
}

/// name string-right-trim
/// impl string_right_trim
/// mina 2
/// maxa 2
/// spec false
/// args CHAR-BAG STRING
/// retv trimmed-string
/// docs
///   Return a substring of STRING, with characters in CHAR-BAG stripped off
///   the end. CHAR-BAG may be t, in which case all whitespace
///   characters will be stripped, or a sequence of characters.
/// endd
func string_right_trim(arglist lob.Object, nargs int) lob.Object {
	return string_trim_help(arglist, nargs, right_side)
}

/// name substring
/// impl substring
/// mina 2
/// maxa 3
/// spec false
/// args string start &optional end
/// retv the substring
/// docs
///   Return a substring of STRING, bounded by indices START and END
///   (or the end of the original string).
/// endd
func substring(arglist lob.Object, nargs int) lob.Object {
	stringArg := arglist.Car().ValueString(nil)
	start := intArg(arglist.Cdr().Car(), "start")
	end := intArgOr(arglist.Cdr().Cdr().Car(), "end", -1)
	strlen := utf8.RuneCountInString(stringArg)
	if strlen < end {
		end = strlen
	}
	return lob.NewString(olu.RuneSubstr(stringArg, start, end))
}

// EOF
