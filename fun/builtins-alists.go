// Association list builtins
package fun

import (
	"../call"
	"../lob"
)

// body for assoc, assq, sassoc, sassq: look up item in alist; if none matches,
// return defaultValue or, if it is a function, call it with no arguments to
// return and return its value. If eq is true, the test is #'eq, otherwise
// equal.
func assocBody(item, alist, defaultValue lob.Object, eq bool) lob.Object {
	for lob.IsPair(alist) {
		cons := lob.Pop(&alist)
		if eq {
			if cons.Car().Eq(item) {
				return cons
			}
		} else {
			if cons.Car().Equal(item) {
				return cons
			}
		}
	}
	switch defaultFunction := defaultValue.(type) {
	case call.Callable:
		return defaultFunction.Call(0, lob.Nil)
	}
	return defaultValue
}

/// name assoc
/// impl assoc
/// mina 2
/// maxa 2
/// spec false
/// args ITEM ALIST
/// retv pair
/// docs
///   Look up ITEM in ALIST and return the pair whose car is equal to ITEM.
///   Return nil if ITEM is not found as a car in one of the pairs in ALIST.
/// endd
func assoc(arglist lob.Object, nargs int) lob.Object {
	return assocBody(arglist.Car(), arglist.Cdr().Car(), lob.Nil, false)
}

/// name assq
/// impl assq
/// mina 2
/// maxa 2
/// spec false
/// args ITEM ALIST
/// retv pair
/// docs
///   Look up ITEM in ALIST and return the pair whose car is eq to ITEM.
///   Return nil if ITEM is not found as a car in one of the pairs in ALIST.
/// endd
func assq(arglist lob.Object, nargs int) lob.Object {
	return assocBody(arglist.Car(), arglist.Cdr().Car(), lob.Nil, true)
}

/// name sassoc
/// impl sassoc
/// mina 3
/// maxa 3
/// spec false
/// args ITEM ALIST DEFAULT
/// retv pair
/// docs
///   Look up ITEM in ALIST and return the pair whose car is equal to ITEM.
///   If ITEM is not in ALIST, return DEFAULT, or if it is a function, call
///   it with no args and return the result.
/// endd
func sassoc(arglist lob.Object, nargs int) lob.Object {
	return assocBody(arglist.Car(), arglist.Cdr().Car(),
		arglist.Cdr().Cdr().Car(), false)
}

/// name sassq
/// impl sassq
/// mina 3
/// maxa 3
/// spec false
/// args ITEM ALIST DEFAULT
/// retv pair
/// docs
///   Look up ITEM in ALIST and return the pair whose car is eq to ITEM.
///   If ITEM is not in ALIST, return DEFAULT, or if it is a function, call
///   it with no args and return the result.
/// endd
func sassq(arglist lob.Object, nargs int) lob.Object {
	return assocBody(arglist.Car(), arglist.Cdr().Car(),
		arglist.Cdr().Cdr().Car(), true)
}

// body for assoc_if and assoc_if_not: look up item in alist with function test;
// if none matches, call default with no arguments to return a default. Use the
// name of the original function in error messages; if eq is true, the test is
// #'eq, otherwise equal.
func assocIfBody(test, alist lob.Object, name string, if_not bool) lob.Object {
	testfun := callableArg(test, name+": function")
	for lob.IsPair(alist) {
		cons := lob.Pop(&alist)
		testval := testfun.Call(1, lob.Cons(cons.Car(), lob.Nil))
		if lob.Ob2Bool(testval) != if_not {
			return cons
		}
	}
	return lob.Nil
}

/// name assoc-if
/// impl assoc_if
/// mina 2
/// maxa 2
/// spec false
/// args PREDICATE ALIST
/// retv pair
/// docs
///   Return the first cons in ALIST for whose car PREDICATE is true.
///   If there is no such cons, return nil.
/// endd
func assoc_if(arglist lob.Object, nargs int) lob.Object {
	return assocIfBody(arglist.Car(), arglist.Cdr().Car(),
		"assoc-if", false)
}

/// name assoc-if-not
/// impl assoc_if_not
/// mina 2
/// maxa 2
/// spec false
/// args PREDICATE ALIST
/// retv pair
/// docs
///   Return the first cons in ALIST for whose car PREDICATE is false.
///   If there is no such cons, return nil.
/// endd
func assoc_if_not(arglist lob.Object, nargs int) lob.Object {
	return assocIfBody(arglist.Car(), arglist.Cdr().Car(),
		"assoc-if-not", true)
}

// EOF
