// Networking-related builtins
// very incomplete currently
package fun

import (
	"../lob"
	"../olu"
)

/// name netaddrp
/// impl netaddrp
/// mina 1
/// maxa 1
/// spec false
/// args ARG
/// retv t/nil
/// docs
///   Return true iff ARG is a netaddr
/// endd
func netaddrp(arglist lob.Object, nargs int) lob.Object {
	return lob.Bool2Ob(lob.IsNetaddr(arglist.Car()))
}

/// name netaddr
/// impl netaddr
/// mina 1
/// maxa 2
/// spec false
/// args DESTINATION &optional PORTNUMBER
/// retv netaddr object
/// docs
///   Create a new network address and return it.
///   DESTINATION may be a "host:port" string or just the host; in this case,
///   PORT may be the port number (or the corresponding string.
/// endd
func netaddr(arglist lob.Object, nargs int) lob.Object {
	hostmaybeport := arglist.Car().ValueString(nil)
	maybeport := ""
	if nargs == 2 {
		maybeport = arglist.Cdr().Car().ValueString(nil)
	}
	return lob.NewNetaddr(olu.SplitHostPort(hostmaybeport, maybeport))
}

// EOF
