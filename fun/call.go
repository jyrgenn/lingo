// Calling functions and macros
package fun

import (
	"fmt"

	"../hlp"
	"../lob"
	"../msg"
)

func IsBuiltin(ob interface{}) bool {
	switch fun := ob.(type) {
	case *Function:
		return fun.ftype == BUILTIN
	}
	return false
}

// arglist is already evaluated if necessary
func (fun *Function) Call(nargs int, arglist lob.Object) lob.Object {
	if fun.trace {
		fmt.Printf("%*s(%s %s\n", lob.EvalLevel(), "", fname(fun),
			arglist.ValueString(nil)[1:])
	}
	if nargs < fun.minargs {
		args := arglist.ValueString(nil)
		if args == "nil" {
			args = "()"
		}
		msg.Bail("too few arguments for function " +
			fun.ValueString(nil) + "; args are " + args)
	}
	if fun.maxargs >= 0 && nargs > fun.maxargs {
		args := arglist.ValueString(nil)
		if args == "nil" {
			args = "()"
		}
		msg.Bail("too many arguments for function " +
			fun.ValueString(nil) + "; args are " + args)
	}
	switch fun.ftype {
	case BUILTIN:
		return fun.builtin(arglist, nargs)
	case LAMBDA:
		return callForm(fun, fun.fi.env, arglist, nargs)
	case MACRO:
		return callForm(fun, lob.Env, arglist, nargs)
	}
	return nil
}

func bindParams(fun *Function, args lob.Object) {
	fi := fun.fi
	// ordinary parameters: there will be enough ensured by minargs, so we
	// don't need to check for that -- nor if a keyword is among them
	for _, param := range fi.ordParams {
		param.(*lob.Symbol).Bind(lob.Pop(&args))
	}
	// does this function have keywords at all?
	hasKeywords := len(fi.keywParams) != 0
	// &optional parameters: beware of keywords breaking the chain! On
	// seeing the first *valid* one, we must stop taking values for optional
	// args from the arglist, but still assign the &optionals' default
	// values.
	foundKeyword := false
	for index, param := range fi.optParams {
		var value lob.Object
		if !foundKeyword && lob.IsPair(args) {
			prevArgs := args // ready to step back...
			value = lob.Pop(&args)
			if hasKeywords && lob.IsKeyword(value) {
				ksym := value.(*lob.Symbol)
				// is this actually a valid keyword for this
				// function? Only then we skip the rest of the
				// &optionals
				if _, exists := fi.keywParams[ksym]; exists {
					args = prevArgs
					foundKeyword = true
					value = lob.EvalCurEnv(
						fi.optInitforms[index])
				}
			}
		} else {
			value = lob.EvalCurEnv(fi.optInitforms[index])
		}
		param.(*lob.Symbol).Bind(value)
	}
	// &rest parameter
	if fi.restParam != nil {
		var value lob.Object = lob.Nil
		if lob.IsPair(args) {
			if !hasKeywords {
				value = args
				args = lob.Nil // all done
			} else {
				// we (may) have keywords arguments, so we must
				// copy the &rest of the arglist until the first
				// keyword is encountered (or to the end, if
				// never)
				lp := lob.NewListPusher()
				for lob.IsPair(args) {
					first := args.Car()
					if lob.IsKeyword(first) {
						break
					} else {
						lp.Push(first)
						args = args.Cdr()
					}
				}
				value = lp.List()
			}
		}
		fi.restParam.(*lob.Symbol).Bind(value)
	}
	// &key arguments, if present; skip that part if the function doesn't
	// have keywords at all
	if !hasKeywords {
		if lob.IsPair(args) {
			msg.Bailf("unexpected argument(s) for function %v: %v",
				fun.name, args)
		}
		return
	}
	// now, keywords: have a new value map to store the actual arguments
	// (and the copied default values); an assignment of the map won't do,
	// as a map is apparently a pointer-like type on the inside
	kvalues := make(lob.VarMap)
	for key, initform := range fi.keywParams {
		kvalues[key] = lob.EvalCurEnv(initform)
	}
	for lob.IsPair(args) {
		// first a keyword
		key := lob.Pop(&args)
		if !lob.IsSymbol(key) {
			msg.Bailf("expected keyword argument for function %v,"+
				" found %v", fun.name, key)
		}
		keysym := key.(*lob.Symbol)
		_, exists := kvalues[keysym]
		if !exists {
			errfmt := "invalid keyword argument %v for function %v"
			if !lob.IsKeyword(key) {
				errfmt = "keyword expected, found: %v" +
					" (calling %v)"
			}
			msg.Bailf(errfmt, key, fun.name)
		}
		if !lob.IsPair(args) {
			msg.Bailf("value expected after keyword argument %v"+
				" when calling function %v",
				key, fun.name)
		}
		value := lob.Pop(&args)
		kvalues[keysym] = value
	}
	// now bind values to all keyword parameters
	for keysym, value := range kvalues {
		keysym.Keyword2Param().Bind(value)
	}
}

func callForm(fun *Function, env *lob.Environment,
	args lob.Object, nargs int) lob.Object {

	savedEnv := lob.EnterEnvironment(env)
	defer savedEnv.BackTo()

	bindParams(fun, args)
	if fun.ftype == MACRO {
		return lob.EvalCurEnv(hlp.EvalProgn(fun.fi.bodyForms))
	} else {
		return hlp.EvalProgn(fun.fi.bodyForms)
	}
}

func Init_call() {
	//fmt.Println("fun/call.init")
}

// EOF
