// Control structure builtins
package fun

import (
	"fmt"

	"../eval"
	"../hlp"
	"../lob"
	"../msg"
)

/// name cond
/// impl cond
/// mina 0
/// maxa -1
/// spec true
/// args &rest CLAUSES
/// retv value
/// docs
///   For CLAUSES of the for (test-form value-form) evaluate test-form,
///   and for the first one that is non-nil, return the value of evaluating
///   value-form.
/// endd
func cond(arglist lob.Object, nargs int) lob.Object {
	clauseforms := arglist
	for lob.IsPair(clauseforms) {
		clause := lob.Pop(&clauseforms)
		val := lob.EvalCurEnv(clause.Car())
		if !val.IsNil() {
			forms := clause.Cdr()
			if forms.IsNil() {
				return val
			}
			return hlp.EvalProgn(forms)
		}
	}
	return lob.Nil
}

/// name while
/// impl while
/// mina 1
/// maxa -1
/// spec true
/// args COND &optional BODYFORMS
/// retv nil
/// docs
///   IF COND evaluates non-nil, evaluate BODYFORMS and repeat.
/// endd
func while(arglist lob.Object, nargs int) lob.Object {
	cond, body := arglist.Cxr()
	for lob.EvalCurEnv(cond) != lob.Nil {
		hlp.EvalProgn(body)
	}
	return lob.Nil
}

/// name flet
/// impl flet
/// mina 1
/// maxa -1
/// spec true
/// args "(BINDINGS...) BODY ..."
/// retv last body value
/// docs
///   Bind one or more functions to symbols and evaluate BODY.
///   The BINDINGS are (symbol . function) pairs.
/// endd
func flet(arglist lob.Object, nargs int) lob.Object {
	bindings, body := arglist.Cxr()
	var symbols lob.Object = lob.Nil
	defer func() {
		for lob.IsPair(symbols) {
			pair := lob.Pop(&symbols)
			sym, fval := pair.Cxr()
			sym.(*lob.Symbol).SetFunctionSilently(fval)
		}
	}()
	for lob.IsPair(bindings) {
		binding := lob.Pop(&bindings)
		proper, len := hlp.IsProperList(binding)
		if !proper || len != 2 {
			msg.Bail("flet: binding not list of 2:", binding)
		}
		sym, brest := binding.Cxr()
		symfun := symbolArg(sym, "flet: function var").Function()
		symbols = lob.Cons(lob.Cons(sym, symfun), symbols)
		fval := lob.EvalCurEnv(brest.Car())
		sym.(*lob.Symbol).SetFunction(fval)
	}
	if !bindings.IsNil() {
		msg.Bail("flet: bindings not a proper list:",
			arglist.Car())
	}
	return hlp.EvalProgn(body)
}

/// name errset
/// impl errset
/// mina 1
/// maxa 2
/// spec true
/// args EXPR &optional PRINT-ERROR
/// retv list of result or nil
/// docs
///   Return the value of EXPR as a singleton list; on error return nil.
///   In the latter case, a description of the error is in sys:last-error, and,
///   if optional PRINT-ERROR is non-nil or omitted, it is printed as well.
/// endd
func errset(arglist lob.Object, nargs int) lob.Object {
	var retval lob.Object = lob.Nil
	printflag := true
	if nargs > 1 && arglist.Cdr().Car().IsNil() {
		printflag = false
	}
	saveErrset := msg.InErrset
	msg.InErrset = true
	defer func() { msg.InErrset = saveErrset }()

	func() {
		defer func() {
			err := recover()
			if err != nil {
				switch err.(type) {
				case *lob.Pair:
					// this is a throw, pass on to be caught
					// by a catch
					panic(err)
				default:
					lob.SysLastError.SetValue(lob.
						NewString(fmt.Sprint(err)))
					if printflag {
						msg.PrintError(err)
					}
				}
			}
		}()
		retval = lob.ListOf(lob.EvalCurEnv(arglist.Car()))
	}()
	return retval
}

// help to bind the fields of value to the variables in varlist; actually append
// them to the passed symbols/values slices so they can be bound after all
// values of the let/call have been evaluated -- or earlier, depending if we are
// in let or let*.
func destructuringBind(varlist lob.Object, values lob.Object,
	syms *[]*lob.Symbol, vals *[]lob.Object) {

	for lob.IsPair(varlist) {
		if !lob.IsPair(values) && !(values == lob.Nil) {
			msg.Bail("let/let*: value structure error, not pair:",
				values)
		}

		head := lob.Pop(&varlist)
		firstVal := lob.Pop(&values)
		switch varExp := head.(type) {
		case *lob.Symbol:
			// skip assignment if symbol is Nil, meaning we don't
			// need that particular value
			if varExp != lob.Nil {
				*syms = append(*syms, varExp)
				*vals = append(*vals, firstVal)
			}
		case *lob.Pair:
			destructuringBind(varExp, firstVal, syms, vals)
		}
	}
	// if the variable list is ended by a non-nil symbol, bind remaining
	// values to it (or nil)
	if varlist != lob.Nil {
		switch lastSym := varlist.(type) {
		case *lob.Symbol:
			*syms = append(*syms, lastSym)
			*vals = append(*vals, values)
		default:
			msg.Bail("let/let*: non-symbol terminates varlist:",
				lastSym)
		}
	}
}

/// name let
/// impl let
/// mina 1
/// maxa -1
/// spec true
/// args ((SYMBOL1 [VALUE1]) ...) BODYFORMS...
/// retv last body value
/// docs
///   Evaluate BODY with local bindings, return value of last BODY expressions.
///   bindings are of the form SYMBOL or (SYMBOL) or (SYMBOL VALUE), where the
///   first two bind SYMBOL to nil. All VALUEs are evaluated before any variable
///   bindings are done.
/// endd
func let(arglist lob.Object, nargs int) lob.Object {
	varlist, body := arglist.Cxr()
	symbols := []*lob.Symbol{} // symbols to be bound
	values := []lob.Object{}   // values for them
	for lob.IsPair(varlist) {
		var first lob.Object
		first, varlist = pairArg(varlist,
			"let: invalid varlist").Cxr()
		switch vardef := first.(type) {
		case *lob.Symbol:
			symbols = append(symbols, vardef)
			values = append(values, lob.Nil)
		case *lob.Pair:
			var value lob.Object
			theCar := vardef.Car()
			theCdr := vardef.Cdr()
			if lob.IsPair(theCdr) {
				value = lob.EvalCurEnv(theCdr.Car())
				if !theCdr.Cdr().IsNil() {
					msg.Bail("let: malformed variable "+
						"specification:", vardef)
				}
			} else if theCdr.IsNil() {
				value = lob.Nil
			} else {
				msg.Bail("let: invalid variable "+
					"specification:", vardef)
			}
			switch varExp := theCar.(type) {
			case *lob.Symbol:
				symbols = append(symbols, varExp)
				values = append(values, value)
			case *lob.Pair:
				destructuringBind(theCar, value,
					&symbols, &values)
			default:
				msg.Bail("let: invalid declaration structure:",
					vardef)
			}
		default:
			msg.Bailf("let: hosed variable "+
				"specification: ", vardef)
		}
	}
	savedEnv := lob.EnterEnvironment()
	nSaved := len(symbols)
	for i := 0; i < nSaved; i++ {
		symbols[i].Bind(values[i])
	}
	defer savedEnv.BackTo()

	return hlp.EvalProgn(body)
}

/// name let*
/// impl letrec
/// mina 1
/// maxa -1
/// spec true
/// args ((VAR1 VALUE1) ...) BODYFORMS...
/// retv last body value
/// docs
///   Evaluate BODY with local BINDINGS, return value of last BODY expressions.
///   BINDINGS are of the form SYMBOL or (SYMBOL) or (SYMBOL VALUE), where the
///   first two bind SYMBOL to nil. VALUE is evaluated with bindings of earlier
///   variables in the same let* already in place.
/// endd
func letrec(arglist lob.Object, nargs int) lob.Object {
	varlist, body := arglist.Cxr()
	savedEnv := lob.EnterEnvironment()
	defer savedEnv.BackTo()

	for lob.IsPair(varlist) {
		first := lob.Pop(&varlist)
		switch vardef := first.(type) {
		case *lob.Symbol:
			vardef.Bind(lob.Nil)
		case *lob.Pair:
			var valueform lob.Object
			theCar := vardef.Car()
			theCdr := vardef.Cdr()
			if lob.IsPair(theCdr) {
				if !theCdr.Cdr().IsNil() {
					msg.Bailf("let*: malformed variable "+
						"specification: ", vardef)
				}
				valueform = theCdr.Car()
			} else if theCdr.IsNil() {
				valueform = lob.Nil
			} else {
				msg.Bailf("let*: invalid variable "+
					"specification: ", vardef)
			}
			value := lob.EvalCurEnv(valueform)
			switch varExp := theCar.(type) {
			case *lob.Symbol:
				varExp.Bind(value)
			case *lob.Pair:
				symbols := []*lob.Symbol{}
				values := []lob.Object{}
				destructuringBind(theCar, value,
					&symbols, &values)
				nSaved := len(symbols)
				for i := 0; i < nSaved; i++ {
					symbols[i].Bind(values[i])
				}
			default:
				msg.Bail("let: invalid declaration structure:",
					vardef)
			}

		default:
			msg.Bailf("let*: hosed variable specification: ",
				vardef)
		}
	}
	if !varlist.IsNil() {
		msg.Bailf("let*: bindings not a proper list", varlist)
	}

	return hlp.EvalProgn(body)
}

/// name prog1
/// impl prog1
/// mina 1
/// maxa -1
/// spec false
/// args RESULT-FORM &rest BODYFORMS
/// retv first value
/// docs
///   Evaluate all forms and return the value of the first one.
/// endd
func prog1(arglist lob.Object, nargs int) lob.Object {
	var result lob.Object
	for lob.IsPair(arglist) {
		value := lob.Pop(&arglist)
		if result == nil {
			result = value
		}
	}
	return result
}

/// name progn
/// impl progn
/// mina 0
/// maxa -1
/// spec false
/// args &rest BODYFORMS
/// retv last value
/// docs
///   Evaluate all BODYFORMS and return the value of the last one.
/// endd
func progn(arglist lob.Object, nargs int) lob.Object {
	result := lob.Object(lob.Nil)
	for lob.IsPair(arglist) {
		result = lob.Pop(&arglist)
	}
	return result
}

/// name if
/// impl if_
/// mina 2
/// maxa -1
/// spec true
/// args COND THEN &rest ELSE
/// retv value
/// docs
///   If COND evaluates non-nil, evaluate THEN form, otherwise the ELSE forms.
/// endd
func if_(arglist lob.Object, nargs int) lob.Object {
	cond, rest := arglist.Cxr()
	if lob.EvalCurEnv(cond).IsNil() {
		return hlp.EvalProgn(rest.(lob.List).Cdr())
	}
	return lob.EvalCurEnv(rest.(lob.List).Car())
}

/// name unwind-protect
/// impl unwind_protect
/// mina 2
/// maxa -1
/// spec true
/// args bodyform cleanupforms...
/// retv value
/// docs
///   Eval BODYFORM, and even in case of an error or throw, eval CLEANUPFORMS.
///   If BODYFORM completes normally, its value is returned after executing
///   the CLEANUPFORMS.
/// endd
func unwind_protect(arglist lob.Object, nargs int) lob.Object {
	bodyform, cleanupforms := arglist.Cxr()
	defer hlp.EvalProgn(cleanupforms)
	return lob.EvalCurEnv(bodyform)
}

/// name catch
/// impl catch
/// mina 1
/// maxa -1
/// spec true
/// args tag bodyforms...
/// retv value
/// docs
///   Eval BODYFORMS as implicit progn. If a throw occurs to the TAG,
///   return the value that is thrown.
/// endd
func catch(arglist lob.Object, nargs int) (result lob.Object) {
	catchTag := eval.EvalCurEnv(arglist.Car())
	defer func() {
		pvalue := recover()
		if pvalue != nil {
			switch tvalue := pvalue.(type) {
			case *lob.Pair:
				// our tag? return result
				if catchTag.Eq(tvalue.Car()) {
					result = tvalue.Cdr()
					return
				}
			}
			// pass on if error or not our tag
			panic(pvalue)
		}
	}()

	result = hlp.EvalProgn(arglist.Cdr())
	return result
}

/// name throw
/// impl throw
/// mina 2
/// maxa 2
/// spec false
/// args tag resultform
/// retv none
/// docs
///   Cause a non-local control transfer to a catch whose tag is eq to TAG.
///   The value returned by that catch is the value of RESULTFORM.
/// endd
func throw(arglist lob.Object, nargs int) lob.Object {
	// panic with a (tag . value) cons
	panic(lob.Cons(arglist.Car(), arglist.Cdr().Car()))
	return lob.Nil
}

// EOF
