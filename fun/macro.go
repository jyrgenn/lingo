// Macro expansion
package fun

import (
	"../hlp"
	"../lob"
)

var macroCounter = 0

func NMacros() int {
	return macroCounter
}

// expand a list of forms; return new list, haveExpanded
func expandList(list lob.Object) (result lob.Object, haveExpanded bool) {
	haveExpanded = false
	lp := lob.NewListPusher()
	for lob.IsPair(list) {
		elem := lob.Pop(&list)
		elem, didHere := macroExpandRecurse(elem)
		lp.Push(elem)
		haveExpanded = haveExpanded || didHere
	}
	if list != lob.Nil { // last cdr of improper list
		elem, didHere := macroExpandRecurse(list)
		lp.AddEnd(elem)
		haveExpanded = haveExpanded || didHere
	}
	return lp.List(), haveExpanded
}

// expand a single macro call form
func expandMacro(macro *Function, args lob.Object) lob.Object {
	savedEnv := lob.EnterEnvironment()
	defer savedEnv.BackTo()

	bindParams(macro, args)
	return hlp.EvalProgn(macro.fi.bodyForms)
}

// expand all macro calls in a form, 1 pass; return the resulting form and
// whether something has been expanded (in which case we'll try again) or not
func macroExpandRecurse(form lob.Object) (result lob.Object, hasExpanded bool) {
	if lob.IsPair(form) {
		head, args := form.Cxr()
		if lob.IsSymbol(head) {
			maybeMacro := head.(*lob.Symbol).Function()
			if maybeMacro != nil && IsMacro(maybeMacro) {
				result = expandMacro(maybeMacro.(*Function),
					args)
				hasExpanded = true
			} else {
				result, hasExpanded = expandList(form)
			}
		} else {
			result, hasExpanded = expandList(form)
		}
	} else {
		result = form
	}
	return result, hasExpanded
}

// expand macros in a form until there is nothing left to expand
func MacroExpandForm(form lob.Object) lob.Object {
	onceMore := true
	for onceMore {
		form, onceMore = macroExpandRecurse(form)
	}
	return form
}

func Init_macro() {
	// fmt.Println("fun/macro.init")
}
