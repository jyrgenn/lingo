// OS interface builtins
package fun

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"../lob"
	"../msg"
	"../olu"
)

/// name expand-file-name
/// impl expand_file_name
/// mina 1
/// maxa 1
/// spec false
/// args pathname
/// retv expanded pathname
/// docs
///   Return pathname in normal form and with ~ and ~user expanded
///   at the beginning of the pathname, provided the user exists.
///   Normal form means multiple consecutive slashes and /./ are
///   reduced to a single slash, and /../ constructs are resolved.
///   This function is intended to be similar to the Emacs Lisp
///   function of the same name.
///   The value returned is a string, even if the argument is not.
/// endd
func expand_file_name(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	argstring := arg.ValueString(nil)
	newpath := olu.ExpandFileName(argstring)
	if newpath != argstring || !lob.IsString(arg) {
		return lob.NewString(newpath)
	}
	// don't allocate a NewString if it hasn't changed
	return arg
}

/// name normalize-pathname
/// impl normalize_pathname
/// mina 1
/// maxa 1
/// spec false
/// args pathname
/// retv normalized pathname
/// docs
///   Return pathname in normalized form.
///   This means multiple consecutive slashes and /./ are reduced
///   to a single slash, and /../ constructs are resolved.
///   Other than with expand-file-name, ~ and ~user are not expanded.
///   The value returned is a string, even if the argument is not.
/// endd
func normalize_pathname(arglist lob.Object, nargs int) lob.Object {
	arg := arglist.Car()
	argstring := get_pathname(arg, "normalize_pathname")
	newpath := olu.NormalizePathname(argstring)
	if newpath != argstring || !lob.IsString(arg) {
		return lob.NewString(newpath)
	}
	// don't allocate a NewString if it hasn't changed
	return arg
}

/// name get-internal-real-time
/// impl get_internal_real_time
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv internal-time
/// docs
///   Return the current time in internal time units.
///
/// endd
func get_internal_real_time(arglist lob.Object, nargs int) lob.Object {
	return lob.NewInt64Number(time.Now().UnixNano())
}

/// name sleep
/// impl sleep
/// mina 1
/// maxa 1
/// spec false
/// args SECONDS
/// retv nil
/// docs
///   Sleep for a number of SECONDS (which may be fractional) and return nil.
/// endd
func sleep(arglist lob.Object, nargs int) lob.Object {
	duration := 1e9 * numberArg(arglist.Car(), "sleep: duration")
	solong := time.Duration(duration) * time.Nanosecond
	time.Sleep(solong)
	return lob.Nil
}

/// name getenv
/// impl getenv
/// mina 1
/// maxa 1
/// spec false
/// args ENVVAR
/// retv string
/// docs
///   Return the value of ENVVAR from the process environment.
///   Return an empty string if it is not present.
/// endd
func getenv(arglist lob.Object, nargs int) lob.Object {
	envvar := arglist.Car().ValueString(nil)
	return lob.NewString(os.Getenv(envvar))
}

/// name setenv
/// impl setenv
/// mina 2
/// maxa 2
/// spec false
/// args VAR VALUE
/// retv value
/// docs
///   Set process environment variable VAR to VALUE and return the resul.
/// endd
func setenv(arglist lob.Object, nargs int) lob.Object {
	envvar := arglist.Car().ValueString(nil)
	value := arglist.Cdr().Car().ValueString(nil)
	err := os.Setenv(envvar, value)
	if err == nil {
		msg.Bail("setenv: error", err)
	}
	return lob.NewString(value)
}

/// name process-environment
/// impl process_environment
/// mina 0
/// maxa 0
/// spec false
/// args
/// retv env-alist
/// docs
///   Return the process environment as a (key . value) alist.
/// endd
func process_environment(arglist lob.Object, nargs int) lob.Object {
	table := lob.NewTable()
	for _, entry := range os.Environ() {
		elems := strings.SplitN(entry, "=", 2)
		table.Put(lob.NewString(elems[0]), lob.NewString(elems[1]))
	}
	return table
}

/// name shell
/// impl shell
/// mina 1
/// maxa 1
/// spec false
/// args COMMAND
/// retv status
/// docs
///   Run a shell command and return t on success, nil else.
/// endd
func shell(arglist lob.Object, nargs int) lob.Object {
	cmd := arglist.Car().ValueString(nil)
	command := exec.Command("/bin/sh", "-c", cmd)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	command.Stdin = os.Stdin
	err := command.Run()
	if err != nil {
		msg.Bail("shell:", err)
	}
	return lob.Bool2Ob(err == nil)
}

/// name run-process
/// impl run_process
/// mina 1
/// maxa -1
/// spec false
/// args COMMAND ARG1 ... &key (input "") (output t) (error t)
/// retv (stdout stderr)
/// docs
///   Run a process with COMMAND and ARG1 ... and get the results
///   The return value is a list of two elements, the standard output of
///   the process if captured (see below), and the standard error output
///   of the process if captured. If one of those outputs are not captured,
///   the corresponding element will be nil.
///
///   If &key input is supplied, this will be the stdin of the process,
///   either a string or an input port.
///
///   If &key output is t (the default), output will be captured and returned
///   as the first element of the returned list. If it is nil, output will
///   go to stdout. If it is a port, this describes the file for the output.
///
///   &key error controls handling of stderr analogous to output/stdout.
/// endd
func run_process(arglist lob.Object, nargs int) lob.Object {
	program := arglist.Car().ValueString(nil)
	args := []string{}
	for lob.IsPair(arglist) {
		another := lob.Pop(&arglist)
		if lob.IsKeyword(another) {
			arglist = lob.Cons(another, arglist)
			break
		}
		args = append(args, another.ValueString(nil))
	}
	cmd := exec.Command(program)
	if len(args) == 0 {
		args = append(args, cmd.Path)
	}
	cmd.Args = args

	keywords := getKeywords(arglist, lob.Obmap{
		lob.OptError:  lob.T,
		lob.OptInput:  emptyString,
		lob.OptOutput: lob.T,
	})
	// :input
	switch input := keywords[lob.OptInput].(type) {
	case *lob.Port:
		if !input.IsInput() || !input.IsOpen() {
			msg.Bail("run-process: not an open input port:", input)
		}
		cmd.Stdin = input.Reader()
	case *lob.String:
		cmd.Stdin = strings.NewReader(input.ValueString(nil))
	default:
		msg.Bail("run-process: port or string needed for :input, was",
			input.ReaderString(nil))
	}
	// :output
	var outputCapture *bytes.Buffer
	switch output := keywords[lob.OptOutput].(type) {
	case *lob.Port:
		if !output.IsOutput() || !output.IsOpen() {
			msg.Bail("run-process: not an open output port:",
				output)
		}
		cmd.Stdout = output.Writer()
	case *lob.Symbol:
		if output == lob.T {
			outputCapture = bytes.NewBufferString("")
			cmd.Stdout = outputCapture
		} else if output == lob.Nil {
			cmd.Stdout = lob.StdoutWriter()
		}
	}
	// :error
	var stderrCapture *bytes.Buffer
	switch stderr := keywords[lob.OptError].(type) {
	case *lob.Port:
		if !stderr.IsOutput() || !stderr.IsOpen() {
			msg.Bail("run-process: not an open output port:",
				stderr)
		}
		cmd.Stderr = stderr.Writer()
	case *lob.Symbol:
		if stderr == lob.T {
			stderrCapture = bytes.NewBufferString("")
			cmd.Stderr = stderrCapture
		} else if stderr == lob.Nil {
			cmd.Stderr = lob.StderrWriter()
		}
	}

	err := cmd.Run()
	if err != nil {
		msg.Bail("run-process:", err)
	}
	var output lob.Object = lob.Nil
	if outputCapture != nil {
		output = lob.NewString(outputCapture.String())
	}
	var stderr lob.Object = lob.Nil
	if stderrCapture != nil {
		stderr = lob.NewString(stderrCapture.String())
	}
	return lob.Cons(output, lob.Cons(stderr, lob.Nil))
}

/// name chdir
/// impl chdir
/// mina 1
/// maxa 1
/// spec false
/// args DIR &optional NOERROR
/// retv t/nil
/// docs
///   Change the current directory to DIR and return t on success.
///   On failure, raise an error, or return nil if NOERROR is true.
/// endd
func chdir(arglist lob.Object, nargs int) lob.Object {
	dir := arglist.Car().ValueString(nil)
	noerror := nargs > 1 && !arglist.Cdr().Car().IsNil()
	err := os.Chdir(dir)
	if err != nil && !noerror {
		msg.Bail("chdir:", err)
	}
	return lob.Bool2Ob(err == nil)
}

/// name directory
/// impl directory
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv filelist
/// docs
///   Return a list of path names matching PATHSPEC.
///   PATHSPEC may contain wildcards. If not, the resulting list has
///   either one or zero elements.
/// endd
func directory(arglist lob.Object, nargs int) lob.Object {
	pattern := arglist.Car().ValueString(nil)
	result, err := filepath.Glob(pattern)
	if err != nil {
		msg.Bail("glob:", err)
	}
	return lob.StringSlice2list(result)
}

// pathname: the path name of the file
// from: where this has been called
// may_fail: return nil on error, don't bail out
func get_fileinfo(pathname, from string, may_fail bool) *os.FileInfo {
	fi, err := os.Stat(pathname)
	if err != nil {
		if may_fail {
			return nil
		}
		msg.Bailf("%s: error getting attributes of \"%s\": %s",
			from, pathname, err.Error())
	}
	return &fi
}

// get the pathname string from either a pathname string or a port
func get_pathname(patharg lob.Object, from string) string {
	switch pathob := patharg.(type) {
	case *lob.Port:
		if pathob.PType() == lob.PTypeFILE {
			return pathob.Name()
		} else {
			msg.Bailf("%s: port argument %v is not a file port",
				from, pathob)
		}
	default:
		return pathob.ValueString(nil)
	}
	return ""
}

/// name file-author
/// impl file_author
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv author
/// docs
///   Return the author of the specified file.
///   The pathspec is either a pathname string or an open stream.
/// endd
func file_author(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(arglist.Car(), "file-author")
	fi := get_fileinfo(pathname, "file-author", false)
	uid := (*fi).Sys().(*syscall.Stat_t).Uid
	uidS := fmt.Sprintf("%d", uid)
	user, err := user.LookupId(uidS)
	if err != nil {
		msg.Bailf("file-author: error lookuing up uid %s: %s",
			uidS, err.Error())
	}
	return lob.NewString(user.Username)
}

/// name file-length
/// impl file_length
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv length
/// docs
///   Return the length of the specified file.
///   The pathspec is either a pathname string or an open stream.
/// endd
func file_length(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(arglist.Car(), "file-length")
	fi := get_fileinfo(pathname, "file-length", false)
	return lob.NewInt64Number((*fi).Size())
}

/// name file-namestring
/// impl file_namestring
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv basename
/// docs
///   Return the file namestring a.k.a. basename of PATHSPEC.
///   The pathspec is either a pathname string or an open stream.
/// endd
func file_namestring(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(arglist.Car(), "file-namestring")
	elems := strings.Split(pathname, "/")
	return lob.NewString(elems[len(elems)-1])
}

/// name directory-namestring
/// impl directory_namestring
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv dirname
/// docs
///   Return the directory name portion of PATHSPEC.
///   The pathspec is either a pathname string or an open stream.
/// endd
func directory_namestring(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(arglist.Car(), "directory-namestring")
	elems := strings.Split(pathname, "/")
	elems_len := len(elems)
	is_absolute := len(pathname) > 0 && pathname[0] == '/'
	var result string
	switch elems_len {
	case 0, 1: // cannot be absolute
		result = ""
	case 2:
		if is_absolute {
			result = "/"
		} else {
			result = elems[0]
		}
	default:
		result = strings.Join(elems[0:elems_len-1], "/")
	}
	return lob.NewString(result)
}

/// name file-position
/// impl file_position
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv
/// docs
///   Return the position of the specified file.
///   The pathspec is either a pathname string or an open stream. If the
///   position cannot be determined (because the argument is not an open
///   file port), return nil.
/// endd
func file_position(arglist lob.Object, nargs int) lob.Object {
	switch pathob := arglist.Car().(type) {
	case *lob.Port:
		value := pathob.Position()
		if value < 0 {
			return lob.Nil
		} else {
			return lob.NewInt64Number(value)
		}
	default:
		return lob.Nil
	}
}

/// name file-write-date
/// impl file_write_date
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv author
/// docs
///   Return the last-modified date of the specified file.
///   The pathspec is either a pathname string or an open stream.
///   The result has, in theory, nanosecond resolution, but seems
///   to be actually limited to whole seconds.
/// endd
func file_write_date(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(arglist.Car(), "file-write-date")
	fi := get_fileinfo(pathname, "file-write-date", false)
	return lob.NewNumber(time2universal_ns((*fi).ModTime()))
}

/// name probe-file
/// impl probe_file
/// mina 1
/// maxa 1
/// spec false
/// args PATHSPEC
/// retv pathname-or-nil
/// docs
///   Return the pathname of the file, if it exists, otherwise nil.
///   The pathspec is either a pathname string or an open stream.
/// endd
func probe_file(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(arglist.Car(), "probe-file")
	if get_fileinfo(pathname, "probe-file", true) == nil {
		return lob.Nil
	} else {
		return lob.NewString(pathname)
	}
}

/// name delete-file
/// impl delete_file
/// mina 1
/// maxa 1
/// spec false
/// args filespec
/// retv t
/// docs
///   Delete the file denoted by FILESPEC and return t on success.
///   On failure, signal an error.
/// endd
func delete_file(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(lob.Pop(&arglist), "delete-file: filespec")
	err := os.Remove(pathname)
	if err != nil {
		msg.Bailf("delete-file: cannot delete '%s': %v", pathname, err)
	}
	return lob.T
}

/// name rename-file
/// impl rename_file
/// mina 2
/// maxa 2
/// spec false
/// args filespec newname
/// retv newname
/// docs
///   Rename the file denoted by FILESPEC to NEWNAME and return the new name.
///   On failure, signal an error.
/// endd
func rename_file(arglist lob.Object, nargs int) lob.Object {
	pathname := get_pathname(lob.Pop(&arglist), "rename-file: filespec")
	newnameob := lob.Pop(&arglist)
	newname := stringArg(newnameob, "rename-file: newname")
	err := os.Rename(pathname, newname)
	if err != nil {
		msg.Bailf("rename-file: %v", err)
	}
	return newnameob
}

// EOF
