// Performance data helper functions
package hlp

import (
	"fmt"
	"time"

	"../lob"
)

type Perfdata struct {
	evals   int64
	pairs   int64
	times   time.Time
	seconds float64
}

func StartPerfdata() *Perfdata {
	return &Perfdata{
		evals:   lob.EvalCount(),
		pairs:   int64(lob.NPairs()),
		times:   time.Now(),
		seconds: 0,
	}
}

func (pd *Perfdata) Stop() *Perfdata {
	pd.evals = lob.EvalCount() - pd.evals
	pd.pairs = int64(lob.NPairs()) - pd.pairs
	pd.seconds = float64(time.Now().Sub(pd.times)) / float64(time.Second)
	return pd
}

func (pd *Perfdata) List() lob.Object {
	return lob.ListOf(
		lob.Intern("pairs"), lob.NewNumber(float64(pd.pairs)),
		lob.Intern("evals"), lob.NewNumber(float64(pd.evals)),
		lob.Intern("secs"), lob.NewNumber(pd.seconds),
	)
}

func (pd *Perfdata) Values() (evals int64, pairs int64, seconds float64) {
	return pd.evals, pd.pairs, pd.seconds
}

func (pd *Perfdata) Evals() int64 {
	return pd.evals
}

func (pd *Perfdata) Pairs() int64 {
	return pd.pairs
}

func (pd *Perfdata) Seconds() float64 {
	return pd.seconds
}

func (pd *Perfdata) String() string {
	return fmt.Sprintf("%d pairs %d evals in %d ms, %.0f evals/s",
		pd.pairs, pd.evals, int(pd.seconds*1000),
		float64(pd.evals)/pd.seconds)
}

func Init_perfdata() {
	//fmt.Println("hlp/perfdata.init")
}
