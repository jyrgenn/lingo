// General helper functions on object level
package hlp

import (
	"../lob"
	"../msg"
)

func Append(list1, list2 lob.Object) lob.Object {
	if list1.IsNil() {
		return list2
	}
	if list2.IsNil() {
		return list1
	}
	lp := lob.NewListPusher()
	for lob.IsPair(list1) {
		lp.Push(lob.Pop(&list1))
	}
	lp.AddEnd(list2)
	return lp.List()
}

func IsProperList(list lob.Object) (bool, int) {
	nargs := 0
	for lob.IsPair(list) {
		list = list.Cdr()
		nargs++
	}
	return list.IsNil(), nargs
}

func EvalProgn(list lob.Object) lob.Object {
	result := lob.Object(lob.Nil)
	for lob.IsPair(list) {
		first := lob.Pop(&list)
		result = lob.Eval(first, nil)
	}
	if !list.IsNil() {
		msg.Bail("progn: not a proper list:", list)
	}
	return result
}

func Init_helpers() {
	//fmt.Println("hlp/helpers.init")
}
