package call

// A Callable is an object that may be called as a function. This includes
// functions in the stricter sense (builtins and lambdas), but also vectors and
// tables, which can be called with one argument for a "get" operation (and this
// way are functions in the broader mathematical sense) and with two arguments
// for a "put" operation:
//
// > (vector index) => value
// > (vector index new-value) => new-value
// > (table key) => value
// > (table key new-value) => new-value
//
// Also, there are these forms:
// > (regexp string) => list-of-matches
// > (function &rest args) => value
//
// This idea has been stolen from Arc Lisp. More object types may emerge as
// Callables in the future.

import (
	"../lob"
	"../msg"
)

type Callable interface {
	lob.Object
	Call(nargs int, args lob.Object) lob.Object
	Deftype() string
	Synopsis() string
	Docstring() string
	IsSpecial() bool
}

// Evaluate candidate to a Callable and return it. Iff that fails and mayFail is
// false, fail; otherwise, return nil. candidate may directly be a Callable, a
// symbol that has a Callable in the function cell, or a form to be evaluated to
// get the Callable. There is one level of evaluation to get at the callable;
// more seems to be unnecessary.
//
func EvalCallable(candidate lob.Object, mayFail bool) Callable {
	// This function is not pretty, but it is in the hottest loop of the
	// whole Eval, and the nested type-switch form has proven to be a few
	// percent faster than replacing some of that repetitive code with a
	// function call. A shame Go doesn't have macros.

	switch callee := candidate.(type) {
	case Callable:
		return callee
	case *lob.Symbol:
		function := callee.Function()
		if function == nil {
			function = callee.Value()
		}
		switch callee := function.(type) {
		case Callable:
			return callee
		}
		msg.Bailf("no function `%s'", callee)
	default:
		candidate = lob.Eval(callee, nil)
		switch callee := candidate.(type) {
		case Callable:
			return callee
		}
		if !mayFail {
			msg.Bail("not a function: ", callee)
		}
	}
	return nil
}
