# Makefile for lingo

# you may want to change this
INSTALLBASE = /opt/w21
INSTALLBIN = $(INSTALLBASE)/bin

GOSOURCES = *.go */*.go
BUILTINS = fun/builtins-*.go
PRELOAD_LISP = pre/*.lisp
PRELOAD_SRC = $(PRELOAD_LISP) pre/*.go.*
GO_TEST_MODULES = lob olu
REGISTER_BUILTINS = fun/Register_Builtins.go
GO = GO111MODULE=auto go

# remote repositories, sorted reverse so origin is in front
REMOTES = $$(git remote | sort -r)

# default rule: build only (DOCSTRINGS.txt depends on lingo)
build: etags DOCSTRINGS.txt

# everything including installation and git push
all: clean format build test benchmark install push module-deps.dot

# get newest and install
update: pull all
pull:
	git pull

# debug
vars:
	@echo INSTALLBASE=$(INSTALLBASE)
	@echo INSTALLBIN=$(INSTALLBIN)
	@echo GOSOURCES=$(GOSOURCES)
	@echo BUILTINS=$(BUILTINS)
	@echo PRELOAD_SRC=$(PRELOAD_SRC)
	@echo PRELOAD_LISP=$(PRELOAD_LISP)

# format the source code
format: dependencies
	#goimports -w . # destroys my structured comments

# check for some kinds of errors
checksrc:
	$(GO) vet .

# etags works with Go, too!
etags:
	./scripts/etags-or-not.sh $(GOSOURCES) \
            $$(find . -type f \( -name \*.l -o -name \*.lisp \))

dependencies:
	$(GO) install golang.org/x/tools/cmd/goimports
	$(GO) install github.com/chzyer/readline


# the actual build rule
lingo: dependencies gen/lisp-preload_.go buildtag $(REGISTER_BUILTINS) checksrc
	$(GO) build $(BUILDFLAGS)
	./lingo -e '(exit)'            # check preload code

gen:
	mkdir -p gen

# lisp-preload depends on some lisp files
gen/lisp-preload_.go: Makefile $(PRELOAD_SRC) scripts/Generate-preload.sh gen
	cat pre/pre.go.head > $@
	scripts/Generate-preload.sh $(PRELOAD_LISP) >> $@
	cat pre/pre.go.foot >> $@

# the build tag (informative only)
buildtag: gen
	scripts/buildtag.sh > gen/buildtag_.go

# builtin function registration
$(REGISTER_BUILTINS): $(BUILTINS) Makefile \
		scripts/Generate-registration.sh \
		scripts/Generate-registration.lisp \
		fun/register.go.head fun/register.go.foot
	scripts/Generate-registration.sh $(BUILTINS) > $(REGISTER_BUILTINS)

# collection of all function+macro doc strings
DOCSTRINGS.txt: lingo Makefile
	printf "; %s\n" "$$(./lingo -V)" > DOCSTRINGS.txt
	./lingo -q l/alldocs.lisp >> DOCSTRINGS.txt

# run regression tests, (a) all, (b) just one to make sure the
# argument to run-tests works
test: gen
	for mod in $(GO_TEST_MODULES); do $(GO) test ./$$mod; done
	./run-tests.lisp 043
	./run-tests.lisp $(VERBOSE)
test-v:
	$(MAKE) test VERBOSE=-v

# run a simple benchmark (number factorisation in Lisp)
FLENGTH = 9
FCODE = '(factor (read (string 1 (make-string $(FLENGTH) 0) 1)))'
benchmark: lingo
	@echo \; run factor with 1 . 0 x $(FLENGTH) . 1
	./lingo -v -l l/factor -e $(FCODE)
benchmark10:
	make benchmark FLENGTH=10


# package dependency graph; cat be viewed with graphviz
module-deps.dot: $(GOSOURCES) scripts/find-deps.sh
	sh scripts/find-deps.sh > module-deps.dot

# install the lingo binary and the lic program
install: lingo test
	install -c lingo l/lic $${LINGO_INSTALLBIN-$(INSTALLBIN)}
	install -d $(INSTALLBASE)/lib/lingo/l
	install -c l/*.lisp $(INSTALLBASE)/lib/lingo/l


# clean build products
clean:
	find . -name \*~ -exec rm {} +
	rm -f lingo.prof test.log lingo DOCSTRINGS.txt *.log module-deps.dot
	rm -rf gen $(REGISTER_BUILTINS) TAGS

# push to the git repositories (you may be forbidden to do that)
push:
	for r in $(REMOTES); do echo $$r:; git push --all $$r; git push --tags $$r; done
