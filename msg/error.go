// Error handling, objectless
package msg

import (
	"errors"
	"fmt"
	"runtime/debug"
	"strings"

	"../dep"
)

var InErrset = false
var ExitOnError = false

func maybeStackTrace() {
	if dep.GetStacktrace() {
		println(ERROR, string(debug.Stack()))
	}
}

// print an error
func PrintError(arg ...interface{}) {
	maybeStackTrace()
	println(ERROR, arg...)
}

// print an error with format string
func PrintErrorf(format string, arg ...interface{}) {
	maybeStackTrace()
	println(ERROR, fmt.Sprintf(format, arg...))
}

func handle(err error) {
	if dep.SetSysLastError != nil { // it is during unit testing
		dep.SetSysLastError(err)
	}
	// when do we want to drop into an error REPL:
	//   - we are running lingo interactively
	//   - *and* exit on error is not set
	//   - *and* we are not in an errset context,
	//           *or* the REPL is explicitly enabled
	// fmt.Printf("dep.RunInteractively: %v; ExitOnError: %v; "+
	// 	"InErrset: %v; dep.GetErrordrop(): %v\n",
	// 	dep.RunInteractively, ExitOnError, InErrset, dep.GetErrordrop())
	if dep.RunInteractively && !ExitOnError &&
		(!InErrset || dep.GetErrordrop()) {

		dep.PrintStack()
		fmt.Print("Enter recursive REPL:\n")
		PrintError(err)
		dep.RecurseRepl()
	}
	panic(err)
}

func Bailf(format string, arg ...interface{}) {
	handle(fmt.Errorf(format, arg...))
}

func Bail(args ...interface{}) {
	handle(errors.New(strings.TrimSpace(fmt.Sprintln(args...))))
}

func Init_error() {
	//fmt.Println("msg/error.init")
}
