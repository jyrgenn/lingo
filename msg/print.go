// printing messages to the user (objectless)
package msg

import (
	"fmt"
	"os"
	"path"
	"runtime"

	"../dep"
)

const (
	ERROR = 0 + iota
	WARN
	INFO
	DEBUG
	TRACE
	INVALiD
)

var levelName = [...]string{"ERR", "; WARNING", ";", "DBG", "TRC"}

func CallInfo(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if ok {
		dir, base := path.Split(file)
		bdir := path.Base(dir)
		return fmt.Sprintf("%s/%s:%d", bdir, base[:len(base)-3], line)
	}
	return "x"
}

func println(level int, arg ...interface{}) {
	if level == ERROR ||
		(dep.GetVerbosity != nil && dep.GetVerbosity() >= level) {

		pargs := []interface{}{levelName[level]}
		if level >= DEBUG {
			pargs = append(pargs, CallInfo(3))
		}
		pargs = append(pargs, arg...)
		outport := os.Stdout
		if level == ERROR {
			outport = os.Stderr
		}
		fmt.Fprintln(outport, pargs...)
	}
}

func Warn(arg ...interface{}) {
	if dep.GetWarnerrors() {
		Bail(arg...)
	} else {
		println(WARN, arg...)
	}
}

func Warnf(format string, arg ...interface{}) {
	if dep.GetWarnerrors() {
		Bailf(format, arg...)
	} else {
		println(WARN, fmt.Sprintf(format, arg...))
	}
}

func Info(arg ...interface{}) {
	println(INFO, arg...)
}

func Infof(format string, arg ...interface{}) {
	println(INFO, fmt.Sprintf(format, arg...))
}

func Debug(arg ...interface{}) {
	println(DEBUG, arg...)
}

func Debugf(format string, arg ...interface{}) {
	println(DEBUG, fmt.Sprintf(format, arg...))
}

func Trace(arg ...interface{}) {
	println(TRACE, arg...)
}

func Tracef(format string, arg ...interface{}) {
	println(TRACE, fmt.Sprintf(format, arg...))
}

func Init_print() {
	//fmt.Println("msg/print.init")
}
