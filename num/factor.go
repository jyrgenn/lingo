// Numeric: factorisation function
package num

//import "fmt"

// a slice containing all primes calculated so far, in sequence
var primes []uint64 = []uint64{2, 3}

// add the next prime to the "primes" slice; this *must* be the next in the
// sequence
func addNextPrime(n uint64) {
	primes = append(primes, n)
}

// return the highest prime we have so far
func highestPrime() uint64 {
	return primes[len(primes)-1]
}

// return the integer square root of the argument
func Isqrt(op uint64) uint64 {
	res := uint64(0)
	one := uint64(1 << 62)

	for one > op {
		one >>= 2
	}
	for one != 0 {
		if op >= res+one {
			op -= res + one
			res += one << 1 // <-- faster than 2 * one
		}
		res >>= 1
		one >>= 2
	}
	return res
}

// return true iff we have a divisor for n in our sequence of primes
func haveDivisor(n uint64) bool {
	limit := Isqrt(n)
	for p := 0; p < len(primes); p++ {
		prime := primes[p]
		if prime > limit {
			break
		}
		if n%primes[p] == 0 {
			return true
		}
	}
	return false
}

// grow the "primes" slice by the next prime number, provided 3 is already
// present
func growPrimes() {
	candidate := highestPrime() + 2
	for haveDivisor(candidate) {
		candidate += 2
	}
	addNextPrime(candidate)
}

// return a function that, on subsequent calls, returns prime numbers in
// sequence.
func makePrimeSeq() func() uint64 {
	nextIndex := 0
	return func() uint64 {
		for len(primes) <= nextIndex {
			growPrimes()
		}
		result := primes[nextIndex]
		nextIndex++
		return result
	}
}

// print the prime factors of the argument, in sequence
func Factor(n uint64) []uint64 {
	nextPrime := makePrimeSeq()
	result := []uint64{}
	for {
		p := nextPrime()
		limit := Isqrt(n)
		if p > limit {
			break
		}
		for n%p == 0 && n > 1 {
			//fmt.Printf(" %d", p)
			result = append(result, p)
			n /= p
		}
		if n == 1 { // quick and dirty optimization to save
			break // the calculation of the next prime in
		} // a few cases
	}
	if n > 1 {
		//fmt.Printf(" %d", n)
		result = append(result, n)
	}
	//fmt.Println()
	return result
}

func IsPrime(n uint64) bool {
	if n <= 1 {
		return false
	}
	nextPrime := makePrimeSeq()
	limit := Isqrt(n)
	for {
		p := nextPrime()
		if p > limit {
			break
		}
		if n%p == 0 {
			return false
		}
	}
	return true
}

func NextPrime(n uint64) uint64 {
	if n < 2 {
		return 2
	}
	if n == 2 {
		return 3
	}
	n += 1 + n%2
	for ; ; n += 2 {
		if IsPrime(n) {
			return n
		}
	}
}

func Init_factor() {
	//fmt.Println("num/factor.init")
}

// EOF
