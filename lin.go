package main

// lingo main/startup module; initialisation, command line argument handling,
// start a REPL or load an application

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime/pprof"

	"./dep"
	"./eval"
	"./fun"
	"./gen"
	"./hlp"
	"./lio"
	"./lob"
	"./msg"
	"./num"
	"./olu"
	"./sys"
)

var opt struct {
	loadfiles []string
	evalexpr  []string
	// verbosity values: 0 errors; 1 interactive; 2 debug; 3 trace
	// default verbosity level is 1 for interactive, 0 for non-int.
	verbosity   int
	profiling   bool
	errordrop   bool
	interactive bool
	noauto      bool
	nopreload   bool
	stacktraces bool
	norecover   bool
	warnerrors  bool
}

func usage(message string) {
	fmt.Fprintf(os.Stderr,
		`%susage: %s [-ehnspqvEV] [-l loadfile] [-e expr] [file] [arg1 ...]
 -e: evaluate expression
 -h: show this help text
 -i: run interactively despite all evidence to the contrary 
 -l: load file before starting repl
 -n: do not load autoload file
 -N: do lot load preload code
 -p: write cpu profiling data
 -q: be quiet
 -R: don't recover() from a panic() to enable more Go stack traces
 -s: show Go stack traces on error
 -v: increase verbosity
 -x: exit on error
 -E: drop into error repl even inside of errset
 -V: show build version and exit
 -W: let warnings be errors
`, message, os.Args[0])
	os.Exit(2)
}

func parseOptions(argv []string) []string {
	opt.verbosity = 2
	for len(argv) > 0 && len(argv[0]) > 0 &&
		argv[0][0] == '-' && argv[0] != "--" {

		for _, c := range argv[0][1:] {
			switch c {
			case 'e':
				if len(argv) <= 1 {
					usage("eval expression missing\n")
				}
				opt.evalexpr = append(opt.evalexpr, argv[1])
				argv = argv[1:]
				dep.RunInteractively = false
			case 'l':
				if len(argv) > 1 {
					opt.loadfiles =
						append(opt.loadfiles, argv[1])
					argv = argv[1:]
				} else {
					usage("load file name missing\n")
				}
			case 'h':
				usage("")
			case 'i':
				opt.interactive = true
			case 'n':
				opt.noauto = true
			case 'N':
				opt.nopreload = true
			case 'p':
				opt.profiling = true
			case 'q':
				opt.verbosity = 0
			case 'R':
				opt.norecover = true
			case 's':
				opt.stacktraces = true
			case 'v':
				opt.verbosity++
			case 'x':
				msg.ExitOnError = true
			case 'E':
				opt.errordrop = true
			case 'V':
				fmt.Printf("%s %s\n", os.Args[0], gen.Build())
				os.Exit(0)
			case 'W':
				opt.warnerrors = true
			default:
				usage(fmt.Sprintf("wrong option char %c\n", c))
			}
		}
		argv = argv[1:]
	}
	if len(argv) > 0 && argv[0] == "--" {
		argv = argv[1:]
	}
	return argv
}

// return ok status, but exit on !found
func loadOrExit(fname string) bool {
	ok, found := lio.Load(fname, lob.Obmap{})
	if !found {
		msg.PrintErrorf("failed to load \"%s\" on startup", fname)
		os.Exit(4)
	}
	return ok
}

func init_modules() {
	gen.Init_generated()

	// dep and olu have no interal dependencies
	dep.Init_depend()
	olu.Init_OLU()

	//num
	num.Init_factor()

	// msg; needs dep, but not for init
	msg.Init_print()
	msg.Init_error()

	// lob, needs msg dep
	lob.Init_environment()
	lob.Init_stdsyms() // needs environment, needed by others
	lob.Init_char()
	lob.Init_comparable()
	lob.Init_list()
	lob.Init_netaddr()
	lob.Init_number()
	lob.Init_object()
	lob.Init_pair()
	lob.Init_ports()
	lob.Init_regexp()
	lob.Init_sequence()
	lob.Init_string()
	lob.Init_symbol()
	lob.Init_table()
	lob.Init_traceable()
	lob.Init_vector()

	// lob, needs lob
	lob.Init_lob()

	// sys, needs lob msg dep
	sys.Init_props()
	sys.Init_signals()
	sys.Init_errors()
	sys.Init_os()

	// hlp, needs lob dep msg lob
	hlp.Init_perfdata()
	hlp.Init_helpers()

	// fun, needs sys hlp num lob msg dep lob
	fun.Init_funtype()
	fun.Init_call()
	fun.Init_bi_helpers()

	// eval, needs fun sys num hlp lob lob msg dep
	eval.Init_eval()

	// lio, needs all
	lio.Init_repl()
	lio.Init_reader()
	lio.Init_load()
}

func main() {
	loadAppl := "" // if non-empty, the application to load
	if opt.profiling {
		f, err := os.Create("lingo.prof")
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	init_modules()
	lob.ProgramName.SetValue(lob.NewString(os.Args[0]))
	args := parseOptions(os.Args[1:])
	versionString := fmt.Sprintf("%s %s", os.Args[0], gen.Build())
	lob.SysVersionString.DefVar(lob.NewString(versionString))

	if len(args) > 0 {
		loadAppl = args[0]
		// the slice will be empty if there are no more arguments
		cmdargs := make([]lob.Object, len(args[1:]))
		for i, value := range args[1:] {
			cmdargs[i] = lob.NewString(value)
		}
		lob.CmdArgs.DefVar(lob.Slice2list(cmdargs))
		dep.RunInteractively = false
		dep.ReadInteractively = false
	} else {
		lob.CmdArgs.DefVar(lob.Nil)
	}
	// default verbosity "quiet" when running non-interactively so
	// potentially unwanted lingo messages don't distract from a running
	// application (saw this unfavourably with lic)
	if !dep.RunInteractively && opt.verbosity > 0 {
		opt.verbosity--
	}
	sys.SetVerbosity(opt.verbosity)
	sys.SetStacktrace(opt.stacktraces)
	sys.SetErrordrop(opt.errordrop)
	sys.SetWarnerrors(opt.warnerrors)
	dep.NoRecover = opt.norecover

	msg.Info(versionString)
	if !opt.nopreload {
		name := "*preload-code*"
		_, ok := lio.LoadFromString(gen.Preload(), name)
		if !ok {
			msg.PrintErrorf("failed to load %s on startup", name)
			os.Exit(4)
		}
	}
	if !opt.noauto {
		lio.LoadIfExists(path.Join(os.Getenv("HOME"), "/.lingo.lisp"))
	}
	var err bool
	for _, fname := range opt.loadfiles {
		ok := loadOrExit(fname)
		if !ok {
			msg.PrintErrorf("exit due to error in load file")
			dep.Exit(5)
		}
	}
	if len(opt.evalexpr) > 0 {
		msg.ExitOnError = true
		for _, expr := range opt.evalexpr {
			// msg.Debug("evaluate expr:", expr)
			result, ok := lio.LoadFromString(expr, "*cmdline-eval*")
			if !ok {
				msg.PrintErrorf("command line eval failed: %s",
					expr)
				os.Exit(3)
			}
			fmt.Println(result.ValueString(nil))
		}
	}
	status := 0
	if loadAppl != "" {
		lob.ProgramName.SetValue(lob.NewString(args[0]))
		ok := loadOrExit(args[0])
		if !ok {
			status = 1
		}
	}
	if opt.interactive {
		dep.RunInteractively = true
	}
	if dep.RunInteractively {
		_, err = lio.Repl(lob.Stdin(), true)
	}
	if err {
		status = 1
	}
	dep.Exit(status)
}

// EOF
