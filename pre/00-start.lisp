;;; Lisp code integrated into the interpreter and evaluated on startup

(defvar sys:types
  '(char
    environment
    function
    macro
    netaddr
    number
    cons
    port
    regexp
    string
    struct
    symbol
    table
    vector)
  "List of types known to the system.")
(putprop 'sys:types t 'sys:system)

(defun sys:lingo-version (&rest items)
  "Return the components of the sys:lingo-version string variable as a list.
With ITEMS arguments (one or more of call-name, version-tag, build-date,
builder, go-version, build-architecture), return the specified component(s)."
  (let ((components (split-string sys:lingo-version)))
    (if items
        (let* ((names '(call-name version-tag build-date builder go-version
                                  build-architecture))
               (comp-table (apply #'make-table
                                  (mapcar (lambda (key value) (cons key value))
                                          names components))))
          (map (lambda (item) (comp-table item)) items))
      components)))
