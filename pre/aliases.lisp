
(fset 'string-concat #'string)
(fset 'not #'null)
(fset 'map #'mapcar)
(fset 'princ-to-string #'princs)
(fset 'namestring 'normalize-pathname)

(fset 'char-code #'char-int)
(fset 'char= #'=)
(fset 'char/= #'/=)
(fset 'char< #'<)
(fset 'char> #'>)
(fset 'char<= #'<=)
(fset 'char>= #'>=)

;;;EOF
