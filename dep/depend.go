// avoid circular dependencies by placing stuff here

package dep

import (
	"io"
	"os"
)

var RunInteractively bool
var ReadInteractively bool

var GetVerbosity func() int
var GetStacktrace func() bool
var GetErrordrop func() bool
var GetWarnerrors func() bool
var NLambdas func() int
var NBuiltins func() int
var RecurseRepl func() bool
var PrintStack func()
var IsBuiltin func(interface{}) bool
var SetSysLastError func(error)

var StdinReader func() io.Reader
var StdoutWriter func() io.Writer
var StderrWriter func() io.Writer

var NoRecover bool

var LingoHistoryFileName string

var atExitFuncs []func() = []func(){}

func AtExit(f func()) {
	atExitFuncs = append(atExitFuncs, f)
}

// Exit the program, but run the atExitFuncs before. We need to run them in
// reverse defined order, so we can close the first liner (the one that actually
// changed the terminal settings and thus has remembered the original terminal
// settings) last.
//
func Exit(status int) {
	for i := len(atExitFuncs) - 1; i >= 0; i-- {
		atExitFuncs[i]()
	}
	os.Exit(status)
}

func Init_depend() {
	// fmt.Println("dep/depend.init")
	RunInteractively = true
	ReadInteractively = true
	LingoHistoryFileName = os.ExpandEnv("$HOME/.lingo-history")
}
