#!/bin/sh

if scripts/have-program.sh lingo; then
    echo have lingo, so use it 1>&2
    exec lingo scripts/Generate-preload.lisp "$@"
fi

echo no lingo found, using the shell script 1>&2
printf '"'
for file in "$@"; do
    printf ';;; lisp preload: %s\\n' $file
    sed -e 's/\\/\\\\/'g -e 's/"/\\"/g' -e 's/$/\\n/g' < $file | tr -d '\n'
done
echo '"'
