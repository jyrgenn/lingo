#!/bin/sh

if scripts/have-program.sh etags; then
    etags "$@"
else
    echo "no etags found in PATH, no TAGS created"
fi
