#!/bin/sh
# generate the builtin descriptions needed to register the builtin functions

# build with lingo if present and it works
if scripts/have-program.sh lingo; then
    echo have lingo, so use it 1>&2
    exec lingo scripts/Generate-registration.lisp "$@"
fi

echo no lingo found, using the shell script 1>&2
# first, start of the file
sed s,{generator},$0, fun/register.go.head

# now extract the function registration descriptions from builtins-*.go

for fname in "$@"; do
    while read comment word rest; do
        case "$comment $word" in
            "/// name") printf "\t&builtinDescriptor{\n\t\tname: \"%s\",\n" "$rest";;
            "/// impl") printf "\t\tfunction: %s,\n" "$rest" ;;
            "/// mina") printf "\t\tminargs: %s,\n" "$rest" ;;
            "/// maxa") printf "\t\tmaxargs: %s,\n" "$rest" ;;
            "/// spec") printf "\t\tisspecial: %s,\n" "$rest" ;;
            "/// args") printf "\t\tdoc: \`%s) =>" "$rest" ;;
            "/// retv") printf " %s\n" "$rest" ;;
            "/// docs") while read line; do
                            case "$line" in
                                "/// endd") break;;
                                *) echo "$line";;
                            esac
                        done | sed -e 's,///   ,,' -e 's,/// *',,
                        printf "\`,\n\t\tsource: \"%s\",\n\t},\n" $fname ;;
        esac
    done < $fname
done

# last, register_.go end
cat fun/register.go.foot

# EOF
