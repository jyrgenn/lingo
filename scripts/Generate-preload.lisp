#!/usr/bin/env lingo

(princ "\"")
(dolist (file-name sys:args)
  (with-open-file (in file-name)
    (format t ";;; lisp preload: %s\\n" file-name)
    (let (line)
      (while (setf line (read-line in nil))
        (setf line (regex-replace #/\\/ line "\\\\"))
        (setf line (regex-replace #/"/ line "\\\""))
        (setf line (regex-replace #/\n/ line "\\n"))
        (princ line)))))
(princ "\"\n")
