#!/bin/sh
# generate the Go source file containing the build tag

COMMIT=`git describe --dirty`
build=`date "+$COMMIT %Y%m%d:%H%M%S $USER@$HOST"`
version=`go version | sed 's/^go version //'`

cat <<EOB
// Build tag -- generated file, do not edit
package gen;

func Build() string {
        return "$build $version"
}
EOB

