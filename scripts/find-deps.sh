#!/bin/sh
# generate a dependency graph of the lingo 
export GO111MODULE=auto

echo "digraph {"

go list -f '{{ join .Imports "\n"}}' ./lin.go |
    sed -n s,_$PWD/,./,p |
    while read pkg; do
        echo "    \"main\" -> \"$pkg\""
        for dep in `go list -f '{{ join .Imports "\n"}}' $pkg`; do
            echo "    \"$pkg\" -> \"$dep\""
        done | sed -n s,_.*/,,p
    done | sed s,\\./,,

echo "}"
