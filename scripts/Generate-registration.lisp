#!/usr/bin/env lingo
;;; generate the builtin descriptions needed to register the builtin functions

;; print a file with "{generator}" replaced by $0
(defun cat-file-with-replace (fname)
  (with-open-file (in fname)
    (let (line)
      (while (setf line (read-line in nil))
        (setf line (regex-replace #r/{generator}/ line sys:program-name))
        (princ line)))))

;; first, start of registration file
(cat-file-with-replace "fun/register.go.head")

;; these are descriptors for the fields of the builtin definition;
;; they contain the keyword, a default value (or nil, where no default
;; is possible), and a format string with which to print the value
(defvar field-info '(("name" nil "\t&builtinDescriptor{\n\t\tname: \"%s\",\n")
                     ("impl" nil "\t\tfunction: %s,\n")
                     ("mina" "0" "\t\tminargs: %s,\n")
                     ("maxa" "-1" "\t\tmaxargs: %s,\n")
                     ("spec" "false" "\t\tisspecial: %s,\n")
                     ("args" "" "\t\tdoc: \`%s) =>")
                     ("retv" "(unspecified)" " %s\n")
                     ("docs" nil "%s\`,\n")
                     ("source" nil "\t\tsource: \"%s\",\n\t},\n")))

;; print out an entry; the entry's fields are printed in the order
;; according to `field-info' above, and each entry's field's value (or
;; the default, where not present) is printed with the respective
;; format string
(defun printout (entry)                 ;a table fieldname => contents
  (dolist (desc field-info)
    (let (((field nil format-string) desc))
      (format t format-string
              (or (entry field)
                  (cadr desc)
                  (error "missing value for field %s of %s:%s"
                         field (entry "source") (entry "field")))))))

;; now extract the function registration descriptions from builtins-*.go

(dolist (fname sys:args)
  (with-open-file (in fname)
    (let (cur line)                     ;current entry, current line
      (while (setf line (read-line in nil))
        (let (((nil word rest)
               (#r{^/// (name|impl|mina|maxa|spec|args|retv|docs) ?(.*)} line)))
          (when word
            (cond ((eq word "name")
                   (when cur
                     (printout cur))
                   (setf cur (make-table (cons "source" fname)
                                         (cons "name" rest))))
                  ((eq word "docs")
                   (catch 'endd
                     (cur "docs" "")
                     (while (setf line (read-line in nil))
                       (if (#r{^/// endd} line)
                           (throw 'endd nil)
                         (setf line (regex-replace #r{^///   } line ""))
                         (setf line (regex-replace #r{^/// *} line ""))
                         (cur "docs" (string (cur "docs") line))))))
                  (t (cur word rest))))))
      (printout cur))))

;; last, end of registration sourcex
(cat-file-with-replace "fun/register.go.foot")

;; EOF
