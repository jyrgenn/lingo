#!/bin/sh
# return true iff we have a program of the specified name in PATH

program="${1?usage: $0 program}"
oldIFS="$IFS"
IFS=:
for dir in $PATH; do
    if [ -x $dir/$program ]; then
        IFS="$oldIFS"
        exit 0
    fi
done
# not found
exit 1
