// the eval function and helpers

package eval

import (
	"bytes"
	"fmt"

	"../call"
	"../dep"
	"../hlp"
	"../lob"
	"../msg"
	"../sys"
)

var evalCount int64 = 0

var evalStack = []lob.Object{}

func EvalCount() int64 {
	return evalCount
}

func push(ob lob.Object, env *lob.Environment) {
	//fmt.Printf("s: %d, nextIndex %d\n", len(evalStack), nextIndex);
	evalStack = append(evalStack, ob, env)
}

func drop() {
	evalStack = evalStack[:len(evalStack)-2]
}

func EvalLevel() int {
	return len(evalStack) / 2
}

// Print the the environment and its variable bindings for the stack trace.
func printEnv(env *lob.Environment) string {
	buf := bytes.NewBufferString(env.ValueString(nil))
	buf.WriteRune('{')
	if env.Level() == 0 {
		buf.WriteString("...")
	} else {
		first := true
		for key, value := range env.Varmap() {
			if !first {
				buf.WriteString("; ")
			}
			buf.WriteString(key.ValueString(nil))
			buf.WriteRune('=')
			buf.WriteString(value.ValueString(nil))
			first = false
		}
	}
	buf.WriteRune('}')
	return buf.String()
}

// Print the eval stack with expression to be evaluated and the particular
// environment for each frame.
func PrintStack() {
	for i := 0; i < len(evalStack); i += 2 {
		// take care to indent the environment line as appropriate
		levelStr := fmt.Sprintf(":%d ", i/2)
		fmt.Printf("%s%s\n%*s%s\n", levelStr, evalStack[i],
			len(levelStr), "",
			printEnv(evalStack[i+1].(*lob.Environment)))
	}
}

// map fun over list; return the resulting new list, the number of list members,
// and if the list was a proper list
func mapcar_c_prop(fun func(lob.Object) lob.Object, list lob.Object) (
	lob.Object, int, bool) {

	nargs := 0
	lp := lob.NewListPusher()
	for nargs = 0; lob.IsPair(list); nargs++ {
		first := lob.Pop(&list)
		lp.Push(fun(first))
	}
	return lp.List(), nargs, list.IsNil()
}

// eval in current environment
func EvalCurEnv(expr lob.Object) lob.Object {
	return Eval(expr, nil)
}

func Eval(expr lob.Object, env *lob.Environment) lob.Object {
	if sys.HadSignal() {
		msg.Bail("interrupt")
	}
	if env != nil {
		savedEnv := lob.EnterEnvironment(env)
		defer savedEnv.BackTo()
	}
	push(expr, lob.Env)
	defer drop()

	evalCount++
	// Enabling this, think of the equivalent below for the result
	// msg.Infof("eval%*s%s [%T]", (len(evalStack)), "", expr, expr)
	var result lob.Object
	switch ob := expr.(type) {
	case *lob.Symbol:
		result = ob.NeedValue()
	case *lob.Pair:
		arglist := ob.Cdr()
		callee := call.EvalCallable(ob.Car(), false)
		nargs := 0
		proper := false
		if callee.IsSpecial() {
			proper, nargs = hlp.IsProperList(arglist)
		} else {
			arglist, nargs, proper =
				mapcar_c_prop(EvalCurEnv, arglist)
		}
		if !proper {
			msg.Bailf("argument list is not a proper list: %v", ob)
		}
		result = callee.Call(nargs, arglist)
	default:
		result = expr
	}
	// msg.Infof("eval%*s=> %v [%T]", (len(evalStack)), "", result, result)
	return result
}

func Init_eval() {
	//fmt.Println("eval/eval.init")
	lob.Eval = Eval
	lob.EvalCurEnv = EvalCurEnv
	lob.EvalLevel = EvalLevel
	lob.EvalCount = EvalCount
	dep.PrintStack = PrintStack
}
