// Lingo system: set last error function
package sys

import (
	"../dep"
	"../lob"
)

func SetSysLastError(err error) {
	lob.SysLastError.SetValue(lob.NewString(err.Error()))
}

func Init_errors() {
	dep.SetSysLastError = SetSysLastError
}
