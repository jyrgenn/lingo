// Some OS interface stuff
package sys

// OS-related things

import "../lob"

const (
	internal_time_units_per_second = int64(1000000000)
)

var ()

func Init_os() {
	lob.InternSys("internal-time-units-per-second",
		lob.NewInt64Number(internal_time_units_per_second)).
		MakeImmutable()
}

// EOF
