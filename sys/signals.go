// System: signal handling
// I wish I could do more here.
package sys

import (
	"os"
	"os/signal"
)

var sigchan chan os.Signal

func MakeSigChannel(sig os.Signal) chan os.Signal {
	var c chan os.Signal = make(chan os.Signal, 1)
	signal.Notify(c, sig)
	return c
}

func HadSignal() bool {
	select {
	case <-sigchan:
		return true
	default:
	}
	return false
}

func Init_signals() {
	//fmt.Println("sys/signals.init")
	sigchan = MakeSigChannel(os.Interrupt)
}
