// System properties
package sys

import (
	"../dep"
	"../lob"
)

var allprops []*lob.Symbol

func SetErrordrop(value bool) {
	lob.SysErrordrop.SetValue(lob.Bool2Ob(value))
}

func GetErrordrop() bool {
	return lob.SysErrordrop.Value() != lob.Nil &&
		lob.SysErrordrop.Value() != nil
}

func SetWarnerrors(value bool) {
	lob.SysWarnerrors.SetValue(lob.Bool2Ob(value))
}

func GetWarnerrors() bool {
	return lob.SysWarnerrors.Value() != lob.Nil &&
		lob.SysWarnerrors.Value() != nil
}

func GetVerbosity() int {
	value := lob.SysVerbosity.Value()
	if value == lob.Nil {
		return 0
	}
	switch val := value.(type) {
	case *lob.Number:
		return int(val.Value())
	}
	return 1
}

func SetVerbosity(value int) {
	newvalue := lob.NewNumber(float64(value))
	lob.SysVerbosity.SetValue(newvalue)
}

func GetStacktrace() bool {
	return lob.SysStacktrace.Value() != lob.Nil &&
		lob.SysStacktrace.Value() != nil
}
func SetStacktrace(value bool) {
	lob.SysStacktrace.SetValue(lob.Bool2Ob(value))
}

func GetEvaltrace() bool {
	return lob.SysEvaltrace.Value() != lob.Nil &&
		lob.SysEvaltrace.Value() != nil
}
func SetEvaltrace(value bool) {
	lob.SysEvaltrace.SetValue(lob.Bool2Ob(value))
}

func GetCalltrace() bool {
	return lob.SysCalltrace.Value() != lob.Nil &&
		lob.SysCalltrace.Value() != nil
}
func SetCalltrace(value bool) {
	lob.SysCalltrace.SetValue(lob.Bool2Ob(value))
}

func AllProps() lob.Object {
	result := lob.Object(lob.Nil)
	for _, prop := range allprops {
		result = lob.Cons(lob.Cons(prop, prop.Value()), result)
	}
	return result
}

func Init_props() {
	//fmt.Println("sys/props.init")
	allprops = []*lob.Symbol{
		lob.SysVerbosity,
		lob.SysStacktrace,
		lob.SysEvaltrace,
		lob.SysCalltrace,
		lob.SysErrordrop,
		lob.SysWarnerrors,
	}

	for _, prop := range allprops {
		prop.SetValue(lob.Nil)
	}
	dep.GetVerbosity = GetVerbosity
	dep.GetStacktrace = GetStacktrace
	dep.GetErrordrop = GetErrordrop
	dep.GetWarnerrors = GetWarnerrors
}
